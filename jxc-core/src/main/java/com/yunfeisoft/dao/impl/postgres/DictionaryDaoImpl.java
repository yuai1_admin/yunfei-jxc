/**
 * DictionaryDaoImpl.java
 * Created at 2017-07-06
 * Created by Jackie liu
 * Copyright (C) 2014, All rights reserved.
 */
package com.yunfeisoft.dao.impl.postgres;

import com.applet.base.BaseDaoImpl;
import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.WhereBuilder;
import com.applet.utils.Page;
import com.yunfeisoft.dao.inter.DictionaryDao;
import com.yunfeisoft.model.Dictionary;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * <p>ClassName: DictionaryDaoImpl</p>
 * <p>Description: 字典管理Dao实现</p>
 * <p>Author: Jackie liu</p>
 * <p>Date: 2017-07-06</p>
 */
@Repository
public class DictionaryDaoImpl extends BaseDaoImpl<Dictionary, String> implements DictionaryDao {
	
	@Override
	public Page<Dictionary> queryPage(Map<String, Object> params) {
		WhereBuilder qc = new WhereBuilder();
		qc.setOrderByWithDesc("value");
		if (params != null) {
			initPageParam(qc, params);
			String searchInput = (String) params.get("searchInput");
			if (StringUtils.isNotEmpty(searchInput)) {
				qc.andFullLike("name", searchInput);
			}
		}
		return queryPage(qc);
	}
	
	@Override
	public int modifyState(final String[] ids ,final Integer state) {
		String sql = String.format("UPDATE %s SET STATE_ = ?  WHERE ID_ = ?",
				domainModelAnalysis.getTableName());
		int[] batchUpdate = jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {

			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				ps.setInt(1, state);
				ps.setString(2, ids[i]);
			}

			@Override
			public int getBatchSize() {
				return ids.length;
			}
		});
		return batchUpdate.length;
	}

	@Override
	public boolean isDuplicateName(String id, String name) {
		return isDuplicateField(id, name, "name");
	}

	@Override
	public boolean isDuplicateValue(String id, String value) {
		return isDuplicateField(id, value, "value");
	}

	@Override
	public List<Dictionary> queryList(Map<String, Object> params) {
		WhereBuilder qc = new WhereBuilder();
		qc.setOrderByWithDesc("value");
		if (params != null) {
			initPageParam(qc, params);
			qc.andEquals("value", params.get("value"));
		}
		return query(qc);
	}
}