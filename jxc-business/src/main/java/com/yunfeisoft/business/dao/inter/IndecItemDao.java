package com.yunfeisoft.business.dao.inter;

import com.applet.base.BaseDao;
import com.yunfeisoft.business.model.IndecItem;
import com.applet.utils.Page;
import com.yunfeisoft.business.model.OrderItemStatistics;

import java.util.List;
import java.util.Map;

/**
 * ClassName: IndecItemDao
 * Description: 损益单商品信息Dao
 * Author: Jackie liu
 * Date: 2020-08-06
 */
public interface IndecItemDao extends BaseDao<IndecItem, String> {

    public Page<IndecItem> queryPage(Map<String, Object> params);

    public List<IndecItem> queryByIndecOrderId(String indecOrderId);

    public int removeByIndecOrderId(String indecOrderId);

    public List<OrderItemStatistics> queryStatistics(String orgId, String productId);
}