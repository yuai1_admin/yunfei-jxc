/**
 * ResourceController.java
 * Created at 2017-07-06
 * Created by Jackie liu
 * Copyright (C) 2014, All rights reserved.
 */
package com.yunfeisoft.controller.base;

import com.applet.base.BaseController;
import com.applet.utils.*;
import com.yunfeisoft.enumeration.YesNoEnum;
import com.yunfeisoft.model.Resource;
import com.yunfeisoft.service.inter.ResourceService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>ClassName: ResourceController</p>
 * <p>Description: 资源管理Controller</p>
 * <p>Author: Jackie liu</p>
 * <p>Date: 2017-07-06</p>
 */
/**
 * Created by Jackie Liu on 2017/3/18.
 */
@Controller
public class ResourceController extends BaseController {

	@Autowired
	private ResourceService resourceService;

	/**
	 * 添加资源管理
	 *
	 * @param record
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/web/resource/save", method = RequestMethod.POST)
	@ResponseBody
	public Response save(Resource record, HttpServletRequest request, HttpServletResponse response) {
		Validator validator = new Validator();
		validator.required(request, "name", "名称不能为空");
		validator.required(request, "code", "编码不能为空");
		validator.required(request, "url", "url不能为空");
		validator.required(request, "functionName", "模块名称不能为空");
		validator.required(request, "operateType", "操作类型不能为空");
		validator.required(request, "operateObject", "操作对象不能为空");
		validator.required(request, "tableName", "操作表不能为空");
		if (validator.isError()) {
			return ResponseUtils.warn(validator.getMessage());
		}
		Boolean duplicateName = resourceService.isDuplicateName(record.getId(), record.getName());
		if(duplicateName){
			return ResponseUtils.warn("该资源名称已存在");
		}
		Boolean duplicateCode = resourceService.isDuplicateCode(record.getId(), record.getCode());
		if(duplicateCode){
			return ResponseUtils.warn("该资源编码已存在");
		}
		record.setId(KeyUtils.getKey());
		record.setState(YesNoEnum.YES_ACCPET.getValue());
		resourceService.save(record);
		
		return ResponseUtils.success("信息添加成功");
	}

	/**
	 * 修改资源管理
	 *
	 * @param record
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/web/resource/modify", method = RequestMethod.POST)
	@ResponseBody
	public Response modify(Resource record, HttpServletRequest request, HttpServletResponse response) {
		Validator validator = new Validator();
		validator.required(request, "id", "参数有误");
		validator.required(request, "name", "名称不能为空");
		validator.required(request, "code", "编码不能为空");
		validator.required(request, "url", "url不能为空");
		validator.required(request, "functionName", "模块名称不能为空");
		validator.required(request, "operateType", "操作类型不能为空");
		validator.required(request, "operateObject", "操作对象不能为空");
		validator.required(request, "tableName", "操作表不能为空");
		if (validator.isError()) {
			return ResponseUtils.warn(validator.getMessage());
		}
		
		Boolean duplicateName = resourceService.isDuplicateName(record.getId(), record.getName());
		if(duplicateName){
			return ResponseUtils.warn("该资源名称已存在");
		}
		Boolean duplicateCode = resourceService.isDuplicateCode(record.getId(), record.getCode());
		if(duplicateCode){
			return ResponseUtils.warn("该资源编码已存在");
		}
		resourceService.modify(record);
		return ResponseUtils.success("修改成功");
	}

	/**
	 * 查询资源管理
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/web/resource/query", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public Response query(HttpServletRequest request, HttpServletResponse response) {
		String id = ServletRequestUtils.getStringParameter(request, "id", null);
		if (StringUtils.isBlank(id)) {
			return ResponseUtils.warn("参数错误");
		}
		Resource data = resourceService.load(id);
		return ResponseUtils.success(data);
	}

	/**
	 * 分页查询资源管理
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/web/resource/list", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public Response list(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> params = new HashMap<String, Object>();
		String searchInput = ServletRequestUtils.getStringParameter(request, "searchInput", null);
		params.put("searchInput", searchInput);
		initParams(params, request);
		Page<Resource> data = resourceService.queryPage(params);
		return ResponseUtils.success(data);
	}

	/**
	 * 批量删除资源信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "/web/resource/delete", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public Response batchRemove(HttpServletRequest request) {
		String ids = ServletRequestUtils.getStringParameter(request, "ids", null);
		if (StringUtils.isEmpty(ids)) {
			return ResponseUtils.warn("请选择菜单信息");
		}
		resourceService.batchRemove(ids.split(","));
		return ResponseUtils.success("删除成功");
	}

	/**
	 * 停用资源
	 * 
	 * @return
	 */
	@RequestMapping(value = "/web/resource/stop", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public Response stop(HttpServletRequest request) {
		String ids = ServletRequestUtils.getStringParameter(request, "ids", null);
		if (StringUtils.isEmpty(ids)) {
			return ResponseUtils.warn("请选择启用资源信息");
		}
		resourceService.modifyState(ids.split(","), YesNoEnum.NO_CANCEL.getValue());
		return ResponseUtils.success("资源停用成功");
	}

	/**
	 * 启用资源
	 * 
	 * @return
	 */
	@RequestMapping(value = "/web/resource/accpet", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public Response accpet(HttpServletRequest request) {
		String ids = ServletRequestUtils.getStringParameter(request, "ids", null);
		if (StringUtils.isEmpty(ids)) {
			return ResponseUtils.warn("请选择启用资源信息");
		}
		resourceService.modifyState(ids.split(","), YesNoEnum.YES_ACCPET.getValue());
		return ResponseUtils.success("资源启用成功");
	}

}
