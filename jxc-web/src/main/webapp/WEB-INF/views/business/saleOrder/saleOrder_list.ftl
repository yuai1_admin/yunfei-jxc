<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>销售单列表</title>
	<#include "/common/vue_resource.ftl">
</head>
<body>
<div id="app" v-cloak>
    <div class="app-container" @click="hideMenu">
        <div class="layui-row app-header">
            <div class="layui-col-md3">
                <@auth code='saleOrder_add'>
                    <button type="button" class="layui-btn layui-btn-sm" @click="add">创建销售单</button>
                </@auth>
                <button type="button" class="layui-btn layui-btn-sm layui-btn-danger" @click="top.showNotice('sale_order_list')">使用技巧</button>
            </div>
            <div class="layui-col-md9 text-right">
                <@auth code='saleOrder_column_price'>
                    <#--<span style="margin-top:5px;display:inline-block;margin-right:10px;">应收总计：<span style="color:#f33;">￥{{totalSurplusAmount}}</span></span>-->
                    <span style="margin-top:5px;display:inline-block;">合计应收：￥</span>
                    <input type="text" v-model="incomeAmount" placeholder="应收金额" class="layui-input" disabled style="width:100px;"/>
                    <span style="margin-top:5px;display:inline-block;">实际收款：￥</span>
                    <input type="text" v-model="actualAmount" placeholder="实际收款" class="layui-input" style="width:100px;"/>
                    <button type="button" class="layui-btn layui-btn-sm" @click="incomeSelect">收款</button>
                    <span style="margin-top:5px;display:inline-block;">收款状态：</span>
                    <select v-model="params.payStatus" class="layui-input" @change="seachData">
                        <option value="">请选择</option>
                        <#--<option value="1">待收款</option>-->
                        <option value="2">已结清</option>
                        <option value="3">欠款</option>
                    </select>
                </@auth>
                <input type="text" v-model="params.customerName" placeholder="客户名称" class="layui-input" @keyup.13="seachData"/>
                <button type="button" class="layui-btn layui-btn-sm layui-btn-primary search-button" @click="seachData">查询</button>
            </div>
        </div>
        <div class="app-list">
            <div class="layui-row">
                <div class="layui-col-md9">
                    <div class="app-table-num"><span class="num">销售单列表(共 {{total}} 条)</span></div>
                </div>
                <div class="layui-col-md3 text-right">
                    <#--<span class="prev" @click="loadPrev">上一页</span>
                    <span class="next" @click="loadNext">下一页</span>-->
                </div>
            </div>
            <table class="layui-table" lay-even lay-skin="nob" lay-size1="sm">
                <thead>
                <tr>
                    <th style="width:20px;">#</th>
                    <th>订单编号</th>
                    <th>客户名称</th>
                    <th>销售日期</th>
                    <#--<th>出库状态</th>-->
                    <th>收款状态</th>
                    <@auth code='saleOrder_column_price'>
                        <th>总价</th>
                        <th>已收金额</th>
                        <th>待收金额</th>
                        <th style="cursor:pointer" @click="checkAll">全选</th>
                    </@auth>
                    <th>备注</th>
                    <th style="width:200px;">操作</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(item, index) in rows" <@auth code='saleOrder_detail'> @dblclick="showDetail(index)" </@auth>>
                    <#--<td>{{20 * (params.page - 1) + 1 + index}}</td>-->
                    <td>{{1 + index}}</td>
                    <td>{{item.code}}</td>
                    <td>{{item.customerName}}</td>
                    <td>{{item.saleDate}}</td>
                    <#--<td>
                        <span class="ui-warn" v-if="item.status == 1">{{item.statusStr}}</span>
                        <span class="ui-accept" v-if="item.status == 2">{{item.statusStr}}</span>
                        <span class="ui-error" v-if="item.status == 3">{{item.statusStr}}</span>
                    </td>-->
                    <td>
                        <span class="ui-warn" v-if="item.payStatus == 1">{{item.payStatusStr}}</span>
                        <span class="ui-accept" v-if="item.payStatus == 2">{{item.payStatusStr}}</span>
                        <span class="ui-error" v-if="item.payStatus == 3">{{item.payStatusStr}}</span>
                    </td>
                    <@auth code='saleOrder_column_price'>
                        <td>￥{{item.totalAmount}}</td>
                        <td>￥{{item.payAmount}}</td>
                        <td>￥{{item.surplusAmount}}</td>
                        <td><input type="checkbox" v-model="selectOrderIndexs" :value="index" v-if="item.payStatus != 2 && item.status == 2"/></td>
                    </@auth>
                    <td>{{item.remark}}</td>
                    <td class="more-parent">
                        <div class="ui-operating" v-if="item.status == 2" @click="print(index)">打印</div>
                        <div class="ui-split" v-if="item.status == 2"></div>
                        <@auth code='change_sale_order'>
                            <div class="ui-operating" v-if="item.status == 2" @click="modify(index)">改单</div>
                            <div class="ui-split" v-if="item.status == 2"></div>
                        </@auth>
                        <@auth code='income_sale_order'>
                            <div class="ui-operating" v-if="item.payStatus != 2 && item.status == 2" @click="income(index)">收款</div>
                            <div class="ui-split" v-if="item.payStatus != 2 && item.status == 2"></div>
                        </@auth>
                        <@auth code='saleOrder_update'>
                            <div class="ui-operating" v-if="item.status != 2" @click="modify(index)">编辑</div>
                            <div class="ui-split" v-if="item.status != 2"></div>
                        </@auth>
                        <@auth code='saleOrder_detail'>
                            <div class="ui-operating" @click="showDetail(index)">查看</div>
                            <div class="ui-split"></div>
                        </@auth>
                        <@auth code='saleOrder_delete'>
                            <div class="ui-operating" @click="remove(index)">删除</div>
                        </@auth>
                    </td>
                </tr>
                <tr v-if="rows.length <= 0">
                    <td colspan="11" class="text-center">没有更多数据了...</td>
                </tr>
                </tbody>
            </table>
            <div class="layui-row">
                <div class="layui-col-md6">
                    <div class="app-table-num"><span class="num">共 {{total}} 条（滑动到底部加载更多）</span></div>
                </div>
                <div class="layui-col-md6 text-right">
                    <#--<span class="prev" @click="loadPrev">上一页</span>
                    <span class="next" @click="loadNext">下一页</span>-->
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            params: {
                customerName: '',
                payStatus: '',
                page: 1,
            },
            rows: [],
            total: 0,
            selectOrderIndexs: [],
            incomeAmount: '0',
            actualAmount: '0',
            totalSurplusAmount: 0,
            //checkOrderId: '',
            isLast: false,
            isLoading: false,
        },
        mounted: function () {
            //this.loadTotalPayAmount();
            this.loadData();
            this.bindLoadMore();
            <#if (params.add!) == 1>
                this.add();
            </#if>
        },
        watch: {
            selectOrderIndexs: function (newValue, oldValue) {
                var amount = 0, rows = this.rows;
                for (var i = 0; i < newValue.length; i++) {
                    var order = rows[newValue[i]];
                    amount = CalculateFloat.floatAdd(amount, order.surplusAmount);
                }
                this.incomeAmount = amount;
                this.actualAmount = amount;
            },
        },
        methods: {
            seachData:function(){
                this.params.page = 1;
                this.$nextTick(function () {
                    this.loadData();
                });
            },
            bindLoadMore: function () {
                var that = this;
                window.onscroll = function (ev) {
                    //变量scrollTop是滚动条滚动时，距离顶部的距离
                    var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
                    //变量windowHeight是可视区的高度
                    var windowHeight = document.documentElement.clientHeight || document.body.clientHeight;
                    //变量scrollHeight是滚动条的总高度
                    var scrollHeight = document.documentElement.scrollHeight || document.body.scrollHeight;
                    //滚动条到底部的条件(距底部20px时触发加载)
                    if (scrollTop + windowHeight >= scrollHeight - 20 && !that.isLoading && !that.isLast) {
                        that.loadNext(true);
                    }
                };
            },
            /*loadTotalPayAmount: function () {
                this.selectOrderIndexs = [];
                this.incomeAmount = '0';
                var that = this;
                $.http.post("${params.contextPath}/web/saleOrder/totalPayAmount.json", this.params).then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    that.totalSurplusAmount = data.data || 0;
                    that.loadData();
                });
            },*/
            loadData: function (isAdd) {
                this.selectOrderIndexs = [];
                this.incomeAmount = '0';
                var that = this;
                $.http.post("${params.contextPath}/web/saleOrder/list.json", this.params).then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    if (isAdd && (typeof isAdd) != 'object') {
                        that.rows = that.rows.concat(data.rows);
                    } else {
                        that.rows = data.rows;
                    }
                    that.total = data.total;
                    var page = data.total % 20 > 0 ? parseInt(data.total / 20) + 1 : parseInt(data.total / 20);
                    if (page <= that.params.page) {
                        that.isLast = true;
                    } else {
                        that.isLast = false;
                    }
                    that.isLoading = false;
                });
            },
            checkAll: function () {
                if (this.selectOrderIndexs.length > 0) {
                    this.selectOrderIndexs = [];
                    return;
                }
                var amount = 0, rows = this.rows, selectIndexs = [];
                for (var i = 0; i < rows.length; i++) {
                    var item = rows[i];
                    if (item.payStatus != 2 && item.status == 2) {
                        //amount = CalculateFloat.floatAdd(amount, item.surplusAmount);
                        selectIndexs.push(i);
                    }
                }
                this.selectOrderIndexs = selectIndexs;
                //this.incomeAmount = amount;
            },
            loadNext: function (isAdd) {
                /*if (this.rows.length < 20 && this.rows.length > 0) {
                    return;
                }*/
                if (this.isLast || this.isLoading) {
                    return;
                }
                this.isLoading = true;
                this.params.page = this.params.page + 1;
                this.loadData(isAdd);
            },
            loadPrev: function () {
                if (this.params.page <= 1) {
                    return;
                }
                this.params.page = this.params.page - 1;
                this.loadData();
            },
            add:function () {
                this.showTypes = false;
                var url = "${params.contextPath!}/view/business/saleOrder/saleOrder_edit.htm";
                DialogManager.open({url:url, width:'98%', height:'100%', title:'添加销售单', cancel:function (index, layero) {
                        return confirm("单据未保存\n点【确定】表示不保存直接退出\n点【取消】回到单据界面收/结保存");
                    }});
            },
            modify: function (index) {
                var row = this.rows[index], that = this, checkOrderId = this.checkOrderId;
                var url = "${params.contextPath!}/view/business/saleOrder/saleOrder_edit.htm?id=" + row.id;
                DialogManager.open({url: url, width: '98%', height: '100%', title: '编辑销售单', cancel: function (index, layero) {
                        return confirm("单据未保存\n点【确定】表示不保存直接退出\n点【取消】回到单据界面收/结保存");
                    }
                });
            },
            income: function (index) {
                var row = this.rows[index];
                var url = "${params.contextPath!}/view/business/incomeRecord/incomeRecord_edit.htm?saleOrderId=" + row.id;
                DialogManager.open({url: url, width: '60%', height: '100%', title: '编辑销售单收款'});
            },
            income2: function (orderId) {
                layer.closeAll();
                this.loadData();
                var url = "${params.contextPath!}/view/business/incomeRecord/incomeRecord_edit.htm?saleOrderId=" + orderId;
                DialogManager.open({url: url, width: '60%', height: '100%', title: '编辑销售单收款'});
            },
            showDetail: function (index) {
                var row = this.rows[index];
                var url = "${params.contextPath!}/view/business/saleOrder/saleOrder_detail.htm?id=" + row.id;
                DialogManager.open({url: url, width: '98%', height: '100%', title: '查看销售单'});
            },
            print: function (index) {
                var row = this.rows[index];
                var url = "${params.contextPath!}/view/business/saleOrder/saleOrder_print.htm?id=" + row.id;
                DialogManager.open({url: url, width: '98%', height: '100%', title: '打印销售单'});
            },
            /*changeOrder: function (index) {
                var that = this, id = this.rows[index].id;
                this.checkOrderId = id;
                $.http.post("${params.contextPath}/web/saleOrder/changeOrder.json", {id: id}).then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    //$.message("改单成功，现在可以修改或者删除订单");
                    that.loadData();
                    that.modify(index);
                });
            },*/
            showMenu:function (index) {
                this.hideMenu();
                this.$set(this.rows[index], "showMenu", true);
            },
            hideMenu: function () {
                var that = this;
                this.rows.forEach(function (item) {
                    that.$set(item, 'showMenu', false);
                });
            },
            remove:function (index) {//删除
                if (!confirm("确定删除订单吗?")) {
                    return;
                }
                var that = this;
                $.http.post("${params.contextPath}/web/saleOrder/delete.json", {ids: this.rows[index].id}).then(function (data) {
                    $.message(data.message);
                    if (!data.success) {
                        return;
                    }
                    that.loadData();
                });
            },
            incomeSelect: function () {
                var ids = [], rows = this.rows, selectOrderIndexs = this.selectOrderIndexs;
                for (var i = 0; i < selectOrderIndexs.length; i++) {
                    var order = rows[selectOrderIndexs[i]];
                    ids.push(order.id);
                }
                if (ids.length <= 0) {
                    $.message("请勾选需要收款的订单");
                    return;
                }

                if (parseFloat(this.actualAmount) > parseFloat(this.incomeAmount)) {
                    $.message("实际收款金额大于合计应收金额");
                    return;
                }
                var that = this;
                layer.confirm('确定结清勾选的订单吗？', {
                    btn: ['取消', '确定'] //按钮
                }, function () {
                    DialogManager.closeAll();
                    //layer.msg('的确很重要', {icon: 1});
                }, function () {
                    $.http.post("${params.contextPath}/web/incomeRecord/batchSave.json", {
                        saleOrderIds: ids.join(","),
                        actualAmount: that.actualAmount
                    }).then(function (data) {
                        $.message(data.message);
                        if (!data.success) {
                            return;
                        }
                        //that.loadTotalPayAmount();
                        that.loadData();
                    });
                });
            },
        }
    });
</script>
</body>

</html>
