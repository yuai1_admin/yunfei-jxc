package com.yunfeisoft.controller.interceptor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 采集日志拦截器
 * Created by Jackie Liu on 2017/7/10.
 */
public class LogInterceptor extends HandlerInterceptorAdapter {

    @Value("${log.open}")
    private boolean logOpen;

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        super.afterCompletion(request, response, handler, ex);
        if (!logOpen) {
            return;
        }
        /*String uri = request.getRequestURI();
        Resource resource = getResourceInfo(uri, request);
        if (resource != null) {
            OperationLogService service = SpringContextHelper.getBean(OperationLogService.class);
            OperationLog log = new OperationLog();
            log.setId(KeyUtils.getDistributedId());
            //log.setOperateObject(resource.getOperateObject());
            //log.setTableName(resource.getTableName());
            log.setOperateTime(new Date());
            log.setOperateType(resource.getOperateType());
            log.setRegId(resource.getId());
            log.setResourceUrl(resource.getUrl());
            //log.setFunctionName(resource.getFunctionName());
            log.setTerminalId(getRemoteHost(request));
            log.setOperateCondition(getParamStr(request));
            log.setResourceName(resource.getName());
            log.setResourceCode(resource.getCode());
            User user = ApiUtils.getLoginUser(request);
            if (user != null) {
                Organization org = user.getOrganization();
                log.setOrgCode(org.getCode());
                log.setOrgName(org.getName());
                log.setUserId(user.getId());
                log.setUserName(user.getName());
            }
            service.save(log);
        }*/
    }
}
