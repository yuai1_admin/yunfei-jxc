package com.yunfeisoft.business.service.impl;

import com.applet.base.BaseServiceImpl;
import com.yunfeisoft.business.dao.inter.AllotItemDao;
import com.yunfeisoft.business.model.AllotItem;
import com.yunfeisoft.business.model.OrderItemStatistics;
import com.yunfeisoft.business.service.inter.AllotItemService;
import com.applet.sql.separation.DataSourceChange;
import com.applet.utils.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * ClassName: AllotItemServiceImpl
 * Description: 调拨单商品信息service实现
 * Author: Jackie liu
 * Date: 2020-08-06
 */
@Service("allotItemService")
public class AllotItemServiceImpl extends BaseServiceImpl<AllotItem, String, AllotItemDao> implements AllotItemService {

    @Override
    @DataSourceChange(slave = true)
    public Page<AllotItem> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }

    @Override
    public List<AllotItem> queryByAllotOrderId(String orderId) {
        return getDao().queryByAllotOrderId(orderId);
    }

    @Override
    public List<OrderItemStatistics> queryStatistics(String orgId, String productId) {
        return getDao().queryStatistics(orgId, productId);
    }
}