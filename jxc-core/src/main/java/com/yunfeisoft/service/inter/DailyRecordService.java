package com.yunfeisoft.service.inter;

import com.applet.base.BaseService;
import com.applet.utils.Page;
import com.yunfeisoft.model.DailyRecord;

import java.util.Map;

/**
 * ClassName: DailyRecordService
 * Description: 操作日志service接口
 * Author: Jackie liu
 * Date: 2018-03-08
 */
public interface DailyRecordService extends BaseService<DailyRecord, String> {

    public Page<DailyRecord> queryPage(Map<String, Object> params);
}