package com.yunfeisoft.service.inter;

import com.applet.base.BaseService;
import com.applet.utils.Page;
import com.yunfeisoft.model.TaskUser;

import java.util.List;
import java.util.Map;

/**
 * ClassName: TaskUserService
 * Description: 流程任务用户信息service接口
 * Author: Jackie liu
 * Date: 2019-07-08
 */
public interface TaskUserService extends BaseService<TaskUser, String> {

    public Page<TaskUser> queryPage(Map<String, Object> params);

    public List<TaskUser> queryList(Map<String, Object> params);
}