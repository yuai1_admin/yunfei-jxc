package com.yunfeisoft.business.dao.impl.postgres;

import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.WhereBuilder;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.QuoteOrderDao;
import com.yunfeisoft.business.model.QuoteOrder;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * ClassName: QuoteOrderDaoImpl
 * Description: 报价单信息Dao实现
 * Author: Jackie liu
 * Date: 2020-09-01
 */
@Repository
public class QuoteOrderDaoImpl extends ServiceDaoImpl<QuoteOrder, String> implements QuoteOrderDao {

    @Override
    public Page<QuoteOrder> queryPage(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        if (params != null) {
            initPageParam(wb, params);
        }
        return queryPage(wb);
    }
}