<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>查看损益单信息</title>
    <#include "/common/vue_resource.ftl">
    <style>
        body{background:#F2F2F2;padding:15px;}
        .footer span{font-size:12px;margin-right:15px;}
        @media print {
            .income-container, .btn-container, .layui-card-header{display:none;}
        }
    </style>
</head>
<body>
<div id="app" v-cloak>
    <div class="layui-card btn-container">
        <#--<div class="layui-card-header">收款信息</div>-->
        <div class="layui-card-body">
            <button type="button" class="layui-btn layui-btn-normal" @click="window.print();">打印</button>
            <@auth code='indecOrder_update'><button type="button" class="layui-btn layui-btn-normal" v-if="record.status != 2" @click="modify">编辑</button></@auth>
            <@auth code='change_indec_order'><button type="button" class="layui-btn" v-if="record.status == 2" @click="changeOrder">改单</button></@auth>
            <@auth code='indecOrder_delete'><button type="button" class="layui-btn layui-btn-primary" v-if="record.status != 2" @click="remove">删单</button></@auth>
        </div>
    </div>

    <div class="layui-card">
        <div class="layui-card-header">订单信息</div>
        <div class="layui-card-body" style="padding-top:20px;">
            <div style="text-align:center;font-size:26px;margin-bottom:20px;">${(user.organization.name)!}损益单</div>
            <div style="text-align:right;font-weight:bold;">单号：{{record.code}}</div>
            <div style="text-align:right;">制单日期：{{record.indecDate}}</div>
            <table class="layui-table" lay-even lay-skin1="nob" lay-size="sm" style="margin-top:20px;">
                <thead>
                <tr>
                    <th style="width:20px;">序号</th>
                    <th>编码</th>
                    <th>品名</th>
                    <th>规格</th>
                    <th>单位</th>
                    <th>账面库存</th>
                    <th>实际库存</th>
                    <th>损益数量</th>
                    <@auth code='purchaseOrder_column_price'>
                        <th>损益单价</th>
                        <th>金额</th>
                    </@auth>
                    <th>备注</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(item, index) in record.indecItemList">
                    <td>{{1 + index}}</td>
                    <td>{{item.productCode}}</td>
                    <td>{{item.productName}}</td>
                    <td>{{item.productStandard}}</td>
                    <td>{{item.productUnit}}</td>
                    <td>{{item.actualStock}}</td>
                    <td>{{item.stock}}</td>
                    <td>{{item.indecQuantity}}</td>
                    <@auth code='purchaseOrder_column_price'>
                        <td>￥{{item.price}}</td>
                        <td>￥{{item.amount}}</td>
                    </@auth>
                    <td>{{item.remark}}</td>
                </tr>
                <#--<tr>
                    <td></td>
                    <td>合计</td>
                    <td style="font-weight:bold;">￥{{record.totalAmount}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>-->
                <#--<tr>
                    <td colspan="9" class="footer">
                        <span>调出仓库：{{record.outWarehouseName}}</span>
                        <span>调入仓库：{{record.inWarehouseName}}</span>
                    </td>
                </tr>-->
                </tbody>
            </table>
        </div>
    </div>

</div>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            record : {},
        },
        mounted: function () {
            this.loadData();
        },
        methods: {
            loadData: function () {
                if (!'${params.id!}') {
                    return;
                }
                var that = this;
                $.http.post('${params.contextPath}/web/indecOrder/query.json', {id: '${params.id!}'}).then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    that.record = data.data;
                });
            },
            changeOrder: function (index) {
                var that = this;
                $.http.post("${params.contextPath}/web/indecOrder/changeOrder.json", {id: '${params.id!}'}).then(function (data) {
                    $.message(data.message);
                    if (!data.success) {
                        return;
                    }
                    var url = "${params.contextPath!}/view/business/indecOrder/indecOrder_detail.htm?id=${params.id!}";
                    location.href = url;
                });
            },
            modify: function () {
                var url = "${params.contextPath!}/view/business/indecOrder/indecOrder_edit.htm?id=${params.id!}";
                location.href = url;
            },
            remove:function (index) {//删除
                var that = this;
                $.http.post("${params.contextPath}/web/indecOrder/delete.json", {ids: "${params.id!}"}).then(function (data) {
                    $.message(data.message);
                    if (!data.success) {
                        return;
                    }
                    var alt = layer.alert(data.message || "操作成功", function () {
                        parent.app.loadData();
                        parent.layer.closeAll();
                        layer.close(alt);
                    });
                });
            },
        }
    });
</script>
</body>

</html>