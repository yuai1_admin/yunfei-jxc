<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>操作日志详情</title>
    <#include "/common/resource.ftl">
    <script type="text/javascript">
        $(function () {
            <#if (params.id)??>
                $.ajaxRequest({
                    url: '${params.contextPath}/web/dailyRecord/query.json',
                    data: {id: "${params.id}"},
                    success: function (data) {
                        if (!data.success) {
                            $.message(data.message);
                            return;
                        }
                        var record = data.data;
                        for (var key in record) {
                            $("[field='" + key + "']").html(record[key]);
                        }
                    }
                });
            </#if>
        });
    </script>
</head>
<body>
<div class="ui-table-div">
    <table class="layui-table ui-table">
        <tr>
            <th>企业名称</th>
            <td field="orgName">--</td>
        </tr>
        <tr>
            <th>部门名称</th>
            <td field="deptName">--</td>
        </tr>
        <tr>
            <th>用户名称</th>
            <td field="userName">--</td>
        </tr>
        <tr>
            <th>用户账号</th>
            <td field="userAccount">--</td>
        </tr>
        <tr>
            <th>用户角色</th>
            <td field="userRoles">--</td>
        </tr>
        <tr>
            <th>操作类型</th>
            <td field="operateType">--</td>
        </tr>
        <tr>
            <th>操作说明</th>
            <td field="remark">--</td>
        </tr>
        <tr>
            <th>用户IP</th>
            <td field="ip">--</td>
        </tr>
        <tr>
            <th>创建时间</th>
            <td field="createTimeStr">--</td>
        </tr>
    </table>
</div>
</body>

</html>
