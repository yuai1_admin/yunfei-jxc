/**
 * MessageTemplateService.java
 * Created at 2017-09-05
 * Created by Jackie liu
 * Copyright (C) 2014 , All rights reserved.
 */
package com.yunfeisoft.service.inter;

import com.applet.base.BaseService;
import com.yunfeisoft.model.MessageTemplate;

/**
 * <p>ClassName: MessageTemplateService</p>
 * <p>Description: 消息模板service接口</p>
 * <p>Author: Jackie liu</p>
 * <p>Date: 2017-09-05</p>
 */
public interface MessageTemplateService extends BaseService<MessageTemplate, String> {

    public MessageTemplate queryByCode(String code);
}