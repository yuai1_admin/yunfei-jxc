package com.yunfeisoft.model;

import com.applet.base.BaseModel;
import com.applet.sql.record.TransientField;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
* ClassName: TaskUser
* Description: 流程任务用户信息
*
* @Author: Jackie liu
* Date: 2019-07-08
*/
@Entity
@Table(name="TR_TASK_USER")
public class TaskUser extends BaseModel implements Serializable {
	
	/**
     * Field serialVersionUID: 序列号
     */
    private static final long serialVersionUID = 1L;

	/**
     * 任务id
     */
	@Column
	private String taskKey;

	/**
     * 用户id
     */
	@Column
	private String userId;

	@TransientField
	private String userName;
	@TransientField
	private String taskName;


	public String getTaskKey() {
		return taskKey;
	}

	public void setTaskKey(String taskKey) {
		this.taskKey = taskKey;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
}