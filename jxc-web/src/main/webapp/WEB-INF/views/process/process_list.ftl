<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>流程信息列表</title>
	<#include "/common/resource.ftl">
</head>
<body>

<div class="ui-operation">
    <div class="ui-search-items layui-form">
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">流程KEY</label>
                <div class="layui-input-inline">
                    <input type="text" name="key" class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">流程名称</label>
                <div class="layui-input-inline">
                    <input type="text" name="name" class="layui-input">
                </div>
            </div>
        </div>
    </div>
    <div class="ui-buttons">
        <div class="layui-btn-group tools">
            <button class="layui-btn layui-btn-normal open-dialog" p="url:'${params.contextPath}/view/process/process_edit.htm',title:'添加流程信息',width:'600px',height:'400px'"><i class="fa fa-plus"></i>添加</button>
            <button class="layui-btn layui-btn-normal singleSelected" p="url:'${params.contextPath}/view/process/process_edit.htm',title:'编辑流程信息',width:'600px',height:'400px'"><i class="fa fa-pencil"></i>修改</button>
            <button class="layui-btn layui-btn-normal remove-button" reurl="${params.contextPath}/web/process/delete.json"><i class="fa fa-remove"></i>删除</button>
            <button class="layui-btn layui-btn-normal process-task-button" p="url:'${params.contextPath}/view/processTask/processTask_list.htm',title:'配置流程任务信息',width:'80%',height:'100%'"><i class="fa fa-gear"></i>配置流程任务</button>
            <button class="layui-btn layui-btn-normal deploy-button" reurl="${params.contextPath}/web/process/deploy.json"><i class="fa fa-send"></i>部署流程</button>
            <button class="layui-btn layui-btn-normal remove-deploy-button" reurl="${params.contextPath}/web/process/removeDeploy.json"><i class="fa fa-trash"></i>删除部署流程</button>
        </div>
        <div class="ui-searchs">
            <#--<div class="value"><input type="text" name="name" value="" placeholder="名称"/></div>-->
            <input type="button" value="搜索" class="layui-btn layui-btn-danger search-button"/>
        </div>
    </div>
</div>
<div class="ui-content" style="padding-left: 10px;padding-right:10px;">
    <table id="datagrid" options="url:'${params.contextPath}/web/process/list.json',params:'getSearchParams',dblClickRow:'showDetail'">
        <thead>
			<tr>
				<th data-options="field:'id',checkbox:true"></th>
				<th data-options="field:'key',width:100,align:'left'">流程KEY</th>
				<th data-options="field:'name',width:100,align:'left'">流程名称</th>
				<th data-options="field:'bpmnPath',width:100,align:'left'">BPMN文件路径</th>
				<th data-options="field:'deployId',width:100,align:'left'">部署ID</th>
			</tr>
        </thead>
    </table>
</div>
<script type="text/javascript">
    function getSearchParams() {
        return {
            name: $("input[name='name']").val(),
            key: $("input[name='key']").val()
        };
    }
    var showDetail = function (index, row) {
        var url = "${params.contextPath}/view/process/process_detail.htm?id=" + row.id;
        DialogManager.openDialog("url:'" + url + "',title:'流程信息详情',width:'600px',height:'200px'");
    };
    //formatter:formatState
    function formatState(val, row) {
    }

    $(function () {
        $(".remove-button").click(function () {
            var ids = DataGrid.getCheckedIds();
            if (ids == "") {
                $.message("请选择删除记录")
                return false;
            }
            var url = $(this).attr("reurl");
            layer.confirm('确定删除记录', function () {
                $.ajaxRequest({
                    type: 'post',
                    url: url,
                    data: {ids: ids.join(",")},
                    success: function (data) {
                        $.message(data.message);
                        if (data.success) {
                            DataGrid.reload();
                        }
                    }
                });
            });
        });
        $(".deploy-button").click(function () {
            var id = DataGrid.getSelectId();
            if (id == "") {
                $.message("请选择流程记录")
                return false;
            }
            var url = $(this).attr("reurl");
            $.ajaxRequest({
                type: 'post',
                url: url,
                data: {id: id},
                success: function (data) {
                    $.message(data.message);
                    if (data.success) {
                        DataGrid.reload();
                    }
                }
            });
        });
        $(".remove-deploy-button").click(function () {
            var id = DataGrid.getSelectId();
            if (id == "") {
                $.message("请选择流程记录")
                return false;
            }
            var url = $(this).attr("reurl");
            $.ajaxRequest({
                type: 'post',
                url: url,
                data: {id: id},
                success: function (data) {
                    $.message(data.message);
                    if (data.success) {
                        DataGrid.reload();
                    }
                }
            });
        });
        $(".process-task-button").click(function () {
            var row = DataGrid.getSelectRow();
            if (!row) {
                $.message("请选择一行!");
                return false;
            }
            var p = $(this).attr("p");
            var params = eval("({" + (p || "") + "})");
            params.url = $.getUrl(params.url, "processKey", row.key);
            DialogManager.openDialog(JSON.stringify(params).replace("{", "").replace("}", ""));
        });
    });
</script>
</body>

</html>
