<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>编辑调拨单信息</title>
	<#include "/common/vue_resource.ftl">
	<style>
		.layui-card-header{font-weight:bold;}
		.layui-table[lay-size="sm"] td{padding:5px;}
		.product-table, .product-table th{text-align:center;}
		.product-table .layui-input{height:28px;display:inline-block;}
		.product-table .layui-btn-sm{height:27px;line-height:27px;vertical-align:top;}
		.more-parent, .ui-operating{font-size:12px;text-align:center;}
	</style>
</head>
<body style="background:#f2f2f2;">
<div id="app" v-cloak>
	<div style="padding:10px;">
		<form class="layui-form" @submit.prevent="submitForm()" method="post">
			<div class="layui-card">
				<div class="layui-card-header">表单信息
					<button type="button" class="layui-btn layui-btn-sm layui-btn-danger" @click="top.showNotice('allot_order_add')">使用技巧</button>
				</div>
				<div class="layui-card-body">
					<div class="layui-form-item">
						<div class="layui-inline">
							<label class="layui-form-label">调出仓库<span class="ui-request">*</span></label>
							<div class="layui-input-inline">
								<select v-model="record.outWarehouseId" class="layui-input">
									<option value="">请选择</option>
									<option :value="item.id" v-for="(item, index) in warehouses" :key="'out' + item.id">{{item.name}}</option>
								</select>
							</div>
						</div>
						<div class="layui-inline">
							<label class="layui-form-label">调入仓库<span class="ui-request">*</span></label>
							<div class="layui-input-inline">
								<select v-model="record.inWarehouseId" class="layui-input">
									<option value="">请选择</option>
									<option :value="item.id" v-for="(item, index) in warehouses" :key="'in' + item.id">{{item.name}}</option>
								</select>
							</div>
						</div>
						<div class="layui-inline">
							<label class="layui-form-label">调拨日期<span class="ui-request">*</span></label>
							<div class="layui-input-inline">
								<input type="text" v-model="record.allotDate" id="allotDate" placeholder="点击选择日期" class="layui-input" readonly style="cursor:pointer;"/>
							</div>
						</div>
					</div>
					<div class="layui-form-item">
						<div class="layui-inline">
							<label class="layui-form-label">订单备注</label>
							<div class="layui-input-inline">
								<input type="text" v-model="record.remark" placeholder="请输入备注" class="layui-input" style="width:515px;"/>
							</div>
						</div>
					</div>
					<#--end-->
				</div>
			</div>
			<div class="layui-card">
				<div class="layui-card-header">
					商品信息 <span style="color:#FF5722;font-weight:normal;font-size:12px;margin-left:20px;">{{warnMsg}}</span>
					<div style="display:inline-block;float:right;font-weight:normal;font-size:12px;">
						<#--<button type="button" class="layui-btn layui-btn-normal layui-btn-sm" @click="newLineProduct" style="vertical-align:baseline;">新行</button>
						<button type="button" class="layui-btn layui-btn-normal layui-btn-sm" @click="showMultiProductDialog" style="vertical-align:baseline;">多选商品</button>-->
					</div>
				</div>
				<div class="layui-card-body" style="padding:0px;">
					<table class="layui-table product-table" lay-even lay-skin1="nob" lay-size="sm" style="margin:0px;">
						<thead>
						<tr>
							<th style="width:20px;">#</th>
							<th style="width:170px;">商品编码</th>
							<th>商品名称</th>
							<th style="width:80px;">规格</th>
							<th style="width:40px;">单位</th>
							<th style="width:40px;">数量</th>
							<th style="width:50px;">单价</th>
							<th style="width:20px;">小计</th>
							<th style="width:150px;">备注</th>
							<th style="width:40px;">操作</th>
						</tr>
						</thead>
						<tbody>
						<tr v-for="(item, index) in products">
							<td>{{1 + index}}</td>
							<td>
								<input type="text" v-model="item.productCode" class="layui-input" style="width:130px;cursor:pointer;" <#--@click="showMultiProductDialog"--> @click="showProductDialog(index)" readonly placeholder="搜索商品"/>
								<#--<button type="button" class="layui-btn layui-btn-normal layui-btn-sm" @click="showProductDialog(index)">选择</button>-->
							</td>
							<td>{{item.productName}}</td>
							<td>{{item.productStandard}}</td>
							<td>{{item.productUnit}}</td>
							<td><input type="text" v-model="item.quantity" class="layui-input" :data-index="index" data-modal="quantity" @keyup="inputKeyup($event, index, 'quantity')"/></td>
							<td><input type="text" v-model="item.price" class="layui-input" :data-index="index" data-modal="price" @keyup="inputKeyup($event, index, 'price')"/></td>
							<td>{{item.amount}}</td>
							<td><input type="text" v-model="item.remark" class="layui-input" :data-index="index" data-modal="remark" @keyup="inputKeyup($event, index, 'remark')"/></td>
							<td class="more-parent">
								<div class="ui-operating" @click="removeProduct(index)">删除</div>
							</td>
						</tr>
						<tr>
							<td colspan="10">
								<button type="button" class="layui-btn layui-btn-danger layui-btn-sm1" @click="showMultiProductDialog">选择商品</button>
							</td>
						</tr>
						</tbody>
					</table>
				</div>
			</div>

			<div class="layui-form-item">
				<div class="layui-input-block">
					<#--<input type="button" @click="submitForm" value="临时保存" class="layui-btn layui-btn-normal" />-->
					<input type="button" @click="submitForm" value="保存" class="layui-btn" />
					<#--<input type="button" @click="newLineProduct" value="新行" class="layui-btn layui-btn-normal" />-->
					<input type="button" v-if="record.id" @click="remove" value="删除" class="layui-btn layui-btn-danger" />
				</div>
			</div>
		</form>
	</div>
</div>
<script>
	var app = new Vue({
		el: '#app',
		data: {
			showTypes: false,
			record: {
				id: '${params.id!}',
				outWarehouseId:'',
				inWarehouseId:'',
				allotDate:'${.now?string("yyyy-MM-dd")}',
				remark: '',
				productsStr: '',
			},
			warehouses:[],
			products: [],
			currentProductClickIndex: 0,
			warnMsg: '',
		},
		mounted: function () {
			this.init();
			this.loadWarehouses();
		},
		watch: {
			products: {
				handler: function (newVal, oldVal) {
					this.calculateTotalAmount();
				},
				deep: true
			}
		},
		methods: {
			init: function () {
				var that = this;
				laydate.render({
					elem: '#allotDate', type: 'date', done: function (value) {
						that.record.allotDate = value;
					}
				});

				if (!this.record.id) {
					this.newLineProduct();
				}
			},
			newLineProduct: function () {
				this.products.push({
					productId: '',
					warehouseId: '',
					productCode: '',
					productName: '',
					productStandard: '',
					quantity: '',
					price: '',
					amount: 0,
					remark: ''
				});
			},
			calculateTotalAmount: function () {
				var totalAmount = 0, allSuccess = true;
				for (var i = 0; i < this.products.length; i++) {
					var item = this.products[i];
					if (isNaN(item.quantity)) {
						allSuccess = false;
						this.warnMsg = "第" + (i + 1) + "行【数量】不是一个合法的数字";
						continue;
					}
					if (isNaN(item.price)) {
						allSuccess = false;
						this.warnMsg = "第" + (i + 1) + "行【单价】不是一个合法的数字";
						continue;
					}
					var amount = CalculateFloat.floatMul(item.quantity, item.price);
					item.amount = amount;
					//totalAmount = CalculateFloat.floatAdd(totalAmount, amount);
				}
				//this.record.totalAmount = totalAmount;
				if (allSuccess) {
					this.warnMsg = "";
				}
			},
			removeProduct: function (index) {
				this.products.splice(index, 1);
			},
			loadWarehouses: function () {
				var that = this;
				$.http.post("${params.contextPath}/web/warehouse/queryList.json", this.params).then(function (data) {
					if (!data.success) {
						$.message(data.message);
						return;
					}
					that.warehouses = data.data;
					that.loadData();
				});
			},
			loadData: function () {
				if (!'${params.id!}') {
					return;
				}
				var that = this;
				$.http.post('${params.contextPath}/web/allotOrder/query.json', {id: '${params.id!}'}).then(function (data) {
					if (!data.success) {
						$.message(data.message);
						return;
					}
					var item = data.data;
					for (var key in  that.record) {
						that.record[key] = item[key];
					}
					that.products = item.allotItemList || [];
				});
			},
			submitForm: function () {
				this.record.productsStr = JSON.stringify(this.products);
				$.http.post('${params.contextPath}/web/allotOrder/<#if (params.id)??>modify<#else>save</#if>.json', this.record).then(function (data) {
					if (!data.success) {
						$.message(data.message);
						return;
					}
					var alt = layer.alert(data.message || "操作成功", function () {
						parent.app.loadData();
						parent.layer.closeAll();
						layer.close(alt);
					});
				});
			},
			showProductDialog: function (index) {
				if (!this.record.outWarehouseId) {
					$.message("请选择调出仓库");
					return;
				}
				this.currentProductClickIndex = index;
				var url = "${params.contextPath!}/view/business/product/product_stock_dialog.htm?warehouseId=" + this.record.outWarehouseId;
				DialogManager.open({url: url, width: '90%', height: '90%', title: '选择商品'});
			},
			productSelectCallBack: function (row) {
				/*for (var i = 0; i < this.products.length; i ++) {
					var item = this.products[i];
					if (item.productId == row.productId) {
						DialogManager.closeAll();
						return;
					}
				}*/
				var product = this.products[this.currentProductClickIndex];
				product.productId = row.productId;
				product.warehouseId = row.warehouseId;
				product.productCode = row.productCode;
				product.productName = row.productName;
				product.productStandard = row.productStandard;
				product.productUnit = row.productUnit;
				product.price = row.buyPrice;
				if (this.currentProductClickIndex == (this.products.length - 1)) {
					this.newLineProduct();
				}
				DialogManager.closeAll();
			},
			showMultiProductDialog: function () {
				if (!this.record.outWarehouseId) {
					$.message("请选择调出仓库");
					return;
				}
				var url = "${params.contextPath!}/view/business/product/product_stock_dialog.htm?multi=1&warehouseId=" + this.record.outWarehouseId;
				DialogManager.open({url: url, width: '90%', height: '100%', title: '选择商品'});
			},
			multiProductSelectCallBack: function (row) {
				/*for (var i = 0; i < this.products.length; i++) {
					var item = this.products[i];
					if (item.productId == row.productId) {
						return;
					}
				}*/
				var product = {quantity: '', amount: 0, remark: ''};
				product.productId = row.productId;
				product.warehouseId = row.warehouseId;
				product.productCode = row.productCode;
				product.productName = row.productName;
				product.productStandard = row.productStandard;
				product.productUnit = row.productUnit;
				product.price = row.buyPrice;
				this.products.push(product);

				for (var i = 0; i < this.products.length; i ++) {
					var item = this.products[i];
					if (item.productId) {
						continue;
					}
					this.products.splice(i, 1);
				}
				this.newLineProduct();
			},
			remove: function () {//删除
				if (!confirm("确定删除订单吗?")) {
					return;
				}
				$.http.post("${params.contextPath}/web/allotOrder/delete.json", {ids: this.record.id || ""}).then(function (data) {
					$.message(data.message);
					if (!data.success) {
						return;
					}
					var alt = layer.alert(data.message || "操作成功", function () {
						parent.app.loadData();
						parent.layer.closeAll();
						layer.close(alt);
					});
				});
			},
			inputKeyup:function (e, index, modalName) {
				var modal = { 'quantity': 0, 'price': 1, 'remark': 2};
				var modals = ['quantity', 'price', 'remark'];
				var keyCode = e.keyCode;
				if (keyCode == 37) {//左键
					var leftIndex = modal[modalName] - 1;
					leftIndex = leftIndex < 0 ? 0 : leftIndex;
					var leftName = modals[leftIndex];
					$("[data-index='" + index + "'][data-modal='" + leftName + "']").focus();
					return;
				}

				if (keyCode == 39) {//右键
					var rightIndex = modal[modalName] + 1;
					rightIndex = rightIndex > 2 ? 2 : rightIndex;
					var rightName = modals[rightIndex];
					$("[data-index='" + index + "'][data-modal='" + rightName + "']").focus();
					return;
				}

				if (keyCode == 40 || keyCode == 13) {//下键
					var nextIndex = index + 1;
					nextIndex = nextIndex > this.products.length - 1 ? this.products.length - 1 : nextIndex;
					$("[data-index='" + nextIndex + "'][data-modal='" + modalName + "']").focus();
					return;
				}

				if (keyCode == 38) {//上键
					var upIndex = index - 1;
					upIndex = upIndex < 0 ? 0 : upIndex;
					$("[data-index='" + upIndex + "'][data-modal='" + modalName + "']").focus();
					return;
				}
			},
		}
	});
</script>
</body>

</html>