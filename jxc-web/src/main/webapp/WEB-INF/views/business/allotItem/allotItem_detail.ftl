<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>调拨单商品信息详情</title>
    <#include "/common/resource.ftl">
    <script type="text/javascript">
        $(function () {
            <#if (params.id)??>
                $.ajaxRequest({
                    url: '${params.contextPath}/web/allotItem/query.json',
                    data: {id: "${params.id}"},
                    success: function (data) {
                        if (!data.success) {
                            $.message(data.message);
                            return;
                        }
                        var record = data.data;
                        for (var key in record) {
                            $("[field='" + key + "']").html(record[key]);
                        }
                    }
                });
            </#if>
        });
    </script>
</head>
<body>
<div class="ui-table-div">
    <table class="layui-table ui-table">
        <tr>
            <th>主键</th>
            <td field="id">--</td>
        </tr>
        <tr>
            <th>组织id</th>
            <td field="orgId">--</td>
        </tr>
        <tr>
            <th>调拨单id</th>
            <td field="allotOrderId">--</td>
        </tr>
        <tr>
            <th>商品id</th>
            <td field="productId">--</td>
        </tr>
        <tr>
            <th>仓库id</th>
            <td field="warehouseId">--</td>
        </tr>
        <tr>
            <th>调拨数量</th>
            <td field="quantity">--</td>
        </tr>
        <tr>
            <th>调拨前数量</th>
            <td field="beforeAllotQuantity">--</td>
        </tr>
        <tr>
            <th>调拨后数量</th>
            <td field="afterAllotQuantity">--</td>
        </tr>
        <tr>
            <th>备注</th>
            <td field="remark">--</td>
        </tr>
        <tr>
            <th>是否删除(1是，2否)</th>
            <td field="isDel">--</td>
        </tr>
        <tr>
            <th>创建人id</th>
            <td field="createId">--</td>
        </tr>
        <tr>
            <th>创建时间</th>
            <td field="createTime">--</td>
        </tr>
        <tr>
            <th>修改人id</th>
            <td field="modifyId">--</td>
        </tr>
        <tr>
            <th>修改时间</th>
            <td field="modifyTime">--</td>
        </tr>
    </table>
</div>
</body>

</html>
