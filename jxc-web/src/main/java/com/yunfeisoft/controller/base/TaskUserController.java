package com.yunfeisoft.controller.base;

import com.applet.base.BaseController;
import com.applet.utils.*;
import com.yunfeisoft.model.TaskUser;
import com.yunfeisoft.service.inter.TaskUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ClassName: TaskUserController
 * Description: 流程任务用户信息Controller
 * Author: Jackie liu
 * Date: 2019-07-08
 */
@Controller
public class TaskUserController extends BaseController {

    @Autowired
    private TaskUserService taskUserService;

    /**
     * 添加流程任务用户信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/taskUser/save", method = RequestMethod.POST)
    @ResponseBody
    public Response save(HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "taskKey", "参数错误");
        validator.required(request, "userIds", "请选择用户");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }

        String taskKey = ServletRequestUtils.getStringParameter(request, "taskKey", null);
        String userIdsStr = ServletRequestUtils.getStringParameter(request, "userIds", null);
        String[] userIds = userIdsStr.split(",");

        List<TaskUser> list = new ArrayList<>();
        for (String userId : userIds) {
            TaskUser taskUser = new TaskUser();
            taskUser.setTaskKey(taskKey);
            taskUser.setUserId(userId);
            list.add(taskUser);
        }

        taskUserService.batchSave(list);
        return ResponseUtils.success("保存成功");
    }

    /**
     * 分页查询流程任务用户信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/taskUser/list", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response list(HttpServletRequest request, HttpServletResponse response) {
        String taskKey = ServletRequestUtils.getStringParameter(request, "taskKey", "_no_");

        Map<String, Object> params = new HashMap<String, Object>();
        initParams(params, request);
        params.put("taskKey", taskKey);

        List<TaskUser> page = taskUserService.queryList(params);
        return ResponseUtils.success(page);
    }
}
