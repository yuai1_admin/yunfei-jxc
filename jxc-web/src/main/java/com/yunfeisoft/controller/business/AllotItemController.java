package com.yunfeisoft.controller.business;

import com.applet.base.BaseController;
import com.yunfeisoft.business.model.AllotItem;
import com.yunfeisoft.business.service.inter.AllotItemService;
import com.applet.utils.*;
import com.yunfeisoft.model.User;
import com.yunfeisoft.utils.ApiUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * ClassName: AllotItemController
 * Description: 调拨单商品信息Controller
 * Author: Jackie liu
 * Date: 2020-08-06
 */
@Controller
public class AllotItemController extends BaseController {

    @Autowired
    private AllotItemService allotItemService;

    /**
     * 添加调拨单商品信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    /*@RequestMapping(value = "/web/allotItem/save", method = RequestMethod.POST)
    @ResponseBody
    public Response save(AllotItem record, HttpServletRequest request, HttpServletResponse response) {
        allotItemService.save(record);
        return ResponseUtils.success("保存成功");
    }*/

    /**
     * 修改调拨单商品信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    /*@RequestMapping(value = "/web/allotItem/modify", method = RequestMethod.POST)
    @ResponseBody
    public Response modify(AllotItem record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "id", "参数错误");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }
        allotItemService.modify(record);
        return ResponseUtils.success("保存成功");
    }*/

    /**
     * 查询调拨单商品信息
     *
     * @param request
     * @param response
     * @return
     */
    /*@RequestMapping(value = "/web/allotItem/query", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response query(HttpServletRequest request, HttpServletResponse response) {
        String id = ServletRequestUtils.getStringParameter(request, "id", null);
        if (StringUtils.isBlank(id)) {
            return ResponseUtils.warn("参数错误");
        }
        AllotItem record = allotItemService.load(id);
        return ResponseUtils.success(record);
    }*/

    /**
     * 分页查询调拨单商品信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/allotItem/list", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response list(HttpServletRequest request, HttpServletResponse response) {
        String productId = ServletRequestUtils.getStringParameter(request, "productId", null);

        User user = ApiUtils.getLoginUser();
        Map<String, Object> params = new HashMap<String, Object>();
        initParams(params, request);
        params.put("productId", productId);
        params.put("orgId", user.getOrgId());

        Page<AllotItem> page = allotItemService.queryPage(params);
        return ResponseUtils.success(page);
    }

    /**
     * 批量删除调拨单商品信息
     *
     * @param request
     * @param response
     * @return
     */
    /*@RequestMapping(value = "/web/allotItem/delete", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response delete(HttpServletRequest request, HttpServletResponse response) {
        String ids = ServletRequestUtils.getStringParameter(request, "ids", null);
        if (StringUtils.isBlank(ids)) {
            return ResponseUtils.warn("参数错误");
        }
        allotItemService.remove(ids.split(","));
        return ResponseUtils.success("删除成功");
    }*/
}
