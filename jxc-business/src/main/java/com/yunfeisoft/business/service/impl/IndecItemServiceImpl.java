package com.yunfeisoft.business.service.impl;

import com.applet.base.BaseServiceImpl;
import com.applet.sql.separation.DataSourceChange;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.IndecItemDao;
import com.yunfeisoft.business.model.IndecItem;
import com.yunfeisoft.business.model.OrderItemStatistics;
import com.yunfeisoft.business.service.inter.IndecItemService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * ClassName: IndecItemServiceImpl
 * Description: 损益单商品信息service实现
 * Author: Jackie liu
 * Date: 2020-08-06
 */
@Service("indecItemService")
public class IndecItemServiceImpl extends BaseServiceImpl<IndecItem, String, IndecItemDao> implements IndecItemService {

    @Override
    @DataSourceChange(slave = true)
    public Page<IndecItem> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }

    @Override
    public List<IndecItem> queryByIndecOrderId(String indecOrderId) {
        return getDao().queryByIndecOrderId(indecOrderId);
    }

    @Override
    public List<OrderItemStatistics> queryStatistics(String orgId, String productId) {
        return getDao().queryStatistics(orgId, productId);
    }
}