<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>编辑商品参数信息</title>
	<#include "/common/vue_resource.ftl">
</head>
<body>
<div id="app" v-cloak>
	<div class="ui-form">
		<form class="layui-form" @submit.prevent="submitForm()" method="post">
			<div class="layui-form-item">
				<label class="layui-form-label">类别<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<select v-model="record.type" class="layui-input">
						<option value="">请选择</option>
						<option value="1">商品规格</option>
						<option value="2">商品单位</option>
						<option value="3">收入项目</option>
						<option value="4">支出项目</option>
						<option value="5">交易对象</option>
					</select>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">名称<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.name" placeholder="请输入名称" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">备注</label>
				<div class="layui-input-block">
					<textarea type="text" v-model="record.remark" class="layui-textarea"></textarea>
				</div>
			</div>

			<div class="layui-form-item">
				<div class="layui-input-block">
					<input type="submit" value="保存" class="layui-btn" />
				</div>
			</div>
		</form>
	</div>
</div>
<script>
	var app = new Vue({
		el: '#app',
		data: {
			showTypes: false,
			record: {
				id: '${params.id!}',
				type: '',
				name: '',
				remark: '',
			},
		},
		mounted: function () {
			this.init();
			this.loadData();
		},
		methods: {
			init:function(){
				var that = this;
				/*laydate.render({elem: '#beginDate', type:'datetime', done:function (value) {
						that.record.beginDate = value;
					}});*/

			},
			loadData: function () {
				if (!'${params.id!}') {
					return;
				}
				var that = this;
				$.http.post('${params.contextPath}/web/productParams/query.json', {id: '${params.id!}'}).then(function (data) {
					if (!data.success) {
						$.message(data.message);
						return;
					}
					var item = data.data;
					for (var key in  that.record) {
						that.record[key] = item[key];
					}
				});
			},
			submitForm: function () {
				$.http.post('${params.contextPath}/web/productParams/<#if (params.id)??>modify<#else>save</#if>.json', this.record).then(function (data) {
					if (!data.success) {
						$.message(data.message);
						return;
					}
					var alt = layer.alert(data.message || "操作成功", function () {
						parent.app.loadData();
						parent.layer.closeAll();
						layer.close(alt);
					});
				});
			},
		}
	});
</script>
</body>

</html>
