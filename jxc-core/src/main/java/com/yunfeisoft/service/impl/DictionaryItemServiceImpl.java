/**
 * DictionaryItemServiceImpl.java
 * Created at 2017-07-06
 * Created by Jackie liu
 * Copyright (C) 2014 , All rights reserved.
 */
package com.yunfeisoft.service.impl;

import com.applet.base.BaseServiceImpl;
import com.applet.sql.separation.DataSourceChange;
import com.applet.utils.Page;
import com.yunfeisoft.dao.inter.DictionaryItemDao;
import com.yunfeisoft.model.DictionaryItem;
import com.yunfeisoft.service.inter.DictionaryItemService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>ClassName: DictionaryItemServiceImpl</p>
 * <p>Description: 字典项管理service实现</p>
 * <p>Author: Jackie liu</p>
 * <p>Date: 2017-07-06</p>
 */
@Service("dictionaryItemService")
public class DictionaryItemServiceImpl extends BaseServiceImpl<DictionaryItem, String, DictionaryItemDao> implements DictionaryItemService {

    @DataSourceChange(slave = true)
    @Override
    public Page<DictionaryItem> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }

    @DataSourceChange(slave = true)
    @Override
    public int removeByDicId(String[] dicIds) {
        return getDao().removeByDicId(dicIds);
    }

    @Override
    public List<DictionaryItem> queryAcceptList(String dictionaryValue) {
        return getDao().queryAcceptList(dictionaryValue);
    }
}