package com.yunfeisoft.business.dao.inter;

import com.applet.base.BaseDao;
import com.yunfeisoft.business.model.QuoteOrder;
import com.applet.utils.Page;

import java.util.Map;

/**
 * ClassName: QuoteOrderDao
 * Description: 报价单信息Dao
 * Author: Jackie liu
 * Date: 2020-09-01
 */
public interface QuoteOrderDao extends BaseDao<QuoteOrder, String> {

    public Page<QuoteOrder> queryPage(Map<String, Object> params);
}