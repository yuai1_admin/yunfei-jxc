/**
 * AreaServiceImpl.java
 * Created at 2017-07-06
 * Created by Jackie liu
 * Copyright (C) 2014 , All rights reserved.
 */
package com.yunfeisoft.service.impl;

import com.applet.base.BaseServiceImpl;
import com.applet.sql.separation.DataSourceChange;
import com.applet.utils.Constants;
import com.applet.utils.Page;
import com.yunfeisoft.dao.inter.AreaDao;
import com.yunfeisoft.model.Area;
import com.yunfeisoft.service.inter.AreaService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>ClassName: AreaServiceImpl</p>
 * <p>Description: 区域管理service实现</p>
 * <p>Author: Jackie liu</p>
 * <p>Date: 2017-07-06</p>
 */
@Service("areaService")
public class AreaServiceImpl extends BaseServiceImpl<Area, String, AreaDao> implements AreaService {

    @Override
    @DataSourceChange(slave = true)
    public Page<Area> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }

    @Override
    @DataSourceChange(slave = false)
    public int batchRemove(String[] ids) {
        return getDao().batchRemove(ids);
    }

    @Override
    @DataSourceChange(slave = false)
    public int modifyState(int state, String[] ids) {
        return getDao().modifyState(state, ids);
    }

    @Override
    @DataSourceChange(slave = true)
    public List<Area> query(Map<String, Object> params) {
        return getDao().query(params);
    }

    @Override
    public int modifyWithDrag(String id, String targetId) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("idPath", id);
        List<Area> childList = getDao().query(params);

        //设置idPath
        Area old = load(id);
        Area target = load(targetId);

        String oldPath = old.getIdPath();

        old.setIdPath(target.getIdPath() + "/" + id);
        old.setParentId(targetId);

        String newPath = old.getIdPath();

        for (Area child : childList) {
            if (child.getId().equals(id)) {
                continue;
            }
            child.setIdPath(child.getIdPath().replaceFirst(oldPath, newPath));
            getDao().update(child);
        }
        return getDao().update(old);
    }

    @Override
    public int insert(Area area) {
        if (area.getParentId() == null) {
            area.setParentId(Constants.ROOT);
        }
        if (Constants.ROOT.equals(area.getParentId())) {
            area.setIdPath(String.valueOf(area.getId()));
        } else {
            Area parentArea = load(area.getParentId());
            if (parentArea != null) {
                area.setIdPath(area.getId() + "/" + parentArea.getIdPath());
            }
        }
        return super.save(area);
    }

    @Override
    public boolean isDuplicateCode(String id, String code) {
        return getDao().isDuplicateCode(id, code);
    }

    @Override
    public boolean isDuplicateName(String id, String name, String parentId) {
        return getDao().isDuplicateName(id, name, parentId);
    }

    @DataSourceChange(slave = true)
    @Override
    public List<Area> queryRootList(Map<String, Object> params) {
        return getDao().queryRootList(params);
    }

    @DataSourceChange(slave = true)
    @Override
    public List<Area> querySecondList(String parentId, int state) {
        return getDao().querySecondList(parentId, state);
    }
}