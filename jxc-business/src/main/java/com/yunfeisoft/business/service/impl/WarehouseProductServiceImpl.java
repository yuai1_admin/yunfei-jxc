package com.yunfeisoft.business.service.impl;

import com.applet.base.BaseServiceImpl;
import com.yunfeisoft.business.dao.inter.WarehouseProductDao;
import com.yunfeisoft.business.model.WarehouseProduct;
import com.yunfeisoft.business.service.inter.WarehouseProductService;
import com.applet.sql.separation.DataSourceChange;
import com.applet.utils.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * ClassName: WarehouseProductServiceImpl
 * Description: 仓库商品信息service实现
 * Author: Jackie liu
 * Date: 2020-08-04
 */
@Service("warehouseProductService")
public class WarehouseProductServiceImpl extends BaseServiceImpl<WarehouseProduct, String, WarehouseProductDao> implements WarehouseProductService {

    @Override
    @DataSourceChange(slave = true)
    public Page<WarehouseProduct> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }

    @Override
    public List<WarehouseProduct> queryList(Map<String, Object> params) {
        return getDao().queryList(params);
    }

    @Override
    public WarehouseProduct queryStockSum(String productId) {
        return getDao().queryStockSum(productId);
    }
}