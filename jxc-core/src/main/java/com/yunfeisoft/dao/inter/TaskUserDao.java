package com.yunfeisoft.dao.inter;

import com.applet.base.BaseDao;
import com.applet.utils.Page;
import com.yunfeisoft.model.TaskUser;

import java.util.List;
import java.util.Map;

/**
 * ClassName: TaskUserDao
 * Description: 流程任务用户信息Dao
 * Author: Jackie liu
 * Date: 2019-07-08
 */
public interface TaskUserDao extends BaseDao<TaskUser, String> {

    public Page<TaskUser> queryPage(Map<String, Object> params);

    public List<TaskUser> queryList(Map<String, Object> params);

    public int deleteByTaskKey(String taskKey);
}