package com.yunfeisoft.business.service.inter;

import com.applet.base.BaseService;
import com.yunfeisoft.business.model.IndecOrder;
import com.applet.utils.Page;

import java.util.Map;

/**
 * ClassName: IndecOrderService
 * Description: 损益单信息service接口
 * Author: Jackie liu
 * Date: 2020-08-06
 */
public interface IndecOrderService extends BaseService<IndecOrder, String> {

    public Page<IndecOrder> queryPage(Map<String, Object> params);

    public int modifyWithUnLock(String id);
}