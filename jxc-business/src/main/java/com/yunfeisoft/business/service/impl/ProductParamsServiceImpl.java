package com.yunfeisoft.business.service.impl;

import com.applet.base.BaseServiceImpl;
import com.yunfeisoft.business.dao.inter.ProductParamsDao;
import com.yunfeisoft.business.model.ProductParams;
import com.yunfeisoft.business.service.inter.ProductParamsService;
import com.applet.sql.separation.DataSourceChange;
import com.applet.utils.Page;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * ClassName: ProductParamsServiceImpl
 * Description: 商品参数信息service实现
 * Author: Jackie liu
 * Date: 2020-07-23
 */
@Service("productParamsService")
public class ProductParamsServiceImpl extends BaseServiceImpl<ProductParams, String, ProductParamsDao> implements ProductParamsService {

    @Override
    @DataSourceChange(slave = true)
    public Page<ProductParams> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }
}