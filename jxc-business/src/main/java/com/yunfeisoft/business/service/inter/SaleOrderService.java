package com.yunfeisoft.business.service.inter;

import com.applet.base.BaseService;
import com.yunfeisoft.business.model.SaleOrder;
import com.applet.utils.Page;
import com.yunfeisoft.business.model.SaleOrderStatistics;

import java.util.List;
import java.util.Map;

/**
 * ClassName: SaleOrderService
 * Description: 销售单信息service接口
 * Author: Jackie liu
 * Date: 2020-07-23
 */
public interface SaleOrderService extends BaseService<SaleOrder, String> {

    public Page<SaleOrder> queryPage(Map<String, Object> params);

    /**
     * 改单解锁
     * @param id
     * @return
     */
    public int modifyWithUnLock(String id, SaleOrder saleOrder);

    /**
     * 根据客户汇总销售数据
     * @param orgId
     * @param beginDate
     * @param endDate
     * @return
     */
    public List<SaleOrderStatistics> queryStatisticsByCustomer(String orgId, String beginDate, String endDate);

    /**
     * 根据商品类别汇总销售数据
     * @param orgId
     * @param beginDate
     * @param endDate
     * @return
     */
    public List<SaleOrderStatistics> queryStatisticsByProductCategory(String orgId, String beginDate, String endDate);

    /**
     * 根据商品汇总销售数据
     * @param orgId
     * @param beginDate
     * @param endDate
     * @return
     */
    public List<SaleOrderStatistics> queryStatisticsByProduct(String orgId, String beginDate, String endDate);

    /**
     * 根据供应商汇总销售数据
     * @param orgId
     * @param beginDate
     * @param endDate
     * @return
     */
    public List<SaleOrderStatistics> queryStatisticsBySupplier(String orgId, String beginDate, String endDate);

    /**
     * 根据制单人汇总销售数据
     * @param orgId
     * @param beginDate
     * @param endDate
     * @return
     */
    public List<SaleOrderStatistics> queryStatisticsByCreateId(String orgId, String beginDate, String endDate);

    /**
     * 根据日期统计销售单数、销售总额
     * @param orgId
     * @param beginDate
     * @param endDate
     * @return
     */
    public List<SaleOrderStatistics> queryStatisticsByDate(String orgId, String beginDate, String endDate);

    public SaleOrder queryTotalAmount(String orgId, int status, int payStatus);

    //public int modifyWithCheck(String id);
}