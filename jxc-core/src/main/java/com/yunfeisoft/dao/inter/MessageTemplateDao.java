/**
 * MessageTemplateDao.java
 * Created at 2017-09-05
 * Created by Jackie liu
 * Copyright (C) 2014, All rights reserved.
 */
package com.yunfeisoft.dao.inter;

import com.applet.base.BaseDao;
import com.yunfeisoft.model.MessageTemplate;

/**
 * <p>ClassName: MessageTemplateDao</p>
 * <p>Description: 消息模板Dao</p>
 * <p>Author: Jackie liu</p>
 * <p>Date: 2017-09-05</p>
 */
public interface MessageTemplateDao extends BaseDao<MessageTemplate, String> {

    public MessageTemplate queryByCode(String code);
}