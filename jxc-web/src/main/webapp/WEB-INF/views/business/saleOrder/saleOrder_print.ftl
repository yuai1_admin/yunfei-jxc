<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>打印销售单信息</title>
    <#include "/common/vue_resource.ftl">
    <style>
        body{background:#F2F2F2;padding:15px;}
        .footer span{font-size:12px;margin-right:15px;}
        .layui-elem-field legend{font-size:14px;font-weight:bold;}
        .print-input{height:30px;width:50px;display:inline-block;margin:0px auto;}
        .layui-field-box{padding:0px;padding-bottom:5px;}
        .print-table th, .print-table td, .print-table{border:1px solid #000;color:#000;}

        .print-narrow-container{width:65mm;border1:1px solid #000;margin:0 auto;line-height:25px;padding:20px 0px;}
        .print-narrow-container .top{text-align:center;font-weight:bold;}
        .print-narrow-container .middle{margin-top:20px;}
        .print-narrow-container .split{text-align:center;}
        .print-narrow-table{margin:0px;width:100%}
        .print-narrow-table th,.print-narrow-table td{background: #fff;padding1:5px;text-align:center;font-size:14px !important;color:#000000 !important;}
        .print-narrow-container .name{height:15mm;}
        .print-narrow-container .footer{text-align:center;}
        @media print {
            .income-container, .btn-container, .layui-card-header{display:none;}
            .print-container{border:0px !important;}
            .print-table th, .print-table td, .print-table{border:1px solid #000;background:none;color:#000;}

            .print-narrow-container{width:65mm;border1:1px solid #000;margin:0 auto;line-height:25px;padding:20px 0px;display:block;}
            .print-narrow-container .top{text-align:center;font-weight:bold;}
            .print-narrow-container .middle{margin-top:20px;}
            .print-narrow-container .split{text-align:center;}
            .print-narrow-table{margin:0px;}
            .print-narrow-table th,.print-narrow-table td{background: #fff;padding1:5px;text-align:center;font-size:14px !important;color:#000;}
            .print-narrow-container .name{height:15mm;}
            .print-narrow-container .footer{text-align:center;padding-bottom:50px;}
        }
    </style>
</head>
<body>
<div id="app" v-cloak>
    <div class="layui-card btn-container">
        <div class="layui-card-header">打印设置（打印窄性票据时，页面宽度、页边距等设置将不起作用）</div>
        <div class="layui-card-body">
            <div class="layui-row layui-col-space10">
                <div class="layui-col-md2">
                    <fieldset class="layui-elem-field">
                        <legend>页面宽度</legend>
                        <div class="layui-field-box">
                            <table class="layui-table" lay-size="sm" lay-skin="nob" style="padding:0px;margin:0px;">
                                <tr>
                                    <td style="width:50px;"><input type="text" v-model="printParams.pageWidth" class="layui-input print-input"/></td>
                                    <td style="word-break: keep-all;">毫米</td>
                                </tr>
                            </table>
                        </div>
                    </fieldset>
                </div>
                <div class="layui-col-md5">
                    <fieldset class="layui-elem-field">
                        <legend>页边距</legend>
                        <div class="layui-field-box">
                            <table class="layui-table" lay-size="sm" lay-skin="nob" style="padding:0px;margin:0px;">
                                <tr>
                                    <td>左</td>
                                    <td><input type="text" v-model="printParams.pagePaddingLeft" class="layui-input print-input"/></td>
                                    <td>右</td>
                                    <td><input type="text" v-model="printParams.pagePaddingRight" class="layui-input print-input"/></td>
                                    <td>上</td>
                                    <td><input type="text" v-model="printParams.pagePaddingTop" class="layui-input print-input"/></td>
                                    <td>下</td>
                                    <td><input type="text" v-model="printParams.pagePaddingBottom" class="layui-input print-input"/></td>
                                </tr>
                            </table>
                        </div>
                    </fieldset>
                </div>
                <div class="layui-col-md5">
                    <fieldset class="layui-elem-field">
                        <legend>打印操作</legend>
                        <div class="layui-field-box" style="padding:2px 10px;padding-bottom:5px;">
                            <button type="button" class="layui-btn layui-btn-normal" @click="window.print();">立即打印</button>
                            <button type="button" class="layui-btn layui-btn-normal" @click="savePrintParams">保存设置并打印</button>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="layui-row layui-col-space10">
                <fieldset class="layui-elem-field">
                    <legend>单据选项调整</legend>
                    <div class="layui-field-box">
                        <div class="layui-row layui-col-space10">
                            <div class="layui-col-md3">
                                <table class="layui-table" lay-size="sm" lay-skin="nob" style="padding:0px;margin:0px;">
                                    <tr>
                                        <td>打印字号</td>
                                        <td>
                                            <select type="text" v-model="fontSize" class="layui-input print-input">
                                                <option value="14">标准</option>
                                                <option value="16">大1号</option>
                                                <option value="18">大2号</option>
                                                <option value="20">大3号</option>
                                                <option value="12">小1号</option>
                                                <option value="10">小2号</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>打印窄性票据</td>
                                        <td><input type="checkbox" v-model="printParams.printNarrow" value="1" class="print-checkbox"/></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="layui-col-md9">
                                <table class="layui-table" lay-size="sm" lay-skin1="nob" style="padding:0px;margin:0px;text-align:center;">
                                    <tr>
                                        <th style="width:20px;">列名</th>
                                        <td>序号</td>
                                        <td>编码</td>
                                        <td>品名</td>
                                        <td>规格</td>
                                        <td>单位</td>
                                        <td>数量</td>
                                        <td>售价</td>
                                        <td>金额</td>
                                        <td>备注</td>
                                        <td>订单备注</td>
                                    </tr>
                                    <tr>
                                        <th>列宽</th>
                                        <td><input type="text" v-model="printParams.colIndexWidth" class="layui-input print-input"/></td>
                                        <td><input type="text" v-model="printParams.colCodeWidth" class="layui-input print-input"/></td>
                                        <td><input type="text" v-model="printParams.colNameWidth" class="layui-input print-input"/></td>
                                        <td><input type="text" v-model="printParams.colStandardWidth" class="layui-input print-input"/></td>
                                        <td><input type="text" v-model="printParams.colUnitWidth" class="layui-input print-input"/></td>
                                        <td><input type="text" v-model="printParams.colQuantityWidth" class="layui-input print-input"/></td>
                                        <td><input type="text" v-model="printParams.colSalePriceWidth" class="layui-input print-input"/></td>
                                        <td><input type="text" v-model="printParams.colSaleAmountWidth" class="layui-input print-input"/></td>
                                        <td><input type="text" v-model="printParams.colRemarkWidth" class="layui-input print-input"/></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>显示</th>
                                        <td><input type="checkbox" v-model="printParams.colIndexShow" value="1" class="print-checkbox"/></td>
                                        <td><input type="checkbox" v-model="printParams.colCodeShow" value="1" class="print-checkbox"/></td>
                                        <td><input type="checkbox" v-model="printParams.colNameShow" value="1" class="print-checkbox"/></td>
                                        <td><input type="checkbox" v-model="printParams.colStandardShow" value="1" class="print-checkbox"/></td>
                                        <td><input type="checkbox" v-model="printParams.colUnitShow" value="1" class="print-checkbox"/></td>
                                        <td><input type="checkbox" v-model="printParams.colQuantityShow" value="1" class="print-checkbox"/></td>
                                        <td><input type="checkbox" v-model="printParams.colSalePriceShow" value="1" class="print-checkbox"/></td>
                                        <td><input type="checkbox" v-model="printParams.colSaleAmountShow" value="1" class="print-checkbox"/></td>
                                        <td><input type="checkbox" v-model="printParams.colRemarkShow" value="1" class="print-checkbox"/></td>
                                        <td><input type="checkbox" v-model="printParams.colOrderRemarkShow" value="1" class="print-checkbox"/></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
            <#--end-->
        </div>
    </div>

    <div class="layui-card">
        <div class="layui-card-header">单据信息</div>
        <div class="layui-card-body">
            <!--startprint-->
            <div class="print-narrow-container" v-if="printParams.printNarrow && printParams.printNarrow.length > 0">
                <div class="top">${(user.organization.name)!}销售单</div>
                <div class="middle">
                    单号：<span>{{record.code}}</span><br/>
                    制单日期：<span>{{record.saleDate}}</span><br/>
                    客户：<span>{{record.customer && record.customer.name}}</span><br/>
                </div>
                <div class="split">==============================</div>
                <div class="amount">
                    <table class="layui-table print-narrow-table" lay-skin="nob" lay-size="sm">
                        <thead>
                        <tr>
                            <th>品名</th>
                            <th>数量</th>
                            <@auth code='saleOrder_column_price'>
                                <th>售价</th>
                                <th>金额</th>
                            </@auth>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="(item, index) in record.saleItemList">
                            <td>{{item.productName}}</td>
                            <td>{{item.quantity}}</td>
                            <@auth code='saleOrder_column_price'>
                                <td>￥{{item.salePrice}}</td>
                                <td>￥{{item.saleAmount}}</td>
                            </@auth>
                        </tr>
                        <tr style="font-weight:bold;">
                            <td>合计</td>
                            <td>{{record.totalNum}}</td>
                            <@auth code='saleOrder_column_price'>
                                <td></td>
                                <td>￥{{record.totalAmount}}</td>
                            </@auth>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="split">==============================</div>
                <div class="card">
                    备注：<span>{{record.remark || ""}}</span>
                </div>
                <div class="split">==============================</div>
                <div class="footer">
                    谢谢惠顾
                </div>
            </div>
            <!--endprint-->
            <div class="print-container" v-if="!printParams.printNarrow || printParams.printNarrow.length == 0" :style="{width:printParams.pageWidth + 'mm',fontSize:printParams.printFontSize + 'px',paddingTop:printParams.pagePaddingTop + 'mm',paddingLeft:printParams.pagePaddingLeft + 'mm',paddingRight:printParams.pagePaddingRight + 'mm',paddingBottom:printParams.pagePaddingBottom + 'mm',border:'1px solid #FF5722',margin:'0px auto'}">
                <div :style="{textAlign:'center',fontSize:printParams.printTitleFontSize + 'px',marginBottom:'20px'}">${(user.organization.name)!}销售单</div>
                <div :style="{fontSize:printParams.printFontSize + 'px',textAlign:'right',fontWeight:'bold'}">单号：{{record.code}}</div>
                <#--<div style="text-align:right;">制单日期：{{record.saleDate}}</div>-->
                <div class="layui-row" style1="font-size:12px;">
                    <div class="layui-col-md6 layui-col-xs6 layui-col-sm6" :style="{fontSize:printParams.printFontSize + 'px'}">
                        客户：{{record.customer && record.customer.name}}
                    </div>
                    <div class="layui-col-md6 layui-col-xs6 layui-col-sm6" :style="{fontSize:printParams.printFontSize + 'px', textAlign:'right'}">
                        制单日期：{{record.saleDate}}
                    </div>
                </div>
                <table class="layui-table print-table" lay-even1 lay-skin1="nob" lay-size="sm" style="margin-top:20px;">
                    <thead>
                    <tr>
                        <th v-if="printParams.colIndexShow.length > 0" :style="{fontSize:printParams.printFontSize + 'px',width:printParams.colIndexWidth + 'px'}">序号</th>
                        <th v-if="printParams.colCodeShow.length > 0" :style="{fontSize:printParams.printFontSize + 'px',width:printParams.colCodeWidth + 'px'}">编码</th>
                        <th v-if="printParams.colNameShow.length > 0" :style="{fontSize:printParams.printFontSize + 'px',width:printParams.colNameWidth + 'px'}">品名</th>
                        <th v-if="printParams.colStandardShow.length > 0" :style="{fontSize:printParams.printFontSize + 'px',width:printParams.colStandardWidth + 'px'}">规格</th>
                        <th v-if="printParams.colUnitShow.length > 0" :style="{fontSize:printParams.printFontSize + 'px',width:printParams.colUnitWidth + 'px'}">单位</th>
                        <th v-if="printParams.colQuantityShow.length > 0" :style="{fontSize:printParams.printFontSize + 'px',width:printParams.colQuantityWidth + 'px'}">数量</th>
                        <@auth code='saleOrder_column_price'>
                            <th v-if="printParams.colSalePriceShow.length > 0" :style="{fontSize:printParams.printFontSize + 'px',width:printParams.colSalePriceWidth + 'px'}">售价</th>
                            <th v-if="printParams.colSaleAmountShow.length > 0" :style="{fontSize:printParams.printFontSize + 'px',width:printParams.colSaleAmountWidth + 'px'}">金额</th>
                        </@auth>
                        <th v-if="printParams.colRemarkShow.length > 0" :style="{fontSize:printParams.printFontSize + 'px',width:printParams.colRemarkWidth + 'px'}">备注</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="(item, index) in record.saleItemList">
                        <td v-if="printParams.colIndexShow.length > 0" :style="{fontSize:printParams.printFontSize + 'px'}">{{1 + index}}</td>
                        <td v-if="printParams.colCodeShow.length > 0" :style="{fontSize:printParams.printFontSize + 'px'}">{{item.productCode}}</td>
                        <td v-if="printParams.colNameShow.length > 0" :style="{fontSize:printParams.printFontSize + 'px'}">{{item.productName}}</td>
                        <td v-if="printParams.colStandardShow.length > 0" :style="{fontSize:printParams.printFontSize + 'px'}">{{item.productStandard || ""}}</td>
                        <td v-if="printParams.colUnitShow.length > 0" :style="{fontSize:printParams.printFontSize + 'px'}">{{item.productUnit || ""}}</td>
                        <td v-if="printParams.colQuantityShow.length > 0" :style="{fontSize:printParams.printFontSize + 'px'}">{{item.quantity}}</td>
                        <@auth code='saleOrder_column_price'>
                            <td v-if="printParams.colSalePriceShow.length > 0" :style="{fontSize:printParams.printFontSize + 'px'}">￥{{item.salePrice}}</td>
                            <td v-if="printParams.colSaleAmountShow.length > 0" :style="{fontSize:printParams.printFontSize + 'px'}">￥{{item.saleAmount}}</td>
                        </@auth>
                        <td v-if="printParams.colRemarkShow.length > 0" :style="{fontSize:printParams.printFontSize + 'px'}">{{item.remark}}</td>
                    </tr>
                    <tr>
                        <td v-if="printParams.colIndexShow.length > 0"></td>
                        <td v-if="printParams.colCodeShow.length > 0" :style="{fontSize:printParams.printFontSize + 'px'}">合计</td>
                        <td v-if="printParams.colNameShow.length > 0"></td>
                        <td v-if="printParams.colStandardShow.length > 0"></td>
                        <td v-if="printParams.colUnitShow.length > 0"></td>
                        <td v-if="printParams.colQuantityShow.length > 0" :style="{fontSize:printParams.printFontSize + 'px', fontWeight:'bold'}">{{record.totalNum}}</td>
                        <@auth code='saleOrder_column_price'>
                            <td v-if="printParams.colSalePriceShow.length > 0"></td>
                            <td v-if="printParams.colSaleAmountShow.length > 0" :style="{fontSize:printParams.printFontSize + 'px', fontWeight:'bold'}">￥{{record.totalAmount}}</td>
                        </@auth>
                        <td v-if="printParams.colRemarkShow.length > 0"></td>
                    </tr>
                    <tr>
                        <td colspan="9" :style="{fontSize:printParams.printFontSize + 'px'}" v-if="printParams.colOrderRemarkShow.length > 0">备注：{{record.remark || ""}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <#--end-->
        </div>
    </div>

</div>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            record : {},
            printParams: {
                pageWidth: '210',
                pagePaddingTop: '25.4',
                pagePaddingBottom: '25.4',
                pagePaddingLeft: '31.8',
                pagePaddingRight: '31.8',
                printFontSize: '14',
                printTitleFontSize: '26',
                printNarrow: [],
                colIndexWidth: '20',
                colCodeWidth: '20',
                colNameWidth: '200',
                colStandardWidth: '20',
                colUnitWidth: '20',
                colQuantityWidth: '20',
                colSalePriceWidth: '20',
                colSaleAmountWidth: '20',
                colRemarkWidth: '20',
                colIndexShow: ['1'],
                colCodeShow: ['1'],
                colNameShow: ['1'],
                colStandardShow: ['1'],
                colUnitShow: ['1'],
                colQuantityShow: ['1'],
                colSalePriceShow: ['1'],
                colSaleAmountShow: ['1'],
                colRemarkShow: ['1'],
                colOrderRemarkShow: ['1'],
            },
            fontSize: 14,
        },
        watch: {
            fontSize: function (newValue, oldValue) {
                this.printParams.printFontSize = newValue;
                this.printParams.printTitleFontSize = parseInt(newValue) + 12;
            }
        },
        mounted: function () {
            //console.log(JSON.stringify(this.printParams).length);
            this.loadPrintParams();
        },
        methods: {
            loadPrintParams: function () {
                if (!'${params.id!}') {
                    return;
                }
                var that = this;
                $.http.post('${params.contextPath}/web/printParams/query.json', {type: 'sale_order'}).then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    var record = data.data;
                    if (record) {
                        that.printParams = eval('(' + record.params + ')');
                        that.fontSize = that.printParams.printFontSize;
                    }
                    that.loadData();
                });
            },
            loadData: function () {
                if (!'${params.id!}') {
                    return;
                }
                var that = this;
                $.http.post('${params.contextPath}/web/saleOrder/query.json', {id: '${params.id!}'}).then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    var record = data.data, totalNum = 0;
                    var saleItemList = record.saleItemList;
                    for (var i = 0; i < saleItemList.length; i ++) {
                        totalNum = CalculateFloat.floatAdd(totalNum, saleItemList[i].quantity || 0);
                    }
                    record.totalNum = totalNum;
                    that.record = record;
                });
            },
            savePrintParams: function () {
                var that = this;
                var params = JSON.stringify(this.printParams);
                $.http.post('${params.contextPath}/web/printParams/save.json', {
                    type: 'sale_order',
                    params: params
                }).then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    window.print();
                });
            },
        }
    });
</script>
</body>

</html>