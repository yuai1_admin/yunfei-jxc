package com.yunfeisoft.business.dao.impl.postgres;

import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.SelectBuilder;
import com.applet.sql.builder.WhereBuilder;
import com.applet.sql.mapper.DefaultRowMapper;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.AllotItemDao;
import com.yunfeisoft.business.model.*;
import com.yunfeisoft.model.User;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * ClassName: AllotItemDaoImpl
 * Description: 调拨单商品信息Dao实现
 * Author: Jackie liu
 * Date: 2020-08-06
 */
@Repository
public class AllotItemDaoImpl extends ServiceDaoImpl<AllotItem, String> implements AllotItemDao {

    @Override
    public Page<AllotItem> queryPage(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        wb.setOrderByWithDesc("ao.allotDate");
        if (params != null) {
            initPageParam(wb, params);
            wb.andEquals("ai.orgId", params.get("orgId"));
            wb.andEquals("p.id", params.get("productId"));
            wb.andFullLike("p.name", params.get("productName"));
        }

        SelectBuilder builder = getSelectBuilder("ai");
        builder.column("wo.name as outWarehouseName")
                .column("wi.name as inWarehouseName")
                .column("p.code as productCode")
                .column("p.name as productName")
                .column("p.standard as productStandard")
                .column("p.unit as productUnit")
                .column("ao.allotDate as allotDate")
                .column("ao.code as orderCode")
                .column("u.name as createName")
                .join(Product.class).alias("p").on("ai.productId = p.id").build()
                .join(AllotOrder.class).alias("ao").on("ai.allotOrderId = ao.id").build()
                .join(Warehouse.class).alias("wo").on("ao.outWarehouseId = wo.id").build()
                .join(Warehouse.class).alias("wi").on("ao.inWarehouseId = wi.id").build()
                .join(User.class).alias("u").on("ai.createId = u.id").build();

        return queryPage(builder.getSql(), wb);
    }

    @Override
    public List<AllotItem> queryByAllotOrderId(String orderId) {
        if (StringUtils.isBlank(orderId)) {
            return new ArrayList<>();
        }

        SelectBuilder builder = getSelectBuilder("ai");
        builder.column("p.code as productCode")
                .column("p.name as productName")
                .column("p.standard as productStandard")
                .column("p.unit as productUnit")
                .join(Product.class).alias("p").on("ai.productId = p.id").build();

        WhereBuilder wb = new WhereBuilder();
        wb.andEquals("ai.allotOrderId", orderId);

        return query(builder.getSql(), wb);
    }

    @Override
    public int removeByAllotOrderId(String orderId) {
        if (StringUtils.isBlank(orderId)) {
            return 0;
        }

        WhereBuilder wb = new WhereBuilder();
        wb.andEquals("allotOrderId", orderId);
        return deleteByCondition(wb);
    }

    @Override
    public List<OrderItemStatistics> queryStatistics(String orgId, String productId) {
        String sql = "SELECT TO_CHAR(ao.allot_date_, 'YYYY-MM-DD') AS DAY_, SUM (ai.quantity_) AS TOTAL_QUANTITY_" +
                " FROM TT_ALLOT_ITEM ai join tt_allot_order ao on ai.allot_order_id_ = ao.id_" +
                " WHERE aI.IS_DEL_ = 2 AND aO.STATUS_ = ? AND aI.ORG_ID_ = ? AND aI.PRODUCT_ID_ = ? GROUP BY DAY_";

        return jdbcTemplate.query(sql, new Object[]{AllotOrder.AllotOrderStatusEnum.ALLOTED.getValue(), orgId, productId}, new DefaultRowMapper<OrderItemStatistics>(OrderItemStatistics.class));
    }
}