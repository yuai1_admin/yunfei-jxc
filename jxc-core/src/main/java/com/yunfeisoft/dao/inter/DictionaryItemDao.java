/**
 * DictionaryItemDao.java
 * Created at 2017-07-06
 * Created by Jackie liu
 * Copyright (C) 2014, All rights reserved.
 */
package com.yunfeisoft.dao.inter;

import com.applet.base.BaseDao;
import com.applet.utils.Page;
import com.yunfeisoft.model.DictionaryItem;

import java.util.List;
import java.util.Map;

/**
 * <p>ClassName: DictionaryItemDao</p>
 * <p>Description: 字典项管理Dao</p>
 * <p>Author: Jackie liu</p>
 * <p>Date: 2017-07-06</p>
 */
public interface DictionaryItemDao extends BaseDao<DictionaryItem, String> {

	public Page<DictionaryItem> queryPage(Map<String, Object> params);

	public int removeByDicId(String[] dicIds);

	/**
	 * 查询有效的字典项列表
	 * @param dictionaryValue 字典值
	 * @return
	 */
	public List<DictionaryItem> queryAcceptList(String dictionaryValue);

}