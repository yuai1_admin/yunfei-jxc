package com.yunfeisoft.dao.inter;

import com.applet.base.BaseDao;
import com.applet.utils.Page;
import com.yunfeisoft.model.ProcessTask;

import java.util.Map;

/**
 * ClassName: ProcessTaskDao
 * Description: 流程任务(userTask)信息Dao
 * Author: Jackie liu
 * Date: 2019-07-08
 */
public interface ProcessTaskDao extends BaseDao<ProcessTask, String> {

    public Page<ProcessTask> queryPage(Map<String, Object> params);

    public boolean isDuplicateKey(String id, String processKey, String key);
}