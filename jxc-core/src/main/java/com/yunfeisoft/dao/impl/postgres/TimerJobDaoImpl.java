/**
 * TimerJobDaoImpl.java
 * Created at 2017-09-14
 * Created by Jackie liu
 * Copyright (C) 2014, All rights reserved.
 */
package com.yunfeisoft.dao.impl.postgres;

import com.applet.base.BaseDaoImpl;
import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.WhereBuilder;
import com.applet.utils.Page;
import com.driver.dao.inter.TimerJobDao;
import com.yunfeisoft.model.TimerJob;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * <p>ClassName: TimerJobDaoImpl</p>
 * <p>Description: 定时器任务Dao实现</p>
 * <p>Author: Jackie liu</p>
 * <p>Date: 2017-09-14</p>
 */
@Repository
public class TimerJobDaoImpl extends BaseDaoImpl<TimerJob, String> implements TimerJobDao {

    @Override
    public boolean isDuplicateName(String id, String name) {
        return isDuplicateField(id, name, "name");
    }

    @Override
    public boolean isDuplicateClassAndMethodName(String id, String className, String methodName) {
        WhereBuilder wb = new WhereBuilder();
        wb.andEquals("className", className);
        wb.andEquals("methodName", methodName);
        wb.andNotEquals("id", id);
        return count(wb) > 0;
    }

    @Override
    public Page<TimerJob> queryPage(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        if (params != null) {
            initPageParam(wb, params);
            wb.andFullLike("name", params.get("name"));
        }
        return queryPage(wb);
    }
}