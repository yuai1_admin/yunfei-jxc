package com.yunfeisoft.business.model;

import com.applet.base.ServiceModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * ClassName: ProductParams
 * Description: 商品参数信息
 *
 * @Author: Jackie liu
 * Date: 2020-07-23
 */
@Entity
@Table(name = "TT_PRODUCT_PARAMS")
public class ProductParams extends ServiceModel implements Serializable {

    /**
     * Field serialVersionUID: 序列号
     */
    private static final long serialVersionUID = 1L;

    /**
     * 组织id
     */
    @Column
    private String orgId;

    /**
     * 类别(1规格,2单位,3收入项目,4支出项目,5交易对象)
     */
    @Column
    private Integer type;

    /**
     * 名称
     */
    @Column
    private String name;

    /**
     * 备注
     */
    @Column
    private String remark;

    public String getTypeStr() {
        return ParamsTypeEnum.valueOf(type);
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 类别(1规格,2单位,3收入项目,4支出项目,5交易对象)
     */
    public enum ParamsTypeEnum {

        STANDARD(1, "商品规格"),
        UNIT(2, "商品单位"),
        IN_ITEM(3, "收入项目"),
        OUT_ITEM(4, "支出项目"),
        TRADING_PARTNER(5, "交易对象");

        private int value;
        private String label;

        private ParamsTypeEnum(int value, String label) {
            this.value = value;
            this.label = label;
        }

        public static String valueOf(Integer value) {
            if (value == null) {
                return null;
            }
            for (ParamsTypeEnum loop : ParamsTypeEnum.values()) {
                if (value == loop.getValue()) {
                    return loop.getLabel();
                }
            }
            return null;
        }

        public int getValue() {
            return value;
        }

        public String getLabel() {
            return label;
        }
    }
}