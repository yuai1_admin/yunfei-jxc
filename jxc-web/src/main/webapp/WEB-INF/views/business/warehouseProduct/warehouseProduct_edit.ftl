<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>编辑仓库商品信息</title>
	<#include "/common/vue_resource.ftl">
</head>
<body>
<div id="app" v-cloak>
	<div class="ui-form">
		<form class="layui-form" @submit.prevent="submitForm()" method="post">
			<div class="layui-form-item">
				<label class="layui-form-label">主键<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.id" placeholder="请输入主键" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">商品id<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.productId" placeholder="请输入商品id" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">仓库id<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.warehouseId" placeholder="请输入仓库id" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">库存数量<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.stock" placeholder="请输入库存数量" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">缺货下限<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.shortageLimit" placeholder="请输入缺货下限" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">积压上限<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.backlogLimit" placeholder="请输入积压上限" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">是否删除(1是，2否)<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.isDel" placeholder="请输入是否删除(1是，2否)" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">创建人id<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.createId" placeholder="请输入创建人id" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">创建时间<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.createTime" placeholder="请输入创建时间" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">修改人id<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.modifyId" placeholder="请输入修改人id" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">修改时间<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.modifyTime" placeholder="请输入修改时间" class="layui-input"/>
				</div>
			</div>

			<div class="layui-form-item">
				<div class="layui-input-block">
					<input type="submit" value="保存" class="layui-btn" />
				</div>
			</div>
		</form>
	</div>
</div>
<script>
	var app = new Vue({
		el: '#app',
		data: {
			showTypes: false,
			record : {
				id:'${params.id!}',
				id:'',
				productId:'',
				warehouseId:'',
				stock:'',
				shortageLimit:'',
				backlogLimit:'',
				isDel:'',
				createId:'',
				createTime:'',
				modifyId:'',
				modifyTime:'',
			},
		},
		mounted: function () {
			this.init();
			this.loadData();
		},
		methods: {
			init:function(){
				var that = this;
				/*laydate.render({elem: '#beginDate', type:'datetime', done:function (value) {
						that.record.beginDate = value;
					}});*/

			},
			loadData: function () {
				if (!'${params.id!}') {
					return;
				}
				var that = this;
				$.http.post('${params.contextPath}/web/warehouseProduct/query.json', {id: '${params.id!}'}).then(function (data) {
					if (!data.success) {
						$.message(data.message);
						return;
					}
					var item = data.data;
					for (var key in  that.record) {
						that.record[key] = item[key];
					}
				});
			},
			submitForm: function () {
				$.http.post('${params.contextPath}/web/warehouseProduct/<#if (params.id)??>modify<#else>save</#if>.json', this.record).then(function (data) {
					if (!data.success) {
						$.message(data.message);
						return;
					}
					var alt = layer.alert(data.message || "操作成功", function () {
						parent.app.loadData();
						parent.layer.closeAll();
						layer.close(alt);
					});
				});
			},
		}
	});
</script>
</body>

</html>
