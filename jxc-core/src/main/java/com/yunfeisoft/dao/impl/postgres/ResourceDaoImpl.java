/**
 * ResourceDaoImpl.java
 * Created at 2017-07-06
 * Created by Jackie liu
 * Copyright (C) 2014, All rights reserved.
 */
package com.yunfeisoft.dao.impl.postgres;

import com.applet.base.BaseDaoImpl;
import com.applet.sql.builder.WhereBuilder;
import com.applet.utils.Page;
import com.yunfeisoft.dao.inter.ResourceDao;
import com.yunfeisoft.model.Resource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * <p>ClassName: ResourceDaoImpl</p>
 * <p>Description: 资源管理Dao实现</p>
 * <p>Author: Jackie liu</p>
 * <p>Date: 2017-07-06</p>
 */
@Repository
public class ResourceDaoImpl extends BaseDaoImpl<Resource, String> implements ResourceDao {

    @Override
    public List<Resource> query(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        if (params != null) {
            wb.andEquals("url", params.get("url"));
            wb.andEquals("state", params.get("state"));
        }
        return query(wb);
    }

    @Override
    public Page<Resource> queryPage(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        wb.setOrderByWithAsc("name");
        if (params != null && params.size() > 0) {
            wb.andFullLike("name", params.get("name"));
            initPageParam(wb, params);

            String searchInput = (String) params.get("searchInput");
            if (StringUtils.isNotEmpty(searchInput)) {
                wb.andGroup().andFullLike("name", searchInput)
                        .orFullLike("code", searchInput);
            }
        }
        Page<Resource> queryPage = queryPage(wb);
        return queryPage;
    }

    @Override
    public int batchRemove(String[] ids) {
        WhereBuilder wb = new WhereBuilder();
        wb.andIn("_ID", ids);
        return deleteByCondition(wb);
    }

    @Override
    public int modifyState(final String[] ids, final int state) {
        String sql = String.format("UPDATE %s SET STATE_=? WHERE ID_=?", domainModelAnalysis.getTableName());
        int[] batchUpdate = jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setInt(1, state);
                ps.setString(2, ids[i]);
            }

            @Override
            public int getBatchSize() {
                return ids.length;
            }
        });
        return batchUpdate.length;
    }

    @Override
    public Boolean isDuplicateName(String id, String name) {
        return super.isDuplicateField(id, name, "name");
    }

    @Override
    public Boolean isDuplicateCode(String id, String code) {
        return super.isDuplicateField(id, code, "code");
    }
}