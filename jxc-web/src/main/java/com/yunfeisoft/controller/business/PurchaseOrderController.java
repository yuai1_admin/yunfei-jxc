package com.yunfeisoft.controller.business;

import com.applet.base.BaseController;
import com.applet.utils.*;
import com.yunfeisoft.business.model.*;
import com.yunfeisoft.business.service.inter.*;
import com.yunfeisoft.model.User;
import com.yunfeisoft.utils.ApiUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.*;

/**
 * ClassName: PurchaseOrderController
 * Description: 采购单信息Controller
 * Author: Jackie liu
 * Date: 2020-07-23
 */
@Controller
public class PurchaseOrderController extends BaseController {

    @Autowired
    private PurchaseOrderService purchaseOrderService;
    @Autowired
    private SupplierService supplierService;
    @Autowired
    private CodeBuilderService codeBuilderService;
    @Autowired
    private PaymentRecordService paymentRecordService;
    @Autowired
    private PurchaseItemService purchaseItemService;

    /**
     * 添加采购单信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/purchaseOrder/save", method = RequestMethod.POST)
    @ResponseBody
    public Response save(PurchaseOrder record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "supplierId", "供应商为空");
        validator.required(request, "purchaseDate", "采购日期为空");
        validator.number(request, "payAmount", "已收金额不合法");
        validator.required(request, "productsStr", "商品为空");
        validator.required(request, "submitType", "提交方式为空");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }

        User user = ApiUtils.getLoginUser();
        record.setId(KeyUtils.getKey());
        record.setOrgId(user.getOrgId());

        String productsStr = ServletRequestUtils.getStringParameter(request, "productsStr", null);
        List<PurchaseItem> purchaseItems = FastJsonUtils.jsonToList(productsStr, PurchaseItem.class);

        BigDecimal totalAmount = BigDecimal.ZERO;
        Iterator<PurchaseItem> iterator = purchaseItems.iterator();
        while (iterator.hasNext()) {
            PurchaseItem item = iterator.next();
            if (StringUtils.isBlank(item.getProductId())) {
                iterator.remove();
                continue;
            }
            item.setId(KeyUtils.getKey());
            item.setOrgId(user.getOrgId());
            item.setPurchaseOrderId(record.getId());

            if (StringUtils.isBlank(item.getWarehouseId())) {
                return ResponseUtils.warn("含有仓库为空的商品");
            }

            if (item.getQuantity() == null) {
                return ResponseUtils.warn("含有数量不合法或者为空的商品");
            }

            if (item.getPrice() == null) {
                item.setPrice(BigDecimal.ZERO);
                //return ResponseUtils.warn("含有进货价不合法或者为空的商品");
            }

            item.setAmount(item.getPrice().multiply(item.getQuantity()));
            totalAmount = totalAmount.add(item.getAmount());
        }
        record.setTotalAmount(totalAmount);
        record.setPurchaseItemList(purchaseItems);
        if (CollectionUtils.isEmpty(purchaseItems)) {
            return ResponseUtils.warn("商品为空");
        }

        String code = codeBuilderService.generatePurchaseOrderCode(user.getOrgId());
        record.setCode(code);

        String submitType = ServletRequestUtils.getStringParameter(request, "submitType", null);
        if ("settle".equals(submitType)) {//结清保存
            record.setStatus(PurchaseOrder.PurchaseOrderStatusEnum.STORAGED.getValue());
            record.setPayStatus(PurchaseOrder.PurchaseOrderPayStatusEnum.PAID.getValue());
            record.setPayAmount(totalAmount);

            PaymentRecord paymentRecord = new PaymentRecord();
            paymentRecord.setOrgId(user.getOrgId());
            paymentRecord.setSupplierId(record.getSupplierId());
            paymentRecord.setPurchaseOrderId(record.getId());
            paymentRecord.setDueAmount(totalAmount);
            paymentRecord.setPayAmount(record.getPayAmount());
            paymentRecord.setOwedAmount(totalAmount.subtract(record.getPayAmount()));
            paymentRecord.setRemark("录单付款");
            record.setPaymentRecord(paymentRecord);
        } else if ("pay".equals(submitType)) {//收款保存
            record.setStatus(PurchaseOrder.PurchaseOrderStatusEnum.STORAGED.getValue());
            record.setPayStatus(PurchaseOrder.PurchaseOrderPayStatusEnum.TO_BE_CLEARED.getValue());

            //设置付款记录
            record.setPayAmount(BigDecimal.ZERO);
        }

        purchaseOrderService.save(record);
        return ResponseUtils.success("保存成功", record.getId());
    }

    /**
     * 修改采购单信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/purchaseOrder/modify", method = RequestMethod.POST)
    @ResponseBody
    public Response modify(PurchaseOrder record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "id", "参数错误");
        validator.required(request, "supplierId", "供应商为空");
        validator.required(request, "purchaseDate", "采购日期为空");
        validator.number(request, "payAmount", "已收金额不合法");
        validator.required(request, "productsStr", "商品为空");
        validator.required(request, "submitType", "提交方式为空");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }

        /*PurchaseOrder purchaseOrder = purchaseOrderService.load(record.getId());
        if (purchaseOrder.getStatus() != PurchaseOrder.PurchaseOrderStatusEnum.TO_BE_CHANGED.getValue()
                && purchaseOrder.getStatus() != PurchaseOrder.PurchaseOrderStatusEnum.TO_BE_STORAGE.getValue()) {
            return ResponseUtils.warn("该订单状态不是【待入库】，也不是【待更改】，无法修改");
        }*/

        User user = ApiUtils.getLoginUser();

        String productsStr = ServletRequestUtils.getStringParameter(request, "productsStr", null);
        List<PurchaseItem> purchaseItems = FastJsonUtils.jsonToList(productsStr, PurchaseItem.class);

        BigDecimal totalAmount = BigDecimal.ZERO;
        Iterator<PurchaseItem> iterator = purchaseItems.iterator();
        while (iterator.hasNext()) {
            PurchaseItem item = iterator.next();
            if (StringUtils.isBlank(item.getProductId())) {
                iterator.remove();
                continue;
            }
            item.setId(KeyUtils.getKey());
            item.setOrgId(user.getOrgId());
            item.setPurchaseOrderId(record.getId());

            if (StringUtils.isBlank(item.getWarehouseId())) {
                return ResponseUtils.warn("含有仓库为空的商品");
            }

            if (item.getQuantity() == null) {
                return ResponseUtils.warn("含有数量不合法或者为空的商品");
            }

            if (item.getPrice() == null) {
                item.setPrice(BigDecimal.ZERO);
                //return ResponseUtils.warn("含有进货价不合法或者为空的商品");
            }

            item.setAmount(item.getPrice().multiply(item.getQuantity()));
            totalAmount = totalAmount.add(item.getAmount());
        }
        record.setTotalAmount(totalAmount);
        record.setPurchaseItemList(purchaseItems);
        if (CollectionUtils.isEmpty(purchaseItems)) {
            return ResponseUtils.warn("商品为空");
        }

        String submitType = ServletRequestUtils.getStringParameter(request, "submitType", null);
        if ("settle".equals(submitType)) {//结清保存
            record.setStatus(PurchaseOrder.PurchaseOrderStatusEnum.STORAGED.getValue());
            record.setPayStatus(PurchaseOrder.PurchaseOrderPayStatusEnum.PAID.getValue());
            record.setPayAmount(totalAmount);

            PaymentRecord paymentRecord = new PaymentRecord();
            paymentRecord.setOrgId(user.getOrgId());
            paymentRecord.setSupplierId(record.getSupplierId());
            paymentRecord.setPurchaseOrderId(record.getId());
            paymentRecord.setDueAmount(totalAmount);
            paymentRecord.setPayAmount(record.getPayAmount());
            paymentRecord.setOwedAmount(totalAmount.subtract(record.getPayAmount()));
            paymentRecord.setRemark("录单付款");
            record.setPaymentRecord(paymentRecord);
        } else if ("pay".equals(submitType)) {//收款保存
            record.setStatus(PurchaseOrder.PurchaseOrderStatusEnum.STORAGED.getValue());
            record.setPayStatus(PurchaseOrder.PurchaseOrderPayStatusEnum.TO_BE_CLEARED.getValue());

            //设置收款记录
            record.setPayAmount(BigDecimal.ZERO);
        }

        purchaseOrderService.modify(record);
        return ResponseUtils.success("保存成功", record.getId());
    }

    /**
     * 查询采购单信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/purchaseOrder/query", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response query(HttpServletRequest request, HttpServletResponse response) {
        String id = ServletRequestUtils.getStringParameter(request, "id", null);
        if (StringUtils.isBlank(id)) {
            return ResponseUtils.warn("参数错误");
        }
        PurchaseOrder record = purchaseOrderService.load(id);
        if (record != null) {
            Supplier supplier = supplierService.load(record.getSupplierId());
            record.setSupplierName(supplier.getName());
            record.setSupplier(supplier);

            List<PurchaseItem> saleItemList = purchaseItemService.queryByPurchaseOrderId(id);
            record.setPurchaseItemList(saleItemList);

            List<PaymentRecord> paymentRecordList = paymentRecordService.queryByPurchaseOrderId(id);
            record.setPaymentRecordList(paymentRecordList);
        }
        return ResponseUtils.success(record);
    }

    /**
     * 查询采购单信息--分享查询
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/purchaseOrder/share", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response share(HttpServletRequest request, HttpServletResponse response) {
        String id = ServletRequestUtils.getStringParameter(request, "id", null);
        if (StringUtils.isBlank(id)) {
            return ResponseUtils.warn("参数错误");
        }
        PurchaseOrder record = purchaseOrderService.load(id);
        if (record != null) {
            Supplier supplier = supplierService.load(record.getSupplierId());
            record.setSupplierName(supplier.getName());
            record.setSupplier(supplier);

            List<PurchaseItem> saleItemList = purchaseItemService.queryByPurchaseOrderId(id);
            record.setPurchaseItemList(saleItemList);
        }
        return ResponseUtils.success(record);
    }

    /**
     * 查询采购单信息--分享查询
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/purchaseOrder/share", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response share1(HttpServletRequest request, HttpServletResponse response) {
        return share(request, response);
    }

    /**
     * 查询采购单信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/purchaseOrder/querySingle", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response querySingle(HttpServletRequest request, HttpServletResponse response) {
        String id = ServletRequestUtils.getStringParameter(request, "id", null);
        if (StringUtils.isBlank(id)) {
            return ResponseUtils.warn("参数错误");
        }
        PurchaseOrder record = purchaseOrderService.load(id);
        return ResponseUtils.success(record);
    }

    /**
     * 分页查询采购单信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/purchaseOrder/list", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response list(HttpServletRequest request, HttpServletResponse response) {
        String supplierName = ServletRequestUtils.getStringParameter(request, "supplierName", null);
        int payStatus = ServletRequestUtils.getIntParameter(request, "payStatus", -1);

        User user = ApiUtils.getLoginUser();

        Map<String, Object> params = new HashMap<String, Object>();
        initParams(params, request);
        params.put("orgId", user.getOrgId());
        params.put("supplierName", supplierName);
        if (payStatus > 0) {
            params.put("payStatus", payStatus);
        }

        Page<PurchaseOrder> page = purchaseOrderService.queryPage(params);
        return ResponseUtils.success(page);
    }

    /**
     * 批量删除采购单信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/purchaseOrder/delete", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response delete(HttpServletRequest request, HttpServletResponse response) {
        String ids = ServletRequestUtils.getStringParameter(request, "ids", null);
        if (StringUtils.isBlank(ids)) {
            return ResponseUtils.warn("参数错误");
        }

        String[] idArr = ids.split(",");
        /*for (String id : idArr) {
            PurchaseOrder purchaseOrder = purchaseOrderService.load(id);
            if (purchaseOrder.getStatus() != PurchaseOrder.PurchaseOrderStatusEnum.TO_BE_CHANGED.getValue()
                    && purchaseOrder.getStatus() != PurchaseOrder.PurchaseOrderStatusEnum.TO_BE_STORAGE.getValue()) {
                return ResponseUtils.warn("该订单状态不是【待入库】，也不是【待更改】，无法修改");
            }
        }*/

        purchaseOrderService.remove(idArr);
        return ResponseUtils.success("删除成功");
    }

    /**
     * 改单解锁
     *
     * @param request
     * @param response
     * @return
     */
    /*@RequestMapping(value = "/web/purchaseOrder/changeOrder", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response changeOrder(HttpServletRequest request, HttpServletResponse response) {
        String id = ServletRequestUtils.getStringParameter(request, "id", null);
        if (StringUtils.isBlank(id)) {
            return ResponseUtils.warn("参数错误");
        }
        PurchaseOrder purchaseOrder = purchaseOrderService.load(id);
        if (purchaseOrder.getStatus() != PurchaseOrder.PurchaseOrderStatusEnum.STORAGED.getValue()) {
            return ResponseUtils.success("success");
        }

        purchaseOrderService.modifyWithUnLock(id, purchaseOrder);
        return ResponseUtils.success("success");
    }*/

    /**
     * 采购单汇总查询
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/purchaseOrder/statistics", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response statistics(HttpServletRequest request, HttpServletResponse response) {
        String type = ServletRequestUtils.getStringParameter(request, "type", null);
        if (StringUtils.isBlank(type)) {
            return ResponseUtils.warn("统计类型为空");
        }

        String beginDate = ServletRequestUtils.getStringParameter(request, "beginDate", null);
        String endDate = ServletRequestUtils.getStringParameter(request, "endDate", null);
        if (StringUtils.isBlank(beginDate)) {
            beginDate = DateUtils.getNowTime() + " 00:00:00";
        }

        if (StringUtils.isBlank(endDate)) {
            endDate = DateUtils.getNowTime() + " 23:59:59";
        }

        User user = ApiUtils.getLoginUser();

        List<PurchaseOrderStatistics> list = null;
        if ("category".equals(type)) {
            list = purchaseOrderService.queryStatisticsByProductCategory(user.getOrgId(), beginDate, endDate);
        } else if ("product".equals(type)) {
            list = purchaseOrderService.queryStatisticsByProduct(user.getOrgId(), beginDate, endDate);
        } else if ("supplier".equals(type)) {
            list = purchaseOrderService.queryStatisticsBySupplier(user.getOrgId(), beginDate, endDate);
        } else if ("create".equals(type)) {
            list = purchaseOrderService.queryStatisticsByCreateId(user.getOrgId(), beginDate, endDate);
        }

        return ResponseUtils.success(list);
    }

    /**
     * 按日期订单汇总信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/purchaseOrder/datestatistics", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response datestatistics(HttpServletRequest request, HttpServletResponse response) {
        String type = ServletRequestUtils.getStringParameter(request, "purchaseType", null);

        String beginDate = null;
        String endDate = null;
        if ("self".equals(type)) {
            String nowDate = DateUtils.getNowTime();
            beginDate = ServletRequestUtils.getStringParameter(request, "purchaseBeginDate", null);
            endDate = ServletRequestUtils.getStringParameter(request, "purchaseEndDate", null);
            if (StringUtils.isBlank(beginDate)) {
                beginDate = nowDate + " 00:00:00";
            }

            if (StringUtils.isBlank(endDate)) {
                endDate = nowDate + " 23:59:59";
            }
        } else if ("week".equals(type)) {
            Date nowDate = new Date();
            Date firstDayOfWeek = DateUtils.getFirstDayOfWeek(nowDate);
            Date lastDayOfWeek = DateUtils.getLastDayOfWeek(nowDate);

            beginDate = new DateTime(firstDayOfWeek).toString("yyyy-MM-dd") + " 00:00:00";
            endDate = new DateTime(lastDayOfWeek).toString("yyyy-MM-dd") + " 23:59:59";
        } else if ("month".equals(type)) {
            Date nowDate = new Date();
            beginDate = DateUtils.getFirstDayOfMonth(nowDate) + " 00:00:00";
            endDate = DateUtils.getLastDayOfMonthStr(nowDate) + " 23:59:59";
        }

        if (StringUtils.isBlank(beginDate) || StringUtils.isBlank(endDate)) {
            return ResponseUtils.warn("日期参数为空");
        }

        User user = ApiUtils.getLoginUser();

        List<PurchaseOrderStatistics> purchaseOrderStatistics = purchaseOrderService.queryStatisticsByDate(user.getOrgId(), beginDate, endDate);

        return ResponseUtils.success(purchaseOrderStatistics);
    }

    /**
     * 统计所有的待付款金额
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/purchaseOrder/totalPayAmount", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response totalPayAmount(HttpServletRequest request, HttpServletResponse response) {
        User user = ApiUtils.getLoginUser();
        PurchaseOrder purchaseOrder = purchaseOrderService.queryTotalAmount(user.getOrgId(), PurchaseOrder.PurchaseOrderStatusEnum.STORAGED.getValue(), PurchaseOrder.PurchaseOrderPayStatusEnum.TO_BE_CLEARED.getValue());
        BigDecimal totalAmount = purchaseOrder.getTotalAmount() == null ? BigDecimal.ZERO : purchaseOrder.getTotalAmount();
        BigDecimal payAmount = purchaseOrder.getPayAmount() == null ? BigDecimal.ZERO : purchaseOrder.getPayAmount();
        BigDecimal surplusAmount = totalAmount.subtract(payAmount);
        return ResponseUtils.success(surplusAmount);
    }

    /**
     * 检查单据是否是编辑状态，如果是则变成出入库
     *
     * @param request
     * @param response
     * @return
     */
    /*@RequestMapping(value = "/web/purchaseOrder/checkOrder", method = RequestMethod.POST)
    @ResponseBody
    public Response checkOrder(HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "id", "参数错误");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }

        String id = ServletRequestUtils.getStringParameter(request, "id", null);
        purchaseOrderService.modifyWithCheck(id);
        return ResponseUtils.success("保存成功");
    }*/
}
