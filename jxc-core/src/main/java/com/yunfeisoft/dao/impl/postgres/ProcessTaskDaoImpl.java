package com.yunfeisoft.dao.impl.postgres;

import com.applet.base.BaseDaoImpl;
import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.WhereBuilder;
import com.applet.utils.Page;
import com.yunfeisoft.dao.inter.ProcessTaskDao;
import com.yunfeisoft.model.ProcessTask;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * ClassName: ProcessTaskDaoImpl
 * Description: 流程任务(userTask)信息Dao实现
 * Author: Jackie liu
 * Date: 2019-07-08
 */
@Repository
public class ProcessTaskDaoImpl extends BaseDaoImpl<ProcessTask, String> implements ProcessTaskDao {

    @Override
    public Page<ProcessTask> queryPage(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        if (params != null) {
            initPageParam(wb, params);
            wb.andEquals("processKey", params.get("processKey"));
        }
        return queryPage(wb);
    }

    @Override
    public boolean isDuplicateKey(String id, String processKey, String key) {
        WhereBuilder wb = new WhereBuilder();
        wb.andEquals("processKey", processKey);
        wb.andEquals("key", key);
        wb.andNotEquals("id", id);
        return count(wb) > 0;
    }
}