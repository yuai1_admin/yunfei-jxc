package com.yunfeisoft.business.service.impl;

import com.applet.base.BaseServiceImpl;
import com.applet.sql.separation.DataSourceChange;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.IndecItemDao;
import com.yunfeisoft.business.dao.inter.IndecOrderDao;
import com.yunfeisoft.business.dao.inter.WarehouseProductDao;
import com.yunfeisoft.business.model.IndecItem;
import com.yunfeisoft.business.model.IndecOrder;
import com.yunfeisoft.business.model.WarehouseProduct;
import com.yunfeisoft.business.service.inter.IndecOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * ClassName: IndecOrderServiceImpl
 * Description: 损益单信息service实现
 * Author: Jackie liu
 * Date: 2020-08-06
 */
@Service("indecOrderService")
public class IndecOrderServiceImpl extends BaseServiceImpl<IndecOrder, String, IndecOrderDao> implements IndecOrderService {

    @Autowired
    private IndecItemDao indecItemDao;
    @Autowired
    private WarehouseProductDao warehouseProductDao;

    @Override
    @DataSourceChange(slave = true)
    public Page<IndecOrder> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }

    @Override
    public int modifyWithUnLock(String id) {
        //1 回退商品库存
        List<IndecItem> indecItemList = indecItemDao.queryByIndecOrderId(id);
        for (IndecItem item : indecItemList) {
            //仓库商品
            WarehouseProduct warehouseProduct = warehouseProductDao.loadSingle(item.getProductId(), item.getWarehouseId());

            //覆盖仓库商品为之前的库存
            WarehouseProduct product = new WarehouseProduct();
            product.setId(warehouseProduct.getId());
            product.setStock(item.getStock().subtract(item.getIndecQuantity()));
            warehouseProductDao.update(product);
        }

        IndecOrder order = new IndecOrder();
        order.setId(id);
        order.setStatus(IndecOrder.IndecOrderStatusEnum.TO_BE_CHANGED.getValue());
        return getDao().update(order);
    }

    @Override
    public int save(IndecOrder indecOrder) {
        int result = super.save(indecOrder);
        //1 添加商品库存、添加商品记录
        for (IndecItem item : indecOrder.getIndecItemList()) {
            //仓库商品
            WarehouseProduct warehouseProduct = warehouseProductDao.loadSingle(item.getProductId(), item.getWarehouseId());

            //计算损益数量、损益金额
            item.setIndecQuantity(item.getStock().subtract(warehouseProduct.getStock()));
            item.setAmount(item.getIndecQuantity().multiply(item.getPrice()));

            //覆盖仓库商品实际库存
            WarehouseProduct product = new WarehouseProduct();
            product.setId(warehouseProduct.getId());
            product.setStock(item.getStock());
            warehouseProductDao.update(product);
        }
        indecItemDao.batchInsert(indecOrder.getIndecItemList());
        return result;
    }

    @Override
    public int modify(IndecOrder indecOrder) {
        IndecOrder order = getDao().load(indecOrder.getId());
        if (order.getStatus() == IndecOrder.IndecOrderStatusEnum.DELIVERED.getValue()) {
            modifyWithUnLock(order.getId());
        }

        //1 扣减商品库存、删除老商品、添加新商品
        indecItemDao.removeByIndecOrderId(indecOrder.getId());
        //1 添加商品库存、添加商品记录
        for (IndecItem item : indecOrder.getIndecItemList()) {
            //仓库商品
            WarehouseProduct warehouseProduct = warehouseProductDao.loadSingle(item.getProductId(), item.getWarehouseId());

            //计算损益数量、损益金额
            item.setIndecQuantity(item.getStock().subtract(warehouseProduct.getStock()));
            item.setAmount(item.getIndecQuantity().multiply(item.getPrice()));

            //覆盖仓库商品实际库存
            WarehouseProduct product = new WarehouseProduct();
            product.setId(warehouseProduct.getId());
            product.setStock(item.getStock());
            warehouseProductDao.update(product);
        }
        indecItemDao.batchInsert(indecOrder.getIndecItemList());
        return super.modify(indecOrder);
    }

    @Override
    public int remove(String id) {
        IndecOrder indecOrder = load(id);
        if (indecOrder.getStatus() == IndecOrder.IndecOrderStatusEnum.DELIVERED.getValue()) {
            //1 回退商品库存
            List<IndecItem> indecItemList = indecItemDao.queryByIndecOrderId(id);
            for (IndecItem item : indecItemList) {
                //仓库商品
                WarehouseProduct warehouseProduct = warehouseProductDao.loadSingle(item.getProductId(), item.getWarehouseId());

                //覆盖仓库商品为之前的库存
                WarehouseProduct product = new WarehouseProduct();
                product.setId(warehouseProduct.getId());
                product.setStock(item.getStock().subtract(item.getIndecQuantity()));
                warehouseProductDao.update(product);
            }
        }

        //1 删除商品
        indecItemDao.removeByIndecOrderId(id);
        return super.remove(id);
    }

    @Override
    public int remove(String[] ids) {
        for (String id : ids) {
            remove(id);
        }
        return super.remove(ids);
    }
}