package com.yunfeisoft.business.dao.inter;

import com.applet.base.BaseDao;
import com.yunfeisoft.business.model.AllotItem;
import com.applet.utils.Page;
import com.yunfeisoft.business.model.OrderItemStatistics;

import java.util.List;
import java.util.Map;

/**
 * ClassName: AllotItemDao
 * Description: 调拨单商品信息Dao
 * Author: Jackie liu
 * Date: 2020-08-06
 */
public interface AllotItemDao extends BaseDao<AllotItem, String> {

    public Page<AllotItem> queryPage(Map<String, Object> params);

    public List<AllotItem> queryByAllotOrderId(String orderId);

    public int removeByAllotOrderId(String orderId);

    public List<OrderItemStatistics> queryStatistics(String orgId, String productId);
}