<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>调度任务列表</title>
    <#include "/common/resource.ftl">
</head>
<body>

<div class="ui-operation">
    <div class="ui-buttons">
        <div class="layui-btn-group tools">
            <button class="layui-btn layui-btn-normal open-dialog" p="url:'${params.contextPath}/view/timerJob/timerJob_edit.htm',title:'添加调度任务信息',width:'800px',height:'100%'"><i class="fa fa-plus"></i>添加</button>
            <button class="layui-btn layui-btn-normal singleSelected" p="url:'${params.contextPath}/view/timerJob/timerJob_edit.htm',title:'编辑调度任务信息',width:'800px',height:'100%'"><i class="fa fa-pencil"></i>修改 </button>
        </div>
        <div class="layui-btn-group tools">
            <button class="layui-btn layui-btn-normal join-button" reurl="${params.contextPath}/web/timerJob/join.json"><i class="fa fa-sign-in"></i>加入运行</button>
            <button class="layui-btn layui-btn-normal cron-button" reurl="${params.contextPath}/web/timerJob/modifyCron.json"><i class="fa fa-clock-o"></i>更新Cron</button>
            <button class="layui-btn layui-btn-normal unjoin-button" reurl="${params.contextPath}/web/timerJob/removeJob.json"><i class="fa fa-sign-out"></i>移除</button>
            <#--<button class="layui-btn layui-btn-normal singleSelected" p="url:'${params.contextPath}/view/timerJob/timerJob_run.htm',title:'正在运行任务列表',width:'800px',height:'100%'"><i class="fa fa-play"></i>正在运行任务</button>
            <button class="layui-btn layui-btn-normal singleSelected" p="url:'${params.contextPath}/view/timerJob/timerJob_plan.htm',title:'计划中任务列表',width:'800px',height:'100%'"><i class="fa fa-pause"></i>计划中任务</button>
            -->
        </div>

        <div class="ui-searchs">
            <div class="value">
                <select id="timer" style="height:29px;line-height:29px;width:90px;">
                    <option value="0">定时刷新</option>
                    <option value="1">1s</option>
                    <option value="5" selected>5s</option>
                    <option value="10">10s</option>
                    <option value="20">20s</option>
                </select>
            </div>
            <div class="value"><input type="text" name="name" value="" placeholder="名称"/></div>
            <input type="button" value="搜索" class="layui-btn layui-btn-danger search-button"/>
        </div>
    </div>
</div>
<div class="ui-content" style="padding-left: 10px;padding-right:10px;">
    <table id="datagrid" options="url:'${params.contextPath}/web/timerJob/list.json',params:'getSearchParams',dblClickRow:'showDetail'">
        <thead>
            <tr>
                <th data-options="field:'id',checkbox:true"></th>
                <th data-options="field:'name',width:100,align:'left'">名称</th>
                <th data-options="field:'className',width:150,align:'left'">全类名</th>
                <th data-options="field:'methodName',width:150,align:'center'">执行方法名</th>
                <th data-options="field:'cron',width:150,align:'center'">Cron表达式</th>
                <th data-options="field:'isConcurrentStr',width:150,align:'center'">并行运行</th>
                <th data-options="field:'stateStr',width:150,align:'center',formatter:formatState">状态</th>
            </tr>
        </thead>
    </table>
</div>
<script type="text/javascript">
    function getSearchParams() {
        return {name: $("input[name='name']").val()};
    }
    var _refresh_times = 0, _refresh_times_circle;
    function runTimer() {
        window.clearInterval(_refresh_times_circle);
        _refresh_times = parseInt($("#timer option:selected").val());
        if (_refresh_times > 0) {
            _refresh_times_circle = window.setInterval("DataGrid.reload()", _refresh_times * 1000);
        }
    }
    var showDetail = function (index, row) {
        var url = "${params.contextPath}/view/timerJob/timerJob_detail.htm?id=" + row.id;
        DialogManager.openDialog("url:'" + url + "',title:'调度任务详细信息',width:'900px',height:'100%'");
    };
    function formatState(val, row) {
        if (row.state == 4) {
            return '<span class="ui-stop">' + val + '</span>';
        } else {
            return '<span class="ui-accept">' + val + '</span>';
        }
    }
    $(function () {
        runTimer();
        $("#timer").change(function () {
            runTimer();
        });

        $(".join-button, .cron-button, .unjoin-button").click(function () {
            var id = DataGrid.getSelectId();
            if (id == "") {
                $.message("请选中要操作的任务")
                return false;
            }
            var url = $(this).attr("reurl");
            $.ajaxRequest({
                type: 'post',
                url: url,
                data: {id: id},
                success: function (data) {
                    $.message(data.message);
                    if (data.success) {
                        DataGrid.reload();
                    }
                }
            });
        });
    })
</script>
</body>

</html>
