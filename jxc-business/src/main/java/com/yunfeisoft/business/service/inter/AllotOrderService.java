package com.yunfeisoft.business.service.inter;

import com.applet.base.BaseService;
import com.applet.utils.Page;
import com.yunfeisoft.business.model.AllotOrder;

import java.util.Map;

/**
 * ClassName: AllotOrderService
 * Description: 调拨单信息service接口
 * Author: Jackie liu
 * Date: 2020-08-06
 */
public interface AllotOrderService extends BaseService<AllotOrder, String> {

    public Page<AllotOrder> queryPage(Map<String, Object> params);

    public int modifyWithUnLock(String id, AllotOrder allotOrder);
}