package com.yunfeisoft.business.service.inter;

import com.applet.base.BaseService;
import com.yunfeisoft.business.model.OrderItemStatistics;
import com.yunfeisoft.business.model.PurchaseItem;
import com.applet.utils.Page;

import java.util.List;
import java.util.Map;

/**
 * ClassName: PurchaseItemService
 * Description: 采购单商品信息service接口
 * Author: Jackie liu
 * Date: 2020-07-23
 */
public interface PurchaseItemService extends BaseService<PurchaseItem, String> {

    public Page<PurchaseItem> queryPage(Map<String, Object> params);

    public List<PurchaseItem> queryByPurchaseOrderId(String purchaseOrderId);

    public List<OrderItemStatistics> queryStatistics(String orgId, String productId);
}