<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>编辑损益单商品信息</title>
	<#include "/common/vue_resource.ftl">
</head>
<body>
<div id="app" v-cloak>
	<div class="ui-form">
		<form class="layui-form" @submit.prevent="submitForm()" method="post">
			<div class="layui-form-item">
				<label class="layui-form-label">主键<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.id" placeholder="请输入主键" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">组织id<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.orgId" placeholder="请输入组织id" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">损益单id<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.indecOrderId" placeholder="请输入损益单id" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">商品id<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.productId" placeholder="请输入商品id" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">仓库id<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.warehouseId" placeholder="请输入仓库id" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">实际库存<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.stock" placeholder="请输入实际库存" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">损益数量<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.indecQuantity" placeholder="请输入损益数量" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">损益单价<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.price" placeholder="请输入损益单价" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">损益金额<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.amount" placeholder="请输入损益金额" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">备注<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.remark" placeholder="请输入备注" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">是否删除(1是，2否)<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.isDel" placeholder="请输入是否删除(1是，2否)" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">创建人id<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.createId" placeholder="请输入创建人id" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">创建时间<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.createTime" placeholder="请输入创建时间" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">修改人id<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.modifyId" placeholder="请输入修改人id" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">修改时间<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.modifyTime" placeholder="请输入修改时间" class="layui-input"/>
				</div>
			</div>

			<div class="layui-form-item">
				<div class="layui-input-block">
					<input type="submit" value="保存" class="layui-btn" />
				</div>
			</div>
		</form>
	</div>
</div>
<script>
	var app = new Vue({
		el: '#app',
		data: {
			showTypes: false,
			record : {
				id:'${params.id!}',
				id:'',
				orgId:'',
				indecOrderId:'',
				productId:'',
				warehouseId:'',
				stock:'',
				indecQuantity:'',
				price:'',
				amount:'',
				remark:'',
				isDel:'',
				createId:'',
				createTime:'',
				modifyId:'',
				modifyTime:'',
			},
		},
		mounted: function () {
			this.init();
			this.loadData();
		},
		methods: {
			init:function(){
				var that = this;
				/*laydate.render({elem: '#beginDate', type:'datetime', done:function (value) {
						that.record.beginDate = value;
					}});*/

			},
			loadData: function () {
				if (!'${params.id!}') {
					return;
				}
				var that = this;
				$.http.post('${params.contextPath}/web/indecItem/query.json', {id: '${params.id!}'}).then(function (data) {
					if (!data.success) {
						$.message(data.message);
						return;
					}
					var item = data.data;
					for (var key in  that.record) {
						that.record[key] = item[key];
					}
				});
			},
			submitForm: function () {
				$.http.post('${params.contextPath}/web/indecItem/<#if (params.id)??>modify<#else>save</#if>.json', this.record).then(function (data) {
					if (!data.success) {
						$.message(data.message);
						return;
					}
					var alt = layer.alert(data.message || "操作成功", function () {
						parent.app.loadData();
						parent.layer.closeAll();
						layer.close(alt);
					});
				});
			},
		}
	});
</script>
</body>

</html>
