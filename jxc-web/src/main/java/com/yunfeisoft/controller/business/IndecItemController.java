package com.yunfeisoft.controller.business;

import com.applet.base.BaseController;
import com.applet.utils.Page;
import com.applet.utils.Response;
import com.applet.utils.ResponseUtils;
import com.yunfeisoft.business.model.IndecItem;
import com.yunfeisoft.business.service.inter.IndecItemService;
import com.yunfeisoft.model.User;
import com.yunfeisoft.utils.ApiUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * ClassName: IndecItemController
 * Description: 损益单商品信息Controller
 * Author: Jackie liu
 * Date: 2020-08-06
 */
@Controller
public class IndecItemController extends BaseController {

    @Autowired
    private IndecItemService indecItemService;

    /**
     * 添加损益单商品信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    /*@RequestMapping(value = "/web/indecItem/save", method = RequestMethod.POST)
    @ResponseBody
    public Response save(IndecItem record, HttpServletRequest request, HttpServletResponse response) {
        indecItemService.save(record);
        return ResponseUtils.success("保存成功");
    }*/

    /**
     * 修改损益单商品信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    /*@RequestMapping(value = "/web/indecItem/modify", method = RequestMethod.POST)
    @ResponseBody
    public Response modify(IndecItem record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "id", "参数错误");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }
        indecItemService.modify(record);
        return ResponseUtils.success("保存成功");
    }*/

    /**
     * 查询损益单商品信息
     *
     * @param request
     * @param response
     * @return
     */
    /*@RequestMapping(value = "/web/indecItem/query", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response query(HttpServletRequest request, HttpServletResponse response) {
        String id = ServletRequestUtils.getStringParameter(request, "id", null);
        if (StringUtils.isBlank(id)) {
            return ResponseUtils.warn("参数错误");
        }
        IndecItem record = indecItemService.load(id);
        return ResponseUtils.success(record);
    }*/

    /**
     * 分页查询损益单商品信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/indecItem/list", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response list(HttpServletRequest request, HttpServletResponse response) {
        String productId = ServletRequestUtils.getStringParameter(request, "productId", null);

        User user = ApiUtils.getLoginUser();

        Map<String, Object> params = new HashMap<String, Object>();
        initParams(params, request);
        params.put("productId", productId);
        params.put("orgId", user.getOrgId());

        Page<IndecItem> page = indecItemService.queryPage(params);
        return ResponseUtils.success(page);
    }

    /**
     * 批量删除损益单商品信息
     *
     * @param request
     * @param response
     * @return
     */
    /*@RequestMapping(value = "/web/indecItem/delete", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response delete(HttpServletRequest request, HttpServletResponse response) {
        String ids = ServletRequestUtils.getStringParameter(request, "ids", null);
        if (StringUtils.isBlank(ids)) {
            return ResponseUtils.warn("参数错误");
        }
        indecItemService.remove(ids.split(","));
        return ResponseUtils.success("删除成功");
    }*/
}
