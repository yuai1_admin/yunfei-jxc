<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>操作日志列表</title>
	<#include "/common/resource.ftl">
</head>
<body>

<div class="ui-operation">
    <#--<div class="ui-search-items layui-form">
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">多规则验证:</label>
                <div class="layui-input-inline">
                    <input type="text" name="number" class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">验证日期</label>
                <div class="layui-input-inline">
                    <input type="text" name="date" class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">验证链接</label>
                <div class="layui-input-inline">
                    <input type="tel" name="url" class="layui-input">
                </div>
            </div>
        </div>
    </div>-->
    <div class="ui-buttons" style="height:28px;">
        <#--<div class="layui-btn-group tools">
            <@auth code='dailyRecord_add'><button class="layui-btn layui-btn-normal open-dialog" p="url:'${params.contextPath}/view/business/dailyRecord/dailyRecord_edit.htm',title:'添加操作日志',width:'600px',height:'400px'"><i class="fa fa-plus"></i>添加</button></@auth>
            <@auth code='dailyRecord_update'><button class="layui-btn layui-btn-normal singleSelected" p="url:'${params.contextPath}/view/business/dailyRecord/dailyRecord_edit.htm',title:'编辑操作日志',width:'600px',height:'400px'"><i class="fa fa-pencil"></i>修改</button></@auth>
            <@auth code='dailyRecord_delete'><button class="layui-btn layui-btn-normal remove-button" reurl="${params.contextPath}/web/dailyRecord/delete.json"><i class="fa fa-remove"></i>删除</button></@auth>
        </div>-->
        <div class="ui-searchs">
            <div class="value"><input type="text" name="userAccount" value="" placeholder="用户账号"/></div>
            <input type="button" value="搜索" class="layui-btn layui-btn-danger search-button"/>
        </div>
    </div>
</div>
<div class="ui-content" style="padding-left: 10px;padding-right:10px;">
    <table id="datagrid" options="url:'${params.contextPath}/web/dailyRecord/list.json',params:'getSearchParams',dblClickRow:'showDetail'">
        <thead>
			<tr>
				<th data-options="field:'id',checkbox:true"></th>
				<th data-options="field:'orgName',width:100,align:'left'">企业名称</th>
				<th data-options="field:'deptName',width:100,align:'left'">部门名称</th>
				<th data-options="field:'userName',width:100,align:'left'">用户名称</th>
				<th data-options="field:'userAccount',width:100,align:'left'">用户账号</th>
				<th data-options="field:'userRoles',width:100,align:'left'">用户角色</th>
				<th data-options="field:'operateType',width:100,align:'left'">操作类型</th>
				<th data-options="field:'remark',width:100,align:'left'">操作说明</th>
				<th data-options="field:'ip',width:100,align:'left'">用户IP</th>
				<th data-options="field:'createTimeStr',width:100,align:'left'">操作时间</th>
			</tr>
        </thead>
    </table>
</div>
<script type="text/javascript">
    function getSearchParams() {
        return {userAccount: $("input[name='userAccount']").val()};
    }
    var showDetail = function (index, row) {
        <@auth code='dailyRecord_detail'>
            var url = "${params.contextPath}/view/dailyRecord/dailyRecord_detail.htm?id=" + row.id;
            DialogManager.openDialog("url:'" + url + "',title:'操作日志详情',width:'70%',height:'70%'");
        </@auth>
    };
    //formatter:formatState
    function formatState(val, row) {
    }
</script>
</body>

</html>
