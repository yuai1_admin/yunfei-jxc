<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>销售记录列表</title>
	<#include "/common/vue_resource.ftl">
</head>
<body>
<div id="app" v-cloak>
    <div class="app-container" @click="hideMenu">
        <#--<div class="layui-row app-header">
            &lt;#&ndash;<div class="layui-col-md3">
                <button type="button" class="layui-btn layui-btn-sm" @click="add">创建销售记录</button>
            </div>&ndash;&gt;
            <div class="layui-col-md12 text-right">
                <input type="text" v-model="params.productName" placeholder="商品名称" class="layui-input">
                <button type="button" class="layui-btn layui-btn-sm layui-btn-primary search-button" @click="seachData">查询</button>
            </div>
        </div>-->
        <div class="app-list">
            <div class="layui-row">
                <div class="layui-col-md9">
                    <div class="app-table-num"><span class="num">销售记录列表(共 {{total}} 条)</span></div>
                </div>
                <div class="layui-col-md3 text-right">
                    <span class="prev" @click="loadPrev">上一页</span>
                    <span class="next" @click="loadNext">下一页</span>
                </div>
            </div>
            <table class="layui-table" lay-even lay-skin="nob" lay-size1="sm">
                <thead>
                <tr>
                    <th style="width:20px;">#</th>
                    <th>销售时间</th>
                    <th>销售单号</th>
                    <th>数量</th>
                    <@auth code='saleOrder_column_price'>
                        <th>售价</th>
                        <th>成本</th>
                        <th>折扣</th>
                        <th>金额</th>
                        <th>毛利</th>
                    </@auth>
                    <th>客户</th>
                    <th>制单人</th>
                    <th>备注</th>
                    <#--<th style="width:215px;">操作</th>-->
                </tr>
                </thead>
                <tbody>
                <tr v-for="(item, index) in rows" @dblclick="showDetail(index)" style="cursor:pointer;">
                    <td>{{20 * (params.page - 1) + 1 + index}}</td>
                    <td>{{item.saleDate}}</td>
                    <td>{{item.orderCode}}</td>
                    <td>{{item.quantity}}</td>
                    <@auth code='saleOrder_column_price'>
                        <td>￥{{item.salePrice}}</td>
                        <td>￥{{item.price}}</td>
                        <td>{{item.discount}}</td>
                        <td>￥{{item.saleAmount}}</td>
                        <td>￥{{item.costAmount}}</td>
                    </@auth>
                    <td>{{item.customerName}}</td>
                    <td>{{item.createName}}</td>
                    <td>{{item.remark}}</td>
                    <#--<td class="more-parent">
                        <div class="ui-operating" @click="modify(index)">编辑</div>
                        <div class="ui-split"></div>
                        <div class="ui-operating" @click.stop="showMenu(index)">更多</div>
                        <div class="more-container" v-if="item.showMenu">
                            <div class="more-item" v-if="item.status == 1" @click="remove(index)">删除</div>
                        </div>
                    </td>-->
                </tr>
                <tr v-if="rows.length <= 0">
                    <td colspan="12" class="text-center">没有更多数据了...</td>
                </tr>
                </tbody>
            </table>
            <div class="layui-row">
                <div class="layui-col-md6">
                    <div class="app-table-num"><span class="num">共 {{total}} 条</span></div>
                </div>
                <div class="layui-col-md6 text-right">
                    <span class="prev" @click="loadPrev">上一页</span>
                    <span class="next" @click="loadNext">下一页</span>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            params: {
                productId:'${params.productId!}',
                page: 1,
            },
            rows: [],
            total: 0,
        },
        mounted: function () {
            this.loadData();
        },
        methods: {
            seachData:function(){
                this.params.page = 1;
                this.$nextTick(function () {
                    this.loadData();
                });
            },
            loadData: function () {
                var that = this;
                $.http.post("${params.contextPath}/web/saleItem/list.json", this.params).then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    that.rows = data.rows;
                    that.total = data.total;
                });
            },
            loadNext: function () {
                if (this.rows.length < 20 && this.rows.length > 0) {
                    return;
                }
                this.params.page = this.params.page + 1;
                this.loadData();
            },
            loadPrev: function () {
                if (this.params.page <= 1) {
                    return;
                }
                this.params.page = this.params.page - 1;
                this.loadData();
            },
            showMenu:function (index) {
                this.hideMenu();
                this.$set(this.rows[index], "showMenu", true);
            },
            hideMenu: function () {
                var that = this;
                this.rows.forEach(function (item) {
                    that.$set(item, 'showMenu', false);
                });
            },
            showDetail: function (index) {
                var row = this.rows[index];
                var url = "${params.contextPath!}/view/business/saleOrder/saleOrder_detail.htm?id=" + row.saleOrderId;
                DialogManager.open({url: url, width: '98%', height: '100%', title: '查看销售单'});
            },
        }
    });
</script>
</body>

</html>
