# 云飞进销存系统

#### 介绍
开源进销存系统，支持微信小程序端、电脑端、支持商品扫码、订单商品扫码等

```
1：功能简洁易懂，不懂财务也能轻松上手
2：手机，平板，电脑数据实时同步
3：多账户权限管理，老板一键屏蔽系统进货价格，销售价格。
4：支持多仓库，多门店
5：一键分享进货单，销售单，库存商品
6：手机扫码出库，进库，无需另外购买扫码设备
7：全国各行各业电商大佬实测功能，真正实现外出 就能轻松办公
```

#### 软件演示地址

1.  官网地址：http://yunfeisoft.com
2.  演示地址：https://jxc.yunfeisoft.com
3.  账号：0001   密码：123456a
4.  小程序二维码：<img src="https://jxc.yunfeisoft.com/images/miniapp.jpg" alt="云飞进销存" style="zoom:25%;" width="20%"/>

#### 联系作者

1.  QQ：648850843（添加时请备注“云飞进销存系统”）
2.  系统QQ群：1148468582
3.  作者微信（添加时请备注“云飞进销存系统”）：<img src="https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/kefu.png" alt="云飞进销存" width="25%"/>

#### 软件截图

![云飞进销存软件](https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/date_chart.png)
![云飞进销存软件](https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/web_1.png)
<img src="https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/miniapp_1.jpg" alt="云飞进销存系统" width="33%" align="left"/>
<img src="https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/miniapp_2.jpg" alt="云飞进销存软件" width="33%"  align="left"/>
<img src="https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/miniapp_3.jpg" alt="云飞进销存软件" width="33%" align="left"/>
<img src="https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/miniapp_4.jpg" alt="云飞进销存软件" width="33%" align="left"/>
<img src="https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/miniapp_5.jpg" alt="云飞进销存软件" width="33%" align="left"/>
<img src="https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/miniapp_6.jpg" alt="云飞进销存软件" width="33%" align="left"/>
<img src="https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/miniapp_7.jpg" alt="云飞进销存软件" width="33%" align="left" />

![云飞进销存软件](https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/modify_order.png)
![云飞进销存软件](https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/order.png)
![云飞进销存软件](https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/print.png)
![云飞进销存软件](https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/trace.png)

