package com.yunfeisoft.business.dao.impl.postgres;

import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.WhereBuilder;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.WarehouseUserDao;
import com.yunfeisoft.business.model.WarehouseUser;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * ClassName: WarehouseUserDaoImpl
 * Description: 仓库用户信息Dao实现
 * Author: Jackie liu
 * Date: 2020-08-04
 */
@Repository
public class WarehouseUserDaoImpl extends ServiceDaoImpl<WarehouseUser, String> implements WarehouseUserDao {

    @Override
    public Page<WarehouseUser> queryPage(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        if (params != null) {
            initPageParam(wb, params);
        }
        return queryPage(wb);
    }
}