package com.yunfeisoft.dao.impl.postgres;

import com.applet.base.BaseDaoImpl;
import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.WhereBuilder;
import com.applet.utils.Page;
import com.yunfeisoft.dao.inter.ProcessDao;
import com.yunfeisoft.model.Process;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * ClassName: ProcessDaoImpl
 * Description: 流程信息Dao实现
 * Author: Jackie liu
 * Date: 2019-07-08
 */
@Repository
public class ProcessDaoImpl extends BaseDaoImpl<Process, String> implements ProcessDao {

   @Override
   public Page<Process> queryPage(Map<String, Object> params) {
       WhereBuilder wb = new WhereBuilder();
       if (params != null) {
           initPageParam(wb, params);
           wb.andFullLike("key", params.get("key"));
           wb.andFullLike("name", params.get("name"));
       }
       return queryPage(wb);
   }

    @Override
    public boolean isDuplicateKey(String id, String key) {
        return isDuplicateField(id, key, "key");
    }
}