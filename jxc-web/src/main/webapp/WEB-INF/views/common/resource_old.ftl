<link href="${params.contextPath}/css/common.css" rel="stylesheet" type="text/css"/>
<link href="${params.contextPath}/css/style.css" rel="stylesheet" type="text/css"/>
<link href="${params.contextPath}/common/easyui/themes/bootstrap/easyui.css" rel="stylesheet" type="text/css"/>
<link href="${params.contextPath}/common/layui/css/layui.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="${params.contextPath}/common/fontawesome/css/font-awesome.min.css">

<script type="text/javascript" src="${params.contextPath}/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="${params.contextPath}/js/common.js"></script>
<script type="text/javascript" src="${params.contextPath}/common/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${params.contextPath}/common/easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="${params.contextPath}/common/layui/lay/dest/layui.all.js"></script>
<script type="text/javascript" src="${params.contextPath}/js/jquery.form.js"></script>
<script type="text/javascript" src="${params.contextPath}/js/support.js"></script>