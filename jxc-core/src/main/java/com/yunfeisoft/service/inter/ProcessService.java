package com.yunfeisoft.service.inter;

import com.applet.base.BaseService;
import com.yunfeisoft.model.Process;
import com.applet.utils.Page;

import java.util.Map;

/**
 * ClassName: ProcessService
 * Description: 流程信息service接口
 * Author: Jackie liu
 * Date: 2019-07-08
 */
public interface ProcessService extends BaseService<Process, String> {

    public Page<Process> queryPage(Map<String, Object> params);

    public boolean isDuplicateKey(String id, String key);

    /**
     * 部署流程
     * @param id
     * @return
     */
    public int saveDeploy(String id);

    public int removeDeploy(String id);
}