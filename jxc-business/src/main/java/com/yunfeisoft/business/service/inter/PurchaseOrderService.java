package com.yunfeisoft.business.service.inter;

import com.applet.base.BaseService;
import com.yunfeisoft.business.model.PurchaseOrder;
import com.applet.utils.Page;
import com.yunfeisoft.business.model.PurchaseOrderStatistics;
import com.yunfeisoft.business.model.SaleOrderStatistics;

import java.util.List;
import java.util.Map;

/**
 * ClassName: PurchaseOrderService
 * Description: 采购单信息service接口
 * Author: Jackie liu
 * Date: 2020-07-23
 */
public interface PurchaseOrderService extends BaseService<PurchaseOrder, String> {

    public Page<PurchaseOrder> queryPage(Map<String, Object> params);

    public int modifyWithUnLock(String id, PurchaseOrder purchaseOrder);

    /**
     * 根据商品类别汇总采购数据
     * @param orgId
     * @param beginDate
     * @param endDate
     * @return
     */
    public List<PurchaseOrderStatistics> queryStatisticsByProductCategory(String orgId, String beginDate, String endDate);

    /**
     * 根据商品汇总采购数据
     * @param orgId
     * @param beginDate
     * @param endDate
     * @return
     */
    public List<PurchaseOrderStatistics> queryStatisticsByProduct(String orgId, String beginDate, String endDate);

    /**
     * 根据供应商汇总采购数据
     * @param orgId
     * @param beginDate
     * @param endDate
     * @return
     */
    public List<PurchaseOrderStatistics> queryStatisticsBySupplier(String orgId, String beginDate, String endDate);

    /**
     * 根据制单人汇总采购数据
     * @param orgId
     * @param beginDate
     * @param endDate
     * @return
     */
    public List<PurchaseOrderStatistics> queryStatisticsByCreateId(String orgId, String beginDate, String endDate);

    /**
     * 根据日期统计采购单数、采购总额
     * @param orgId
     * @param beginDate
     * @param endDate
     * @return
     */
    public List<PurchaseOrderStatistics> queryStatisticsByDate(String orgId, String beginDate, String endDate);

    public PurchaseOrder queryTotalAmount(String orgId, int status, int payStatus);

    //public int modifyWithCheck(String id);
}