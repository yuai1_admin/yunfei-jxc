package com.yunfeisoft.model;

import com.applet.base.BaseModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "TC_DICTIONARY")
public class Dictionary extends BaseModel implements Serializable {

    /**
     * <p>Field serialVersionUID: 序列号</p>
     */
    private static final long serialVersionUID = 1L;

    //名称
    @Column
    private String name;

    //值
    @Column
    private String value;

    //状态（1启用，2停用）
    @Column
    private Integer state;

    //描述
    @Column
    private String remark;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    // 状态(1启用，2停用)
    public enum StateEnum {
        START_USING(1, "启用"),
        END_USING(2, "停用");

        private int value;
        private String label;

        StateEnum(int value, String label) {
            this.label = label;
            this.value = value;
        }

        public static String valueOf(int value) {
            for (StateEnum loop : StateEnum.values()) {
                if (value == loop.getValue()) {
                    return loop.getLabel();
                }
            }
            return null;
        }

        public int getValue() {
            return value;
        }

        public String getLabel() {
            return label;
        }
    }

    public String getStateStr() {//状态
        if (state != null) {
            return StateEnum.valueOf(state);
        }
        return null;
    }
}