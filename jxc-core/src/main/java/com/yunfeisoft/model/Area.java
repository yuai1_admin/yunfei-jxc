package com.yunfeisoft.model;

import com.applet.base.BaseModel;
import com.yunfeisoft.enumeration.YesNoEnum;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;


@Entity
@Table(name = "TC_AREA")
public class Area extends BaseModel implements Serializable {

    /**
     * <p>Field serialVersionUID: 序列号</p>
     */
    private static final long serialVersionUID = 1L;

    //上级区域id
    @Column
    private String parentId;

    //区域名称
    @Column
    private String name;

    //排序
    @Column
    private Integer orderBy;

    //区域编码（国标码）
    @Column
    private String code;

    //区域类型
    @Column
    private Integer category;

    //备注
    @Column
    private String remark;

    //状态(1启用，2停用，3删除)
    @Column
    private Integer state;

    //id路径
    @Column
    private String idPath;

    private List<Area> areaList;

    public String getCategoryStr() {
        if (category != null) {
            return AreaCategory.valueOf(category);
        }
        return null;
    }

    public String getStateStr() {
        if (state != null) {
            return YesNoEnum.valueOfValidateLabel(state);
        }
        return null;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(Integer orderBy) {
        this.orderBy = orderBy;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getIdPath() {
        return idPath;
    }

    public void setIdPath(String idPath) {
        this.idPath = idPath;
    }

    public List<Area> getAreaList() {
        return areaList;
    }

    public void setAreaList(List<Area> areaList) {
        this.areaList = areaList;
    }


    public enum AreaCategory {
        PROVINCE(1, "省"),
        CIYT(2, "市"),
        COUNTY(3, "区/县"),
        TOWN(4, "镇"),
        VILLAGE(5, "村"),
        COMMITTEE(6, "居委会/村委会");

        public static String valueOf(int value) {
            AreaCategory[] values = AreaCategory.values();
            for (AreaCategory areaCategoryEnum : values) {
                if (value == areaCategoryEnum.getValue()) {
                    return areaCategoryEnum.getLable();
                }
            }
            return null;
        }

        private int value;
        private String lable;

        public int getValue() {
            return value;
        }

        public String getLable() {
            return lable;
        }

        private AreaCategory(int value, String lable) {
            this.value = value;
            this.lable = lable;
        }

    }

}