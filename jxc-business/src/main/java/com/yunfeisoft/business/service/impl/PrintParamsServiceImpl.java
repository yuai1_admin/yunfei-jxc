package com.yunfeisoft.business.service.impl;

import com.applet.base.BaseServiceImpl;
import com.applet.sql.separation.DataSourceChange;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.PrintParamsDao;
import com.yunfeisoft.business.model.PrintParams;
import com.yunfeisoft.business.service.inter.PrintParamsService;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * ClassName: PrintParamsServiceImpl
 * Description: 打印设置信息service实现
 * Author: Jackie liu
 * Date: 2020-08-18
 */
@Service("printParamsService")
public class PrintParamsServiceImpl extends BaseServiceImpl<PrintParams, String, PrintParamsDao> implements PrintParamsService {

    @Override
    @DataSourceChange(slave = true)
    public Page<PrintParams> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }

    @Override
    public PrintParams loadByType(String orgId, String type) {
        return getDao().loadByType(orgId, type);
    }

    @Override
    public int save(PrintParams printParams) {
        PrintParams params = getDao().loadByType(printParams.getOrgId(), printParams.getType());
        if (params != null) {
            printParams.setId(params.getId());
            return getDao().update(printParams);
        }
        return super.save(printParams);
    }
}