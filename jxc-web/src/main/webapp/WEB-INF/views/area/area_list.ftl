<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="${params.contextPath}/common/ztree/css/zTreeStyle/zTreeStyle.css"/>
<title>区域列表</title> <#include "/common/resource.ftl">
<script type="text/javascript" src="${params.contextPath}/common/ztree/js/jquery.ztree.all.min.js"></script>
</head>
<body>
	<table class="ui-table">
		<tr>
			<td class="ui-table-left">
				<!-- ** 左侧栏 ** -->
				<div class="ui-head">
					<span>区域结构树</span> <span class="right"> <i
						class="fa fa-remove"></i>&nbsp;取消选中
					</span>
				</div>
				<div class="ui-body">
					<ul id="treeDemo" class="ztree"></ul>
				</div>
			</td>
			<td class="ui-table-right" >
				

				<div class="ui-operation">

					<div class="ui-buttons">
						<div class="layui-btn-group tools">
							<button class="layui-btn layui-btn-normal open-dialog" p="url:'${params.contextPath}/view/area/area_edit.htm',title:'添加区域',width:'800px',height:'90%'">
								<i class="fa fa-plus"></i>添加
							</button>
							<button class="layui-btn layui-btn-normal singleSelected" p="url:'${params.contextPath}/view/area/area_edit.htm',title:'编辑区域',width:'800px',height:'90%'">
								<i class="fa fa-pencil"></i>修改
							</button>
							<button class="layui-btn layui-btn-normal remove-button" reurl="${params.contextPath}/web/area/delete.json">
								<i class="fa fa-remove"></i>删除
							</button>
							<button class="layui-btn layui-btn-normal  accept-button" reurl="${params.contextPath}/web/area/accpet.json">
								<i class="fa fa-play"></i>启用
							</button>
							<button class="layui-btn layui-btn-normal  stop-button" reurl="${params.contextPath}/web/area/stop.json">
								<i class="fa fa-stop"></i>停用
							</button>
						</div>
						<div class="ui-searchs">
							<div class="value" style="">
								<input type="text" placeholder="名称  编码" name="searchInput" value="" />
								<input type="hidden" name="parentId" value=""/>
							</div>
							<input type="button" value="搜索" class="layui-btn layui-btn-danger search-button" />
						</div>
					</div>
				</div>
				<div class="ui-content" style="padding-left: 10px;padding-right:10px;">
					<table id="datagrid"  options="url:'${params.contextPath}/web/area/list.json',params:'getSearchParams'">
						<thead>
							<tr>
								<th data-options="field:'ck',checkbox:true"></th>
								<th data-options="field:'name',width:150,align:'left'">区域名称</th>
								<th data-options="field:'code',width:180,align:'left'">区域编码</th>
								<!-- <th data-options="field:'orderBy',width:80,align:'left'">排序</th> -->
 								<th data-options="field:'categoryStr',width:80,align:'left'">区域类型</th>
  								<th data-options="field:'stateStr',width:80,align:'center',formatter:formatState">状态</th>
 							</tr>
						</thead>
					</table>
				</div>
			</td>
		</tr>
	</table>
</body>
<script type="text/javascript">
var getSearchParams = function(){
    return {
        searchInput:$("input[name='searchInput']").val() || "",
        parentId:$("input[name='parentId']").val() || "",
      
    };
}
	
	$(".ui-head .right").click(function(){
        var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
        treeObj.cancelSelectedNode();
        $("input[name='parentId']").val("");
        DataGrid.refresh();
    });
	
	function formatState(val,row){
		if (row.state == 1) {
			return '<span class="ui-accept">' + val + '</span>';
		} else {
			return '<span class="ui-stop">' + val + '</span>';
		}
	}
	
	function treeClick(event, treeId, treeNode, clickFlag) {
         $("input[name='parentId']").val(treeNode.id);
         DataGrid.refresh();
    }
	function beforeDrop(treeId, treeNodes, targetNode, moveType, isCopy) {
        var success = false;
        $.ajaxRequest({
            url:"${params.contextPath}/web/area/drag.json",
            data:{id:treeNodes[0].id, targetId:targetNode.id},
            async:false,
            success:function (data) {
                if (!data.success) {
                    $.message("拖拽失败");
                    return;
                }
                $.message(data.message);
                success = true;
            }
        });
        return success;
    }
	
	function loadTree() {
		var setting = {
			data : {
				simpleData : {
					enable : true,
					idkey : "id",
					pIdKey : "pId"
				}
			},
			edit : {
				drag : {
					autoExpandTrigger : true
				},
				enable : true,
				showRemoveBtn : false,
				showRenameBtn : false
			},
			 callback:{onClick:treeClick,beforeDrop:beforeDrop}
		};

		$.ajaxRequest({
			type : 'post',
			url : '${params.contextPath}/web/area/tree.json',
			async:true,
			success : function(data) {
 				zTree = $.fn.zTree.init($("#treeDemo"), setting, data.data);
				//zTree.expandAll(true); //展开根节点
 			}
		});

	}

	$(function(){
		loadTree();
		$(".remove-button").click(function() {
			var ids = DataGrid.getCheckedIds();
			if (ids == "") {
				layer.msg("请选择删除记录")
				return false;
			}
			var url = $(this).attr("reurl");
			layer.confirm('确定删除记录', function() {
				$.ajaxRequest({
					url : url,
					data : {
						ids : ids.join(",")
					},
					success : function(data) {
						$.message(data.message);
						if (data.success) {
							DataGrid.reload();
						}
					}
				});
			});
		});
		
		$(".stop-button").click(function(){
			var ids = DataGrid.getCheckedIds();
			if (ids == "") {
				layer.msg("请选择停用区域")
				return false;
			}
			var url = $(this).attr("reurl");
			$.ajaxRequest({
				url : url,
				data : {
					ids : ids.join(",")
				},
				success : function(data) {
					$.message(data.message);
					if (data.success) {
						DataGrid.reload();
					}
				}
			});
		});
		
		$(".accept-button").click(function() {
			var ids = DataGrid.getCheckedIds();
			if (ids == "") {
				layer.msg("请选择启用区域")
				return false;
			}
			var url = $(this).attr("reurl");
			$.ajaxRequest({
				url : url,
				data : {
					ids : ids.join(",")
				},
				success : function(data) {
					$.message(data.message);
					if (data.success) {
						DataGrid.reload();
					}
				}
			});
		});
	})
   
</script>
</html>
