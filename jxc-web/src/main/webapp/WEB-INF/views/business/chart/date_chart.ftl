<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>日期汇总</title>
	<#include "/common/vue_resource.ftl">
    <style>
        .small-box{border-radius:2px;padding:10px;color:#fff;margin:10px;}
        .small-box h3{font-size:38px;margin:0 0 10px 0;font-weight:bold;white-space:nowrap;}
        .small-box p{font-size:15px;}
        .layui-card-header {font-size: 18px;}
        .search {float: right;padding-left: 5px;}
        .search input, .search select{height:28px;width:100px;border:1px solid #C9C9C9;border-radius:2px;padding:0px 5px;}
        .chart{height:300px;}
    </style>
</head>
<body>
<div id="app" v-cloak>
    <div class="app-container" style="background:none;">
        <#--今日数据统计-->
        <div class="layui-row" style="padding:10px;background:#fff;">
            <div class="layui-col-xs3 layui-col-sm3 layui-col-md3">
                <div class="small-box" style="background:#00c0ef;">
                    <h3>{{saleNum}}单</h3>
                    <p>今日销售单数</p>
                </div>
            </div>
            <div class="layui-col-xs3 layui-col-sm3 layui-col-md3">
                <div class="small-box" style="background:#00a65a;">
                    <h3>￥{{saleAmount}}</h3>
                    <p>今日销售总额</p>
                </div>
            </div>
            <div class="layui-col-xs3 layui-col-sm3 layui-col-md3">
                <div class="small-box" style="background:#f39c12;">
                    <h3>{{purchaseNum}}单</h3>
                    <p>今日采购单数</p>
                </div>
            </div>
            <div class="layui-col-xs3 layui-col-sm3 layui-col-md3">
                <div class="small-box" style="background:#dd4b39;">
                    <h3>￥{{purchaseAmount}}</h3>
                    <p>今日采购总额</p>
                </div>
            </div>
        </div>

        <div class="layui-card" style="margin-top:20px;">
            <div class="layui-card-header">销售统计
                <button type="button" class="layui-btn layui-btn-sm layui-btn-danger" @click="top.showNotice('date_chart')">使用技巧</button>
                <div class="search">
                    汇总条件：<select v-model="params.saleType">
                        <option value="self">自定义</option>
                        <option value="week">本周</option>
                        <option value="month">本月</option>
                    </select>
                    <input type="text" id="saleBeginDate" v-model="params.saleBeginDate" readonly style="cursor:pointer;background:#F2F2F2;"/> 至
                    <input type="text" id="saleEndDate" v-model="params.saleEndDate" readonly style="cursor:pointer;background:#F2F2F2;"/>
                    <button type="button" class="layui-btn layui-btn-normal layui-btn-xs" style="margin-top:-5px;" @click="loadSaleOrder(true)">查询</button>
                </div>
            </div>
            <div class="layui-card-body chart" id="sale-chart"></div>
        </div>

        <div class="layui-card" style="margin-top:20px;">
            <div class="layui-card-header">采购统计
                <div class="search">
                    汇总条件：<select v-model="params.purchaseType">
                        <option value="self">自定义</option>
                        <option value="week">本周</option>
                        <option value="month">本月</option>
                    </select>
                    <input type="text" id="purchaseBeginDate" v-model="params.purchaseBeginDate" readonly style="cursor:pointer;background:#F2F2F2;"/> 至
                    <input type="text" id="purchaseEndDate" v-model="params.purchaseEndDate" readonly style="cursor:pointer;background:#F2F2F2;"/>
                    <button type="button" class="layui-btn layui-btn-normal layui-btn-xs" style="margin-top:-5px;" @click="loadPurchaseOrder">查询</button>
                </div>
            </div>
            <div class="layui-card-body chart" id="purchase-chart"></div>
        </div>

    </div>
</div>
<script crossorigin="anonymous" integrity="sha384-et+fqdf7kslHL5Ip8rXSJPUPODLa7eMfpFTBaCfnlMzrcAz/wxI5Xm/mNTZwd+7H" src="https://lib.baomitu.com/echarts/4.7.0/echarts.min.js"></script>
<script>
    //销售统计
    var SaleChart = {
        chartObj: null,
        init: function () {
            if (this.chartObj) {
                return this;
            }
            this.chartObj = echarts.init(document.getElementById('sale-chart'));
            return this;
        },
        setOption: function (data) {
            /*if (!data || data.length <= 0) {
                $("#sale-chart").html("暂无数据");
                return;
            }*/
            //日期, 销售总额, 销售单数
            var dates = [], totalAmount = [], totalNum = [];
            for (var i = 0; i < data.length; i ++) {
                var item = data[i];
                dates.push(item.day);
                totalAmount.push(item.totalAmount || 0);
                totalNum.push(item.saleNum || 0);
            }
            this.init();
            var option = {
                title: {text: '销售区域图'},
                tooltip: {trigger: 'axis',axisPointer: {type: 'cross',label: {backgroundColor: '#6a7985'}}},
                legend: {data: ['销售总额', '销售单数']},
                toolbox: {feature: {saveAsImage: {}}},
                grid: {left: '3%',right: '4%',bottom: '3%',containLabel: true},
                xAxis: [{type: 'category',boundaryGap: false,data: dates}],
                yAxis: [{type: 'value'}],
                series: [
                    {name: '销售总额',type: 'line',stack: '总量',areaStyle: {},data: totalAmount},
                    {name: '销售单数',type: 'line',stack: '总量',areaStyle: {},data: totalNum},
                ]
            };
            this.chartObj.setOption(option);
            return this;
        }
    };

    //采购统计
    var PurchaseChart = {
        chartObj: null,
        init: function () {
            if (this.chartObj) {
                return this;
            }
            this.chartObj = echarts.init(document.getElementById('purchase-chart'));
            return this;
        },
        setOption: function (data) {
            /*if (!data || data.length <= 0) {
                $("#purchase-chart").html("暂无数据");
                return;
            }*/
            //日期, 销售总额, 销售单数
            var dates = [], totalAmount = [], totalNum = [];
            for (var i = 0; i < data.length; i ++) {
                var item = data[i];
                dates.push(item.day);
                totalAmount.push(item.totalAmount || 0);
                totalNum.push(item.purchaseNum || 0);
            }
            this.init();
            var option = {
                title: {text: '采购区域图'},
                tooltip: {trigger: 'axis',axisPointer: {type: 'cross',label: {backgroundColor: '#6a7985'}}},
                legend: {data: ['采购总额', '采购单数']},
                toolbox: {feature: {saveAsImage: {}}},
                grid: {left: '3%',right: '4%',bottom: '3%',containLabel: true},
                xAxis: [{type: 'category',boundaryGap: false,data: dates}],
                yAxis: [{type: 'value'}],
                series: [
                    {name: '采购总额',type: 'line',stack: '总量',areaStyle: {},data: totalAmount},
                    {name: '采购单数',type: 'line',stack: '总量',areaStyle: {},data: totalNum},
                ]
            };
            this.chartObj.setOption(option);
            return this;
        }
    };

    var app = new Vue({
        el: '#app',
        data: {
            params: {
                saleBeginDate: '',
                saleEndDate: '',
                purchaseBeginDate: '',
                purchaseEndDate: '',
                purchaseType: 'week',
                saleType: 'week',
            },
            saleNum: '0',
            saleAmount: '0',
            purchaseNum: '0',
            purchaseAmount: '0',
        },
        mounted: function () {
            this.loadNow();

            var that = this;
            laydate.render({elem: '#saleBeginDate', type:'date', done:function (value) {
                    that.params.saleBeginDate = value;
                }});

            laydate.render({elem: '#saleEndDate', type:'date', done:function (value) {
                    that.params.saleEndDate = value;
                }});

            laydate.render({elem: '#purchaseBeginDate', type:'date', done:function (value) {
                    that.params.purchaseBeginDate = value;
                }});

            laydate.render({elem: '#purchaseEndDate', type:'date', done:function (value) {
                    that.params.purchaseEndDate = value;
                }});
        },
        methods: {
            loadNow: function () {
                var that = this;
                $.http.post("${params.contextPath}/web/saleOrder/nowstatistics.json").then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    var result = data.data;
                    var purchaseOrder = result.purchaseOrder || {};
                    var saleOrder = result.saleOrder || {};

                    that.saleNum = saleOrder.saleNum || 0;
                    that.saleAmount = saleOrder.totalAmount || 0;
                    that.purchaseNum = purchaseOrder.purchaseNum || 0;
                    that.purchaseAmount = purchaseOrder.totalAmount || 0;
                    that.loadSaleOrder();
                });
            },
            loadSaleOrder: function (only) {
                var that = this;
                $.http.post("${params.contextPath}/web/saleOrder/datestatistics.json", this.params).then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    SaleChart.setOption(data.data);
                    if (!only) {
                        that.loadPurchaseOrder();
                    }
                });
            },
            loadPurchaseOrder: function () {
                var that = this;
                $.http.post("${params.contextPath}/web/purchaseOrder/datestatistics.json", this.params).then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    PurchaseChart.setOption(data.data);
                });
            }
        }
    });
</script>
</body>

</html>
