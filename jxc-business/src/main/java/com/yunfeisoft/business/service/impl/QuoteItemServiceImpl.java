package com.yunfeisoft.business.service.impl;

import com.applet.base.BaseServiceImpl;
import com.yunfeisoft.business.dao.inter.QuoteItemDao;
import com.yunfeisoft.business.model.QuoteItem;
import com.yunfeisoft.business.service.inter.QuoteItemService;
import com.applet.sql.separation.DataSourceChange;
import com.applet.utils.Page;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * ClassName: QuoteItemServiceImpl
 * Description: 报价单商品信息service实现
 * Author: Jackie liu
 * Date: 2020-09-01
 */
@Service("quoteItemService")
public class QuoteItemServiceImpl extends BaseServiceImpl<QuoteItem, String, QuoteItemDao> implements QuoteItemService {

    @Override
    @DataSourceChange(slave = true)
    public Page<QuoteItem> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }
}