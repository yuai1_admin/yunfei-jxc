<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>聊天记录</title>
    <link rel="stylesheet" type="text/css" href="${params.contextPath}/common/layui/css/layui.css"/>
    <style>
        body .layim-chat-main{height:auto;margin-bottom:30px;}
        #LAY_page{position:fixed;bottom:-12px;margin-left:8px;}
    </style>
</head>
<body>

<div class="layim-chat-main">
    <ul id="LAY_view"></ul>
</div>

<div id="LAY_page"></div>

<textarea title="消息模版" id="LAY_tpl" style="display:none;">
{{# layui.each(d.data, function(index, item){
  if(item.id == parent.layui.layim.cache().mine.id){ }}
    <li class="layim-chat-mine"><div class="layim-chat-user"><img src="{{ item.avatar }}"><cite><i>{{ layui.data.date(item.timestamp) }}</i>{{ item.username }}</cite></div><div class="layim-chat-text">{{ layui.layim.content(item.content) }}</div></li>
  {{# } else { }}
    <li><div class="layim-chat-user"><img src="{{ item.avatar }}"><cite>{{ item.username }}<i>{{ layui.data.date(item.timestamp) }}</i></cite></div><div class="layim-chat-text">{{ layui.layim.content(item.content) }}</div></li>
  {{# }
}); }}
</textarea>

<script type="text/javascript" src="${params.contextPath}/common/layui/layui.js"></script>
<script type="text/javascript">
    var Page = {
        rows:20,
        page:1,
        friendId:'${params.id}'
    };

    //var url = location.search;
    var load = function(isPage){
        $.ajax({
            url:'${params.contextPath}/web/imContent/imList.json',
            data:Page,
            type:"post",
            success:function (data) {
                if (data.code != 0) {
                    layer.msg(data.msg || data.message);
                    return;
                }
                var list = data.data.data;
                if (list.length == 0) {
                    layer.msg("没有更多历史记录");
                    return;
                }
                var html = laytpl(LAY_tpl.value).render({
                    data: list
                });
                $('#LAY_view').html(html);
                if (data.data.total <= Page.rows) {
                    $("#LAY_page").hide();
                    return;
                }

                if (isPage) {
                    laypage.render({
                        elem: 'LAY_page',
                        limit: Page.rows,
                        count: data.data.total,
                        jump: function (obj, first) {
                            Page.page = obj.curr;
                            load(false);
                        }
                    });
                }//end if
            }
        });
    };

    var layim = "", layer = "", laytpl = "", $ = "", laypage = ""
    layui.use(['layim', 'laypage'], function(){
        layim = layui.layim;
        layer = layui.layer;
        laytpl = layui.laytpl;
        $ = layui.jquery;
        laypage = layui.laypage;

        load(true);
    });

</script>
</body>
</html>
