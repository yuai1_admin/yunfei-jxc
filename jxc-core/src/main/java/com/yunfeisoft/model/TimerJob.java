package com.yunfeisoft.model;

import com.applet.base.BaseModel;
import com.applet.utils.FastJsonUtils;
import com.yunfeisoft.enumeration.YesNoEnum;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(name = "TS_TIMER_JOB")
public class TimerJob extends BaseModel implements Serializable {

    /**
     * <p>Field serialVersionUID: 序列号</p>
     */
    private static final long serialVersionUID = 1L;

    //名称
    @Column
    private String name;

    //状态(1未加入，2计划中)
    @Column
    private Integer state;

    //cron表达式
    @Column
    private String cron;

    //任务执行时调用哪个类 包名+类名
    @Column
    private String className;

    //任务调用的方法名
    @Column
    private String methodName;

    //是否并行运行(1是，2否)
    @Column
    private Integer isConcurrent;

    //描述
    @Column
    private String remark;

    //运行异常消息
    private String errorMessage;

    public String getStateStr() {
        if (state != null) {
            return Status.valueOf(state);
        }
        return null;
    }

    public String getIsConcurrentStr() {
        if (isConcurrent != null) {
            return YesNoEnum.valueOf(isConcurrent);
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getCron() {
        return cron;
    }

    public void setCron(String cron) {
        this.cron = cron;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Integer getIsConcurrent() {
        return isConcurrent;
    }

    public void setIsConcurrent(Integer isConcurrent) {
        this.isConcurrent = isConcurrent;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public enum  Status {

        NO_JOIN(1, "未加入"),
        JOIN(2, "计划中"),
        RUNNING(3, "执行中"),
        ERROR(4, "执行异常");

        private final int value;
        private final String label;

        private Status(int value, String label) {
            this.value = value;
            this.label = label;
        }

        public static String valueOf(int value) {
            for (Status s : Status.values()) {
                if (s.getValue() == value) {
                    return s.getLabel();
                }
            }
            return null;
        }

        public int getValue() {
            return value;
        }

        public String getLabel() {
            return label;
        }
    }

    @Override
    public String toString() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("jobId", id);
        map.put("jobName", name);
        map.put("cronExpression", cron);
        map.put("jobClassName", className);
        map.put("jobMethodName", methodName);
        map.put("isConcurrent", false);
        if (isConcurrent == YesNoEnum.YES_ACCPET.getValue()) {
            map.put("isConcurrent", true);
        }
        return FastJsonUtils.objToJson(map);
    }
}