/**
 * TimerJobServiceImpl.java
 * Created at 2017-09-14
 * Created by Jackie liu
 * Copyright (C) 2014 , All rights reserved.
 */
package com.yunfeisoft.service.impl;

import com.applet.base.BaseServiceImpl;
import com.applet.utils.Page;
import com.driver.dao.inter.TimerJobDao;
import com.yunfeisoft.model.TimerJob;
import com.yunfeisoft.service.inter.TimerJobService;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <p>ClassName: TimerJobServiceImpl</p>
 * <p>Description: 定时器任务service实现</p>
 * <p>Author: Jackie liu</p>
 * <p>Date: 2017-09-14</p>
 */
@Service("timerJobService")
public class TimerJobServiceImpl extends BaseServiceImpl<TimerJob, String, TimerJobDao> implements TimerJobService {

    @Override
    public boolean isDuplicateName(String id, String name) {
        return getDao().isDuplicateName(id, name);
    }

    @Override
    public boolean isDuplicateClassAndMethodName(String id, String className, String methodName) {
        return getDao().isDuplicateClassAndMethodName(id, className, methodName);
    }

    @Override
    public Page<TimerJob> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }
}