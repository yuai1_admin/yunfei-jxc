package com.yunfeisoft.business.service.inter;

import com.applet.base.BaseService;
import com.yunfeisoft.business.model.QuoteOrder;
import com.applet.utils.Page;

import java.util.Map;

/**
 * ClassName: QuoteOrderService
 * Description: 报价单信息service接口
 * Author: Jackie liu
 * Date: 2020-09-01
 */
public interface QuoteOrderService extends BaseService<QuoteOrder, String> {

    public Page<QuoteOrder> queryPage(Map<String, Object> params);
}