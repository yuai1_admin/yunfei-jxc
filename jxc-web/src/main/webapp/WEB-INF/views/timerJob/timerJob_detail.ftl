<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>调度任务详细信息</title>
    <#include "/common/resource.ftl">
    <script type="text/javascript">
        $(function () {
        <#if (params.id)??>
            $.ajaxRequest({
                url: '${params.contextPath}/web/timerJob/query.json',
                data: {id: "${params.id}"},
                success: function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    var record = data.data;
                    for (var key in record) {
                        $("[field='" + key + "']").html(record[key]);
                    }
                    if (record.state != 4) {
                        $("[field='errorMessage']").html("--");
                        return;
                    }
                    var errorMessage = record.errorMessage || "";
                    var array = errorMessage.split("\n");
                    var content = "";
                    for (var i = 0; i < array.length; i ++) {
                        var str = $.trim(array[i]);
                        if (i == 0) {
                            content += "<p class='ui-required'><b>{0}</b></p>".format(array[i]);
                            continue;
                        }
                        if (str.startWith("at")) {
                            content += "<p style='padding-left:20px'>{0}</p>".format(array[i]);
                            continue;
                        }
                        content += "<p>{0}</p>".format(array[i]);
                    }
                    $("[field='errorMessage']").html(content);
                }
            });
        </#if>
        });
    </script>
</head>
<body>
<div class="ui-table-div">
    <table class="layui-table ui-table">
        <tr>
            <th>名称</th>
            <td field="name">--</td>
        </tr>
        <tr>
            <th>全类名</th>
            <td field="className">--</td>
        </tr>
        <tr>
            <th>执行方法名</th>
            <td field="methodName">--</td>
        </tr>
        <tr>
            <th>Cron表达式</th>
            <td field="cron">--</td>
        </tr>
        <tr>
            <th>并行运行</th>
            <td field="isConcurrentStr">--</td>
        </tr>
        <tr>
            <th>状态</th>
            <td field="stateStr">--</td>
        </tr>
        <tr>
            <th>异常信息</th>
            <td field="errorMessage">--</td>
        </tr>
        <tr>
            <th>描述</th>
            <td field="remark">--</td>
        </tr>
    </table>
</div>
</body>

</html>
