package com.yunfeisoft.controller.business;

import com.applet.base.BaseController;
import com.applet.utils.Page;
import com.applet.utils.Response;
import com.applet.utils.ResponseUtils;
import com.applet.utils.Validator;
import com.yunfeisoft.business.model.Warehouse;
import com.yunfeisoft.business.service.inter.CodeBuilderService;
import com.yunfeisoft.business.service.inter.WarehouseService;
import com.yunfeisoft.enumeration.YesNoEnum;
import com.yunfeisoft.model.User;
import com.yunfeisoft.utils.ApiUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ClassName: WarehouseController
 * Description: 仓库信息Controller
 * Author: Jackie liu
 * Date: 2020-08-04
 */
@Controller
public class WarehouseController extends BaseController {

    @Autowired
    private WarehouseService warehouseService;
    @Autowired
    private CodeBuilderService codeBuilderService;

    /**
     * 添加仓库信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/warehouse/save", method = RequestMethod.POST)
    @ResponseBody
    public Response save(Warehouse record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "name", "名称为空");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }

        User user = ApiUtils.getLoginUser();
        record.setOrgId(user.getOrgId());

        boolean dupName = warehouseService.isDupName(user.getOrgId(), record.getId(), record.getName());
        if (dupName) {
            return ResponseUtils.warn("该名称已经存在");
        }

        String code = codeBuilderService.generateCode("WAREHOUSE", 2, user.getOrgId());
        record.setCode(code);
        record.setIsDefault(YesNoEnum.NO_CANCEL.getValue());

        warehouseService.save(record);
        return ResponseUtils.success("保存成功");
    }

    /**
     * 修改仓库信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/warehouse/modify", method = RequestMethod.POST)
    @ResponseBody
    public Response modify(Warehouse record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "id", "参数错误");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }

        User user = ApiUtils.getLoginUser();
        boolean dupName = warehouseService.isDupName(user.getOrgId(), record.getId(), record.getName());
        if (dupName) {
            return ResponseUtils.warn("该名称已经存在");
        }

        warehouseService.modify(record);
        return ResponseUtils.success("保存成功");
    }

    /**
     * 查询仓库信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/warehouse/query", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response query(HttpServletRequest request, HttpServletResponse response) {
        String id = ServletRequestUtils.getStringParameter(request, "id", null);
        if (StringUtils.isBlank(id)) {
            return ResponseUtils.warn("参数错误");
        }
        Warehouse record = warehouseService.load(id);
        return ResponseUtils.success(record);
    }

    /**
     * 分页查询仓库信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/warehouse/list", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response list(HttpServletRequest request, HttpServletResponse response) {
        String name = ServletRequestUtils.getStringParameter(request, "name", null);

        User user = ApiUtils.getLoginUser();

        Map<String, Object> params = new HashMap<String, Object>();
        initParams(params, request);
        params.put("name", name);
        params.put("orgId", user.getOrgId());

        Page<Warehouse> page = warehouseService.queryPage(params);
        return ResponseUtils.success(page);
    }

    /**
     * 查询仓库信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/warehouse/queryList", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response queryTree(HttpServletRequest request, HttpServletResponse response) {
        String userId = ServletRequestUtils.getStringParameter(request, "userId", null);
        User user = ApiUtils.getLoginUser();

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("orgId", user.getOrgId());
        params.put("userId", userId);

        List<Warehouse> list = warehouseService.queryList(params);
        return ResponseUtils.success(list);
    }

    /**
     * 批量删除仓库信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/warehouse/delete", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response delete(HttpServletRequest request, HttpServletResponse response) {
        String ids = ServletRequestUtils.getStringParameter(request, "ids", null);
        if (StringUtils.isBlank(ids)) {
            return ResponseUtils.warn("参数错误");
        }
        warehouseService.remove(ids.split(","));
        return ResponseUtils.success("删除成功");
    }

    /**
     * 设置默认
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/warehouse/default", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response setDefault(HttpServletRequest request, HttpServletResponse response) {
        String id = ServletRequestUtils.getStringParameter(request, "id", null);
        if (StringUtils.isBlank(id)) {
            return ResponseUtils.warn("参数错误");
        }

        User user = ApiUtils.getLoginUser();

        warehouseService.modifyDefault(id, user.getOrgId());
        return ResponseUtils.success("设置成功");
    }
}
