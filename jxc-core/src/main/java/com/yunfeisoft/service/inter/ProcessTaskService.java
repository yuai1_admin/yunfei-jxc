package com.yunfeisoft.service.inter;

import com.applet.base.BaseService;
import com.applet.utils.Page;
import com.yunfeisoft.model.ProcessTask;

import java.util.Map;

/**
 * ClassName: ProcessTaskService
 * Description: 流程任务(userTask)信息service接口
 * Author: Jackie liu
 * Date: 2019-07-08
 */
public interface ProcessTaskService extends BaseService<ProcessTask, String> {

    public Page<ProcessTask> queryPage(Map<String, Object> params);

    public boolean isDuplicateKey(String id, String processKey, String key);
}