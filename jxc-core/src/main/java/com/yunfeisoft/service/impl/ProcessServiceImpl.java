package com.yunfeisoft.service.impl;

import com.applet.base.BaseServiceImpl;
import com.applet.sql.separation.DataSourceChange;
import com.applet.utils.Page;
import com.yunfeisoft.dao.inter.ProcessDao;
import com.yunfeisoft.model.Process;
import com.yunfeisoft.service.inter.ProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * ClassName: ProcessServiceImpl
 * Description: 流程信息service实现
 * Author: Jackie liu
 * Date: 2019-07-08
 */
@Service("processService")
public class ProcessServiceImpl extends BaseServiceImpl<Process, String, ProcessDao> implements ProcessService {

    //@Autowired
    //private WorkflowHelper workFlowHelper;

    @Override
    @DataSourceChange(slave = true)
    public Page<Process> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }

    @Override
    public boolean isDuplicateKey(String id, String key) {
        return getDao().isDuplicateKey(id, key);
    }

    @Override
    public int saveDeploy(String id) {
        Process record = getDao().load(id);
        /*String imagePath = record.getBpmnPath().replace(".bpmn", ".png");

        if (StringUtils.isNotBlank(record.getDeployId())) {
            workFlowHelper.deleteDeployment(record.getDeployId());
        }

        Deployment deployment = workFlowHelper.deployment(record.getName(), record.getBpmnPath(), imagePath);
        record.setDeployId(deployment.getId());*/

        return getDao().update(record);
    }

    /**
     * 强制删除流程部署
     *
     * @param id
     * @return
     */
    @Override
    public int removeDeploy(String id) {
        Process record = getDao().load(id);
        /*if (StringUtils.isNotBlank(record.getDeployId())) {
            workFlowHelper.deleteDeploymentForce(record.getDeployId());
        }
        record.setDeployId(null);*/
        return getDao().updateForce(record);
    }
}