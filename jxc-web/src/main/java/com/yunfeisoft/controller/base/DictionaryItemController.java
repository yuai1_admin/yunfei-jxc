/**
 * DictionaryController.java
 * Created at 2017-07-06
 * Created by Jackie liu
 * Copyright (C) 2014, All rights reserved.
 */
package com.yunfeisoft.controller.base;

import com.applet.base.BaseController;
import com.applet.utils.*;
import com.yunfeisoft.model.DictionaryItem;
import com.yunfeisoft.service.inter.DictionaryItemService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>ClassName: DictionaryController</p>
 * <p>Description: 字典项管理Controller</p>
 * <p>Author: Jackie liu</p>
 * <p>Date: 2017-07-06</p>
 */

/**
 * Created by Jackie Liu on 2017/3/18.
 */
@Controller
public class DictionaryItemController extends BaseController {

    @Autowired
    private DictionaryItemService dictionaryItemService;

    /**
     * 添加字典管理
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/dictionaryItem/save", method = RequestMethod.POST)
    @ResponseBody
    public Response save(DictionaryItem record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "name", "字典项名称为空");
        validator.required(request, "value", "字典项值为空");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }
        record.setId(KeyUtils.getKey());
        dictionaryItemService.save(record);
        return ResponseUtils.success("保存成功");
    }

    /**
     * 修改字典管理
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/dictionaryItem/modify", method = RequestMethod.POST)
    @ResponseBody
    public Response modify(DictionaryItem record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "name", "字典名称为空");
        validator.required(request, "value", "字典值为空");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }
        DictionaryItem dic = dictionaryItemService.load(record.getId());
        record.setDictionaryId(dic.getDictionaryId());
        dictionaryItemService.modifyForce(record);
        return ResponseUtils.success("修改成功");
    }

    /**
     * 查询字典管理
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/dictionaryItem/query", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response query(HttpServletRequest request, HttpServletResponse response) {
        String id = ServletRequestUtils.getStringParameter(request, "id", null);
        if (StringUtils.isBlank(id)) {
            return ResponseUtils.warn("参数错误");
        }
        DictionaryItem record = dictionaryItemService.load(id);
        return ResponseUtils.success("sucesss", record);
    }

    /**
     * 分页查询字典管理
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/dictionaryItem/list", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response list(HttpServletRequest request, HttpServletResponse response) {
        String searchInput = ServletRequestUtils.getStringParameter(request, "searchInput", null);
        String dicId = ServletRequestUtils.getStringParameter(request, "dicId", null);
        Map<String, Object> params = new HashMap<String, Object>();
        initParams(params, request);
        params.put("searchInput", searchInput);
        params.put("dicId", dicId);

        initParams(params, request);
        Page<DictionaryItem> page = dictionaryItemService.queryPage(params);
        return ResponseUtils.success(page);
    }

    /**
     * 批量删除执勤任务
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/dictionaryItem/remove", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response remove(HttpServletRequest request, HttpServletResponse response) {
        String ids = ServletRequestUtils.getStringParameter(request, "ids", null);
        if (StringUtils.isEmpty(ids)) {
            return ResponseUtils.warn("参数错误");
        }
        String[] idArr = ids.split(",");
        dictionaryItemService.remove(idArr);
        return ResponseUtils.success("删除成功");
    }

}
