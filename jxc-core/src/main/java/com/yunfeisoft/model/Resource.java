package com.yunfeisoft.model;

import com.applet.base.BaseModel;
import com.yunfeisoft.enumeration.YesNoEnum;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "TS_RESOURCE")
public class Resource extends BaseModel implements Serializable {

    /**
     * <p>
     * Field serialVersionUID: 序列号
     * </p>
     */
    private static final long serialVersionUID = 1L;

    // 名称
    @Column
    private String name;

    // 编码
    @Column
    private String code;

    // URL
    @Column
    private String url;

    // 状态（1启用，2停用）
    @Column
    private Integer state;

    // 备注
    @Column
    private String remark;

    // 操作类型(0：登录；1：查询；2：新增；3：修改；4：删除)
    @Column
    private Integer operateType;

    // 功能模块名称(操作类型为0-登录时，置空；为其它类型时，可填写当时用户所操作的具体功能模块名称)
    //@Column(name = "F_FUNCTION_NAME")
    //private String functionName;

    // 日志对象类型(1:人,2:车,3:案件,4:物品,5:线索,6:组织,7:全文检索,99:其它)
    //@Column(name = "F_OPERATE_OBJECT")
    //private String operateObject;

    // 日志对象名称(比如“暂住人口表”)
    //@Column(name = "F_TABLE_NAME")
    //private String tableName;

    // 格式化操作类型
    public String getOperateTypeStr() {
        return OperateType.valueOf(operateType);
    }

    // 格式化状态
    public String getStateStr() {
        return YesNoEnum.valueOfValidateLabel(state);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getOperateType() {
        return operateType;
    }

    public void setOperateType(Integer operateType) {
        this.operateType = operateType;
    }

    public enum OperateType {
        CHECK(1, "查询"), ADD(2, "新增"), MODIFY(3, "修改"), DELETE(4, "删除");

        private int value;
        private String lable;

        private OperateType(int value, String lable) {
            this.value = value;
            this.lable = lable;
        }

        public static String valueOf(int value) {
            OperateType[] values = OperateType.values();
            for (OperateType resourceEnum : values) {
                if (value == resourceEnum.getValue()) {
                    return resourceEnum.getLable();
                }
            }
            return null;
        }

        public int getValue() {
            return value;
        }

        public String getLable() {
            return lable;
        }

    }

}