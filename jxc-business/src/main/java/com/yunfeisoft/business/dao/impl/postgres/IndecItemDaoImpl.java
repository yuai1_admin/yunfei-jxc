package com.yunfeisoft.business.dao.impl.postgres;

import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.WhereBuilder;
import com.applet.sql.mapper.DefaultRowMapper;
import com.applet.utils.DateUtils;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.IndecItemDao;
import com.yunfeisoft.business.model.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * ClassName: IndecItemDaoImpl
 * Description: 损益单商品信息Dao实现
 * Author: Jackie liu
 * Date: 2020-08-06
 */
@Repository
public class IndecItemDaoImpl extends ServiceDaoImpl<IndecItem, String> implements IndecItemDao {

    @Override
    public Page<IndecItem> queryPage(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        wb.setOrderByWithDesc("io.indecDate");
        if (params != null) {
            initPageParam(wb, params);
            wb.andEquals("ii.orgId", params.get("orgId"));
            wb.andEquals("p.id", params.get("productId"));
            wb.andFullLike("p.name", params.get("productName"));
        }

        getSelectBuilder("ii");
        String sql = "SELECT II.*, W.NAME_ AS WAREHOUSE_NAME_, " +
                "P.NAME_ AS PRODUCT_NAME_, P.STANDARD_ AS PRODUCT_STANDARD_, " +
                "P.CODE_ AS PRODUCT_CODE_, P.UNIT_ AS PRODUCT_UNIT_, IO.CODE_ AS ORDER_CODE_, IO.INDEC_DATE_ AS INDEC_DATE_, U.NAME_ AS CREATE_NAME_ " +
                "FROM TT_INDEC_ITEM II " +
                "JOIN TT_WAREHOUSE_PRODUCT WP ON (II.PRODUCT_ID_ = WP.PRODUCT_ID_ AND II.WAREHOUSE_ID_ = WP.WAREHOUSE_ID_) " +
                "JOIN TT_PRODUCT P ON II.PRODUCT_ID_ = P.ID_ " +
                "JOIN TT_WAREHOUSE W ON II.WAREHOUSE_ID_ = W.ID_ " +
                "JOIN TT_INDEC_ORDER IO ON II.INDEC_ORDER_ID_ = IO.ID_ " +
                "JOIN TS_USER U ON II.CREATE_ID_ = U.ID_ ";

        return queryPage(sql, wb);
    }

    @Override
    public List<IndecItem> queryByIndecOrderId(String indecOrderId) {
        if (StringUtils.isBlank(indecOrderId)) {
            return new ArrayList<>();
        }

        getSelectBuilder("ii");
        String sql = "SELECT II.*, WP.STOCK_ AS PRODUCT_STOCK_, W.NAME_ AS WAREHOUSE_NAME_, " +
                "P.NAME_ AS PRODUCT_NAME_, P.STANDARD_ AS PRODUCT_STANDARD_, " +
                "P.CODE_ AS PRODUCT_CODE_, P.UNIT_ AS PRODUCT_UNIT_ " +
                "FROM TT_INDEC_ITEM II " +
                "JOIN TT_WAREHOUSE_PRODUCT WP ON (II.PRODUCT_ID_ = WP.PRODUCT_ID_ AND II.WAREHOUSE_ID_ = WP.WAREHOUSE_ID_) " +
                "JOIN TT_PRODUCT P ON II.PRODUCT_ID_ = P.ID_ " +
                "JOIN TT_WAREHOUSE W ON II.WAREHOUSE_ID_ = W.ID_";
        WhereBuilder wb = new WhereBuilder();
        wb.andEquals("ii.indecOrderId", indecOrderId);
        return query(sql, wb);
    }

    @Override
    public int removeByIndecOrderId(String indecOrderId) {
        if (StringUtils.isBlank(indecOrderId)) {
            return 0;
        }

        WhereBuilder wb = new WhereBuilder();
        wb.andEquals("indecOrderId", indecOrderId);
        return deleteByCondition(wb);
    }

    @Override
    public List<OrderItemStatistics> queryStatistics(String orgId, String productId) {
        String sql = "SELECT TO_CHAR(IO.INDEC_DATE_, 'YYYY-MM-DD') AS DAY_, SUM (II.INDEC_QUANTITY_) AS TOTAL_QUANTITY_" +
                " FROM TT_INDEC_ITEM II JOIN TT_INDEC_ORDER IO ON II.INDEC_ORDER_ID_ = IO.ID_" +
                " WHERE II.IS_DEL_ = 2 AND IO.STATUS_ = ? AND II.ORG_ID_ = ? AND II.PRODUCT_ID_ = ? GROUP BY DAY_";

        return jdbcTemplate.query(sql, new Object[]{IndecOrder.IndecOrderStatusEnum.DELIVERED.getValue(), orgId, productId}, new DefaultRowMapper<OrderItemStatistics>(OrderItemStatistics.class));
    }
}