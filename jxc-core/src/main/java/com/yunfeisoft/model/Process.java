package com.yunfeisoft.model;

import com.applet.base.BaseModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * ClassName: Process
 * Description: 流程信息
 *
 * @Author: Jackie liu
 * Date: 2019-07-08
 */
@Entity
@Table(name = "TT_PROCESS")
public class Process extends BaseModel implements Serializable {

    /**
     * Field serialVersionUID: 序列号
     */
    private static final long serialVersionUID = 1L;

    /**
     * 流程id
     */
    @Column
    private String key;

    /**
     * 流程名称
     */
    @Column
    private String name;

    /**
     * bpmn文件路径
     */
    @Column
    private String bpmnPath;

    /**
     * 流程部署id
     */
    @Column
    private String deployId;


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBpmnPath() {
        return bpmnPath;
    }

    public void setBpmnPath(String bpmnPath) {
        this.bpmnPath = bpmnPath;
    }

    public String getDeployId() {
        return deployId;
    }

    public void setDeployId(String deployId) {
        this.deployId = deployId;
    }
}