<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="${params.contextPath}/common/ztree/css/zTreeStyle/zTreeStyle.css"/>
	<title>分组/群组</title>
	<#include "/common/resource.ftl">
	<script type="text/javascript" src="${params.contextPath}/common/ztree/js/jquery.ztree.all.min.js"></script>
	<style type="text/css">
		.save-group{float:none;position:absolute;top:0px;right:0px;cursor:pointer;border-radius:0px 2px 2px 0px}
	</style>
</head>
<body>
	<div class="layui-container" style="margin-top:10px;">
		<div class="layui-row layui-col-space10">
			<div class="layui-col-md6">
				<!-- ** 左侧栏 ** -->
				<div class="ui-head">
					<span>我的分组</span>
					<#--<span class="right"> <i class="fa fa-plus"></i>&nbsp;添加分组</span>-->
					<span class="right remove-friend"> <i class="fa fa-remove"></i>&nbsp;删除好友</span>
                    <span class="right remove-group"> <i class="fa fa-remove"></i>&nbsp;删除分组</span>
					<#--<span class="right"> <i class="fa fa-ban"></i>&nbsp;取消选中</span>-->
				</div>
				<div class="ui-body">
                    <form class="layui-form layui-form-pane ajax-form2" action="${params.contextPath}/web/imGroup/save.json">
                        <div class="layui-form-item" style="position:relative;">
                            <div class="layui-input-block" style="margin-right:108px;margin-left:0px;">
                                <input type="text" name="name" placeholder="请输入分组名称" class="layui-input">
                            </div>
							<input type="submit" class="layui-form-label save-group" value="保存"/>
                            <#--<label class="layui-form-label save-group">保存</label>-->
                        </div>
                    </form>
					<ul id="groupTree" class="ztree"></ul>
				</div>
			</div>
			<div class="layui-col-md6 ">
				<div class="ui-head">
					<span>未添加的好友</span>
				</div>
				<div class="ui-body">
                    <p style="color:#FF5722">点击下面节点将添加好友到对应的分组中</p>
					<ul id="strangerTree" class="ztree"></ul>
				</div>
			</div>
		</div>
	</div>
    <script type="text/javascript">
        function beforeDrop(treeId, treeNodes, targetNode, moveType, isCopy) {
            var thisNode = treeNodes[0];
            if (!thisNode || !targetNode || !thisNode.isUser || targetNode.isUser) {
                return false;
            }
            var success = false;
            $.ajaxRequest({
                url:"${params.contextPath}/web/imUserGroup/modify.json",
                data:{id:thisNode.data.id, groupId:targetNode.id},
                async:false,
                success:function (data) {
                    if (!data.success) {
                        $.message("移动好友失败");
                        return;
                    }
                    $.message("移动好友成功");
                    success = true;
                }
            });
            return success;
        }
        function loadFriendTree() {
            var setting = {
                data: {simpleData: {enable: true,}},
                edit: {
                    drag: {autoExpandTrigger: true},
                    enable: true,
                    showRemoveBtn: false,
                    showRenameBtn: false
                },
                callback: {beforeDrop: beforeDrop}
            };

            //我的分组
            $.ajaxRequest({
                type : 'post',
                url : '${params.contextPath}/web/imGroup/tree.json',
                success : function(data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    var list = data.data;
                    for (var i = 0; i < list.length; i++) {
                        var item = list[i];
                        if (item.data) {
                            item.isUser = true;
                            item.icon = "${params.contextPath}/images/user2.png";
                        } else {
                            item.isUser = false;
                            item.icon = "${params.contextPath}/images/group.png";
                        }
                    }
                    zTree = $.fn.zTree.init($("#groupTree"), setting, list);
                    zTree.expandAll(true); //展开根节点
                }
            });

        }
        function addFriend(event, treeId, treeNode, clickFlag) {
            var groupTree = $.fn.zTree.getZTreeObj("groupTree");
            var nodes = groupTree.getSelectedNodes();
            if (nodes.length <= 0 || nodes[0].isUser) {
                $.message("请选中分组节点");
                return;
            }
            var groupId = nodes[0].id;
            var friendId = treeNode.id;
            $.ajaxRequest({
                url:"${params.contextPath}/web/imUserGroup/save.json",
                data:{groupId:groupId, friendId:friendId},
                async:false,
                success:function (data) {
                    $.message(data.message);
                    if (!data.success) {
                        return;
                    }
                    var strangerTree = $.fn.zTree.getZTreeObj("strangerTree");
                    strangerTree.removeNode(treeNode);
                    loadFriendTree();
                }
            });
        }
        function loadStrangerTree() {
            var setting = {
                data: {simpleData: {enable: true,}},
                callback: {onClick: addFriend}
            };

            //我的分组
            $.ajaxRequest({
                type: 'post',
                url: '${params.contextPath}/web/user/strangerTree.json',
                success: function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    var list = data.data;
                    for (var i = 0; i < list.length; i++) {
                        var item = list[i];
                        if (item.data.policeNo) {
                            item.isUser = true;
                            item.icon = "${params.contextPath}/images/user2.png";
                        } else {
                            item.isUser = false;
                        }
                    }
                    zTree = $.fn.zTree.init($("#strangerTree"), setting, data.data);
                    zTree.expandNode(true); //展开根节点
                }
            });
        }

        $(function() {
            loadFriendTree();
            loadStrangerTree();
            $(".ajax-form2").unbind().submit(function () {
                var form = $(this);
                form.formSubmit({
					parentRefresh: false,
					parentClose: false,
                    callBack:function (data) {
                        loadFriendTree();
                    }
                });
                return false;
            });
            //删除好友
            $(".remove-friend").click(function () {
                var groupTree = $.fn.zTree.getZTreeObj("groupTree");
                var nodes = groupTree.getSelectedNodes();
                if (nodes.length <= 0 || !nodes[0].isUser) {
                    $.message("请选中我的分组中好友节点");
                    return;
                }
                layer.confirm("确定删除该好友吗？", function(){
                    $.ajaxRequest({
                        url:"${params.contextPath}/web/imUserGroup/remove.json",
                        data:{id:nodes[0].data.id},
                        async:false,
                        success:function (data) {
                            $.message(data.message);
                            if (!data.success) {
                                return;
                            }
                            groupTree.removeNode(nodes[0]);
                            loadStrangerTree();
                        }
                    });
                });
            });
            //删除分组
            $(".remove-group").click(function () {
                var groupTree = $.fn.zTree.getZTreeObj("groupTree");
                var nodes = groupTree.getSelectedNodes();
                if (nodes.length <= 0 || nodes[0].isUser) {
                    $.message("请选中我的分组中分组节点");
                    return;
                }
                layer.confirm("确定删除该分组及该分组里的好友吗？", function(){
                    $.ajaxRequest({
                        url:"${params.contextPath}/web/imGroup/remove.json",
                        data:{id:nodes[0].id},
                        async:false,
                        success:function (data) {
                            $.message(data.message);
                            if (!data.success) {
                                return;
                            }
                            groupTree.removeNode(nodes[0]);
                            loadStrangerTree();
                        }
                    });
                });
            });//

        });
    </script>
</body>
</html>
