package com.yunfeisoft.business.dao.impl.postgres;

import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.SelectBuilder;
import com.applet.sql.builder.WhereBuilder;
import com.applet.sql.mapper.DefaultRowMapper;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.PurchaseItemDao;
import com.yunfeisoft.business.model.*;
import com.yunfeisoft.model.User;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * ClassName: PurchaseItemDaoImpl
 * Description: 采购单商品信息Dao实现
 * Author: Jackie liu
 * Date: 2020-07-23
 */
@Repository
public class PurchaseItemDaoImpl extends ServiceDaoImpl<PurchaseItem, String> implements PurchaseItemDao {

    @Override
    public Page<PurchaseItem> queryPage(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        wb.setOrderByWithDesc("po.purchaseDate");
        if (params != null) {
            initPageParam(wb, params);
            wb.andEquals("pi.orgId", params.get("orgId"));
            wb.andEquals("p.id", params.get("productId"));
            wb.andFullLike("p.name", params.get("productName"));
        }

        SelectBuilder builder = getSelectBuilder("pi");
        builder.column("p.code as productCode")
                .column("p.name as productName")
                .column("p.standard as productStandard")
                .column("p.unit as productUnit")
                .column("po.purchaseDate as purchaseDate")
                .column("po.code as orderCode")
                .column("u.name as createName")
                .column("s.name as supplierName")
                .join(Product.class).alias("p").on("pi.productId = p.id").build()
                .join(PurchaseOrder.class).alias("po").on("pi.purchaseOrderId = po.id").build()
                .join(Supplier.class).alias("s").on("po.supplierId = s.id").build()
                .join(User.class).alias("u").on("pi.createId = u.id").build();

        return queryPage(builder.getSql(), wb);
    }

    @Override
    public List<PurchaseItem> queryByPurchaseOrderId(String purchaseOrderId) {
        if (StringUtils.isBlank(purchaseOrderId)) {
            return new ArrayList<>();
        }

        SelectBuilder builder = getSelectBuilder("pi");
        builder.column("p.code as productCode")
                .column("p.name as productName")
                .column("p.standard as productStandard")
                .column("p.unit as productUnit")
                .join(Product.class).alias("p").on("pi.productId = p.id").build();

        WhereBuilder wb = new WhereBuilder();
        wb.andEquals("pi.purchaseOrderId", purchaseOrderId);
        return query(builder.getSql(), wb);
    }

    @Override
    public int removeByPurchaseOrderId(String purchaseOrderId) {
        if (StringUtils.isBlank(purchaseOrderId)) {
            return 0;
        }

        WhereBuilder wb = new WhereBuilder();
        wb.andEquals("purchaseOrderId", purchaseOrderId);
        return deleteByCondition(wb);
    }

    @Override
    public List<OrderItemStatistics> queryStatistics(String orgId, String productId) {
        String sql = "SELECT TO_CHAR(PO.PURCHASE_DATE_, 'YYYY-MM-DD') AS DAY_, SUM (PI.QUANTITY_) AS TOTAL_QUANTITY_" +
                " FROM TT_PURCHASE_ITEM PI JOIN TT_PURCHASE_ORDER PO ON PI.PURCHASE_ORDER_ID_ = PO.ID_" +
                " WHERE PI.IS_DEL_ = 2 AND PO.STATUS_ = ? AND PI.ORG_ID_ = ? AND PI.PRODUCT_ID_ = ? GROUP BY DAY_";

        return jdbcTemplate.query(sql, new Object[]{PurchaseOrder.PurchaseOrderStatusEnum.STORAGED.getValue(), orgId, productId}, new DefaultRowMapper<OrderItemStatistics>(OrderItemStatistics.class));
    }

    @Override
    public PurchaseItem queryAmountAndQuantity(String productId) {
        String sql = "SELECT SUM(QUANTITY_) AS QUANTITY_, SUM(AMOUNT_) AS AMOUNT_ FROM TT_PURCHASE_ITEM WHERE PRODUCT_ID_ = ? AND IS_DEL_ = 2";
        List<PurchaseItem> list = jdbcTemplate.query(sql, new Object[]{productId}, new DefaultRowMapper<PurchaseItem>(PurchaseItem.class));
        return CollectionUtils.isEmpty(list) ? new PurchaseItem() : list.get(0);
    }
}