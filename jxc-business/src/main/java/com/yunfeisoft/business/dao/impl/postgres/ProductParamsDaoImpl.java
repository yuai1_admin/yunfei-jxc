package com.yunfeisoft.business.dao.impl.postgres;

import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.WhereBuilder;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.ProductParamsDao;
import com.yunfeisoft.business.model.ProductParams;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * ClassName: ProductParamsDaoImpl
 * Description: 商品参数信息Dao实现
 * Author: Jackie liu
 * Date: 2020-07-23
 */
@Repository
public class ProductParamsDaoImpl extends ServiceDaoImpl<ProductParams, String> implements ProductParamsDao {

    @Override
    public Page<ProductParams> queryPage(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        if (params != null) {
            initPageParam(wb, params);
            wb.andEquals("type", params.get("type"));
            wb.andEquals("orgId", params.get("orgId"));
            wb.andFullLike("name", params.get("name"));
        }
        return queryPage(wb);
    }
}