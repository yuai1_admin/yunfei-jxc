package com.yunfeisoft.business.model;

import com.applet.base.ServiceModel;
import com.applet.sql.record.TransientField;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * ClassName: AllotOrder
 * Description: 调拨单信息
 *
 * @Author: Jackie liu
 * Date: 2020-08-06
 */
@Entity
@Table(name = "TT_ALLOT_ORDER")
public class AllotOrder extends ServiceModel implements Serializable {

    /**
     * Field serialVersionUID: 序列号
     */
    private static final long serialVersionUID = 1L;

    /**
     * 组织id
     */
    @Column
    private String orgId;

    /**
     * 调出仓库id
     */
    @Column
    private String outWarehouseId;

    /**
     * 调入仓库id
     */
    @Column
    private String inWarehouseId;

    /**
     * 订单编号
     */
    @Column
    private String code;

    /**
     * 调拨日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @Column
    private Date allotDate;

    /**
     * 备注
     */
    @Column
    private String remark;

    /**
     * 状态(1待调拨，2已调拨，3待更改)
     */
    @Column
    private Integer status;

    /**
     * 总金额
     */
    @Column
    private BigDecimal totalAmount;

    @TransientField
    private String outWarehouseName;
    @TransientField
    private String inWarehouseName;

    private List<AllotItem> allotItemList;

    public String getStatusStr() {
        return AllotOrderStatusEnum.valueOf(status);
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getOutWarehouseId() {
        return outWarehouseId;
    }

    public void setOutWarehouseId(String outWarehouseId) {
        this.outWarehouseId = outWarehouseId;
    }

    public String getInWarehouseId() {
        return inWarehouseId;
    }

    public void setInWarehouseId(String inWarehouseId) {
        this.inWarehouseId = inWarehouseId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getAllotDate() {
        return allotDate;
    }

    public void setAllotDate(Date allotDate) {
        this.allotDate = allotDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public List<AllotItem> getAllotItemList() {
        return allotItemList;
    }

    public void setAllotItemList(List<AllotItem> allotItemList) {
        this.allotItemList = allotItemList;
    }

    public String getOutWarehouseName() {
        return outWarehouseName;
    }

    public void setOutWarehouseName(String outWarehouseName) {
        this.outWarehouseName = outWarehouseName;
    }

    public String getInWarehouseName() {
        return inWarehouseName;
    }

    public void setInWarehouseName(String inWarehouseName) {
        this.inWarehouseName = inWarehouseName;
    }

    /**
     * 状态(1待调拨，2已调拨，3待更改)
     */
    public enum AllotOrderStatusEnum {

        TO_BE_ALLOTED(1, "待调拨"),
        ALLOTED(2, "已调拨"),
        TO_BE_CHANGED(3, "待变更");

        private int value;
        private String label;

        private AllotOrderStatusEnum(int value, String label) {
            this.value = value;
            this.label = label;
        }

        public static String valueOf(Integer value) {
            if (value == null) {
                return null;
            }
            for (AllotOrderStatusEnum loop : AllotOrderStatusEnum.values()) {
                if (value == loop.getValue()) {
                    return loop.getLabel();
                }
            }
            return null;
        }

        public int getValue() {
            return value;
        }

        public String getLabel() {
            return label;
        }
    }
}