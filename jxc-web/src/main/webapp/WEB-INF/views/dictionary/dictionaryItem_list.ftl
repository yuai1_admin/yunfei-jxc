<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>字典项列表</title>
    <#include "/common/resource.ftl">
    <script type="text/javascript">
        var getSearchParams = function(){
            return {
                searchInput:$("input[name='searchInput']").val() || "",
                dicId:$("input[name='dicId']").val() || "",
            };
        }
    </script>
</head>
<body>

   <div class="ui-operation">
       <div class="ui-buttons">
   		 <div class="layui-btn-group tools">
		    <button class="layui-btn layui-btn-normal open-dialog" p="url:'${params.contextPath}/view/dictionary/dictionaryItem_edit.htm?dicId=${params.dicId}',title:'添加字典项',width:'600px',height:'300px'"><i class="fa fa-plus"></i>添加</button>
         	<button class="layui-btn layui-btn-normal singleSelected" p="url:'${params.contextPath}/view/dictionary/dictionaryItem_edit.htm',title:'编辑字典项',width:'600px',height:'300px'"><i class="fa fa-pencil"></i>修改</button>
            <button class="layui-btn layui-btn-normal remove-button" reurl="${params.contextPath}/web/dictionaryItem/remove.json"><i class="fa fa-remove"></i>删除</button>
         </div>   
         <div class="ui-searchs">
             <div class="value" style="">
             	<input type="hidden" name="dicId" value="${params.dicId}"/>
                <input type="text" name="searchInput" placeholder="搜索字典项key">
             </div>
             <input type="button" value="搜索" class="layui-btn layui-btn-danger search-button"/>
         </div>
       </div>
   </div>
   <div class="ui-content" style="padding-left: 10px;padding-right:10px;">
       <table id="datagrid" options="url:'${params.contextPath}/web/dictionaryItem/list.json?dicId=${params.dicId}',params:'getSearchParams'">
           <thead>
               <tr>
                   <th data-options="field:'id',checkbox:true"></th>
                   <th data-options="field:'name',width:100">字典项名称</th>
                   <th data-options="field:'value',width:70">字典项值</th>
               </tr>
           </thead>
       </table>
   </div>
</body>
<script type="text/javascript">
$(function () {
    $(".remove-button").click(function () {
        var ids = DataGrid.getCheckedIds();
        if (ids == "") {
            $.message("请选择删除记录")
            return false;
        }
        var url = $(this).attr("reurl");
        layer.confirm('确定删除记录', function () {
            $.ajaxRequest({
                url:url,
                data:{ids:ids.join(",")},
                success:function(data){
                    $.message(data.message);
                     if (data.success) {
                        DataGrid.reload();
                    }
                }
            });
        });
    });
});
</script>
</html>
