<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>编辑调度任务信息</title> <#include "/common/resource.ftl">
<script type="text/javascript">
        $(function () {
            <#if (params.id)??>
                $.ajaxRequest({
                    url: '${params.contextPath}/web/timerJob/query.json',
                    data: {id: "${params.id}"},
                    success: function (data) {
                        if (!data.success) {
                            $.message(data.message);
                            return;
                        }
                        var record = data.data;
                        for (var key in record) {
                            $("[name='" + key + "']").val(record[key]);
                        }
                    }
                });
            </#if>
        });
    </script>
</head>
<body>
	<div class="ui-form">
		<form class="layui-form ajax-form" action="${params.contextPath}/web/timerJob/<#if (params.id)??>modify<#else>save</#if>.json" method="post">
			<input type="hidden" name="id" value="${params.id}" />
			<div class="layui-form-item">
				<label class="layui-form-label">名称<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" name="name" placeholder="请输入名称" class="layui-input">
				</div>
			</div>
            <div class="layui-form-item">
                <label class="layui-form-label">全类名<span class="ui-request">*</span></label>
                <div class="layui-input-block">
                    <input type="text" name="className" placeholder="请输入全类名" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">执行方法名<span class="ui-request">*</span></label>
                <div class="layui-input-block">
                    <input type="text" name="methodName" placeholder="请输入执行方法名" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">Cron表达式<span class="ui-request">*</span></label>
                <div class="layui-input-block">
                    <input type="text" name="cron" placeholder="请输入Cron表达式" class="layui-input">
                </div>
            </div>

 			<div class="layui-form-item">
				<label class="layui-form-label">并行运行<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<select class="layui-selectnew" name="isConcurrent">
                        <option value="2">否</option>
						<option value="1">是</option>
					</select>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">备注</label>
				<div class="layui-input-block">
					<textarea name="remark" placeholder="请输入备注内容" class="layui-textarea"></textarea>
				</div>
			</div>

			<div class="layui-form-item">
				<div class="layui-input-block">
					<input type="submit" value="保存" class="layui-btn" />
				</div>
			</div>
		</form>
	</div>
</body>

</html>
