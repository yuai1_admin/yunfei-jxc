package com.yunfeisoft.business.model;

import com.alibaba.excel.annotation.ExcelProperty;
import com.applet.base.ServiceModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * ClassName: Supplier
 * Description: 供应商信息
 *
 * @Author: Jackie liu
 * Date: 2020-07-23
 */
@Entity
@Table(name = "TT_SUPPLIER")
public class Supplier extends ServiceModel implements Serializable {

    /**
     * Field serialVersionUID: 序列号
     */
    private static final long serialVersionUID = 1L;

    /**
     * 组织id
     */
    @Column
    private String orgId;

    /**
     * 编码
     */
    @ExcelProperty("编码")
    @Column
    private String code;

    /**
     * 名称
     */
    @ExcelProperty("名称")
    @Column
    private String name;

    /**
     * 联系人
     */
    @ExcelProperty("联系人")
    @Column
    private String linkman;

    /**
     * 传真
     */
    @ExcelProperty("传真")
    @Column
    private String fax;

    /**
     * 常用电话
     */
    @ExcelProperty("常用电话")
    @Column
    private String phone;

    /**
     * 备用电话
     */
    @ExcelProperty("备用电话")
    @Column
    private String standbyPhone;

    /**
     * 地址
     */
    @ExcelProperty("地址")
    @Column
    private String address;

    /**
     * QQ号
     */
    @ExcelProperty("QQ号")
    @Column
    private String qq;

    /**
     * 微信
     */
    @ExcelProperty("微信")
    @Column
    private String wechat;

    /**
     * 旺旺
     */
    @ExcelProperty("旺旺")
    @Column
    private String wangwang;

    /**
     * Email
     */
    @ExcelProperty("Email")
    @Column
    private String email;

    /**
     * 网址
     */
    @ExcelProperty("网址")
    @Column
    private String website;

    /**
     * 银行账户
     */
    @ExcelProperty("银行账户")
    @Column
    private String bankAccount;

    /**
     * 银行户名
     */
    @ExcelProperty("银行户名")
    @Column
    private String bankName;

    /**
     * 账号/卡号
     */
    @ExcelProperty("账号/卡号")
    @Column
    private String bankNo;

    /**
     * 预付余额
     */
    @ExcelProperty("预付余额")
    @Column
    private BigDecimal balance;

    /**
     * 拼音简码
     */
    @ExcelProperty("拼音简码")
    @Column
    private String pinyinCode;

    /**
     * 备注
     */
    @ExcelProperty("备注")
    @Column
    private String remark;


    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLinkman() {
        return linkman;
    }

    public void setLinkman(String linkman) {
        this.linkman = linkman;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStandbyPhone() {
        return standbyPhone;
    }

    public void setStandbyPhone(String standbyPhone) {
        this.standbyPhone = standbyPhone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }

    public String getWangwang() {
        return wangwang;
    }

    public void setWangwang(String wangwang) {
        this.wangwang = wangwang;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankNo() {
        return bankNo;
    }

    public void setBankNo(String bankNo) {
        this.bankNo = bankNo;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getPinyinCode() {
        return pinyinCode;
    }

    public void setPinyinCode(String pinyinCode) {
        this.pinyinCode = pinyinCode;
    }


}