/**
 * ResourceDao.java
 * Created at 2017-07-06
 * Created by Jackie liu
 * Copyright (C) 2014, All rights reserved.
 */
package com.yunfeisoft.dao.inter;

import com.applet.base.BaseDao;
import com.applet.utils.Page;
import com.yunfeisoft.model.Resource;

import java.util.List;
import java.util.Map;

/**
 * <p>ClassName: ResourceDao</p>
 * <p>Description: 资源管理Dao</p>
 * <p>Author: Jackie liu</p>
 * <p>Date: 2017-07-06</p>
 */
public interface ResourceDao extends BaseDao<Resource, String> {

    public List<Resource> query(Map<String, Object> params);

    public Page<Resource> queryPage(Map<String, Object> params);

    public int batchRemove(String[] ids);

    public int modifyState(String[] ids, int state);

    public Boolean isDuplicateName(String id, String name);

    public Boolean isDuplicateCode(String id, String code);


}