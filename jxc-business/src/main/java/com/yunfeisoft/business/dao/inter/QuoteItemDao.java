package com.yunfeisoft.business.dao.inter;

import com.applet.base.BaseDao;
import com.applet.utils.Page;
import com.yunfeisoft.business.model.QuoteItem;

import java.util.Map;

/**
 * ClassName: QuoteItemDao
 * Description: 报价单商品信息Dao
 * Author: Jackie liu
 * Date: 2020-09-01
 */
public interface QuoteItemDao extends BaseDao<QuoteItem, String> {

    public Page<QuoteItem> queryPage(Map<String, Object> params);
}