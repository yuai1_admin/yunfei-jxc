package com.yunfeisoft.business.service.inter;

import com.applet.base.BaseService;
import com.yunfeisoft.business.model.AllotItem;
import com.applet.utils.Page;
import com.yunfeisoft.business.model.OrderItemStatistics;

import java.util.List;
import java.util.Map;

/**
 * ClassName: AllotItemService
 * Description: 调拨单商品信息service接口
 * Author: Jackie liu
 * Date: 2020-08-06
 */
public interface AllotItemService extends BaseService<AllotItem, String> {

    public Page<AllotItem> queryPage(Map<String, Object> params);

    public List<AllotItem> queryByAllotOrderId(String orderId);

    public List<OrderItemStatistics> queryStatistics(String orgId, String productId);

}