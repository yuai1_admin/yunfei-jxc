/**
 * AreaDaoImpl.java
 * Created at 2017-07-06
 * Created by Jackie liu
 * Copyright (C) 2014, All rights reserved.
 */
package com.yunfeisoft.dao.impl.postgres;

import com.applet.base.BaseDaoImpl;
import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.WhereBuilder;
import com.applet.utils.Page;
import com.yunfeisoft.dao.inter.AreaDao;
import com.yunfeisoft.model.Area;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * <p>ClassName: AreaDaoImpl</p>
 * <p>Description: 区域管理Dao实现</p>
 * <p>Author: Jackie liu</p>
 * <p>Date: 2017-07-06</p>
 */
@Repository
public class AreaDaoImpl extends BaseDaoImpl<Area, String> implements AreaDao {

    @Override
    public Page<Area> queryPage(Map<String, Object> params) {
        WhereBuilder queryCondition = new WhereBuilder();
        queryCondition.setOrderByWithAsc("code");
        if (params != null) {
            initPageParam(queryCondition, params);
            queryCondition.andFullLike("idPath ", params.get("idPath"));
            String searchInput = (String) params.get("searchInput");
            if (StringUtils.isNotEmpty(searchInput)) {
                queryCondition.andGroup()
                        .andFullLike("name", searchInput)
                        .orFullLike("code", searchInput);
            }
        }
        return queryPage(queryCondition);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Area> query(Map<String, Object> params) {
        WhereBuilder qc = new WhereBuilder();
        qc.setOrderByWithAsc("code");
        if (qc != null) {
            qc.setOrderByWithDesc("state");
            qc.andEquals("name", params.get("name"));
            qc.andEquals("state", params.get("state"));
            qc.andIn("parentId", (List<Long>) params.get("parentIds"));
            qc.andIn("id", (List<Long>) params.get("ids"));
            qc.andFullLike("idPath", params.get("idPath"));
        }
        return query(qc);
    }

    @Override
    public int batchRemove(String[] ids) {
        WhereBuilder qc = new WhereBuilder();
        qc.andIn("id", ids);
        return deleteByCondition(qc);
    }

    @Override
    public int modifyState(final int state, final String[] ids) {
        String sql = String.format("UPDATE %s SET STATE_ = ? WHERE ID_ = ?", domainModelAnalysis.getTableName());
        int[] result = jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setInt(1, state);
                ps.setString(2, ids[i]);
            }

            @Override
            public int getBatchSize() {
                return ids.length;
            }
        });
        return result.length;
    }

    @Override
    public boolean isDuplicateCode(String id, String code) {
        return super.isDuplicateField(id, code, "code");
    }

    @Override
    public boolean isDuplicateName(String id, String name, String parentId) {
        WhereBuilder qc = new WhereBuilder();
        qc.andEquals("name", name);
        qc.andEquals("parentId", parentId);
        qc.andNotEquals("id", id);
        return count(qc) > 0;
    }

    @Override
    public List<Area> queryRootList(Map<String, Object> params) {
        WhereBuilder queryCondition = new WhereBuilder();
        queryCondition.setOrderByWithAsc("code");
        if (params != null) {
            String parentId = (String) params.get("parentId");
            Integer state = (Integer) params.get("state");
            queryCondition.andEquals("parentId", parentId);
            queryCondition.andEquals("state", state);
        }
        return query(queryCondition);
    }

    @Override
    public List<Area> querySecondList(String parentId, int state) {
        WhereBuilder queryCondition = new WhereBuilder();
        queryCondition.setOrderByWithAsc("code");
        queryCondition.andEquals("parentId", parentId);
        queryCondition.andEquals("state", state);
        return query(queryCondition);
    }
}