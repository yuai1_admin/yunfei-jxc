package com.yunfeisoft.dao.inter;

import com.applet.base.BaseDao;
import com.applet.utils.Page;
import com.yunfeisoft.model.Process;

import java.util.Map;

/**
 * ClassName: ProcessDao
 * Description: 流程信息Dao
 * Author: Jackie liu
 * Date: 2019-07-08
 */
public interface ProcessDao extends BaseDao<Process, String> {

    public Page<Process> queryPage(Map<String, Object> params);

    public boolean isDuplicateKey(String id, String key);
}