/**
 * TimerJobService.java
 * Created at 2017-09-14
 * Created by Jackie liu
 * Copyright (C) 2014 , All rights reserved.
 */
package com.yunfeisoft.service.inter;

import com.applet.base.BaseService;
import com.applet.utils.Page;
import com.yunfeisoft.model.TimerJob;

import java.util.Map;

/**
 * <p>ClassName: TimerJobService</p>
 * <p>Description: 定时器任务service接口</p>
 * <p>Author: Jackie liu</p>
 * <p>Date: 2017-09-14</p>
 */
public interface TimerJobService extends BaseService<TimerJob, String> {

    public boolean isDuplicateName(String id, String name);

    public boolean isDuplicateClassAndMethodName(String id, String className, String methodName);

    public Page<TimerJob> queryPage(Map<String, Object> params);
}