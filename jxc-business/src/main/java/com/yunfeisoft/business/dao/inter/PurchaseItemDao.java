package com.yunfeisoft.business.dao.inter;

import com.applet.base.BaseDao;
import com.yunfeisoft.business.model.OrderItemStatistics;
import com.yunfeisoft.business.model.PurchaseItem;
import com.applet.utils.Page;

import java.util.List;
import java.util.Map;

/**
 * ClassName: PurchaseItemDao
 * Description: 采购单商品信息Dao
 * Author: Jackie liu
 * Date: 2020-07-23
 */
public interface PurchaseItemDao extends BaseDao<PurchaseItem, String> {

    public Page<PurchaseItem> queryPage(Map<String, Object> params);

    public List<PurchaseItem> queryByPurchaseOrderId(String purchaseOrderId);

    public int removeByPurchaseOrderId(String purchaseOrderId);

    public List<OrderItemStatistics> queryStatistics(String orgId, String productId);

    public PurchaseItem queryAmountAndQuantity(String productId);
}