package com.yunfeisoft.business.service.inter;

import com.applet.base.BaseService;
import com.yunfeisoft.business.model.IndecItem;
import com.applet.utils.Page;
import com.yunfeisoft.business.model.OrderItemStatistics;

import java.util.List;
import java.util.Map;

/**
 * ClassName: IndecItemService
 * Description: 损益单商品信息service接口
 * Author: Jackie liu
 * Date: 2020-08-06
 */
public interface IndecItemService extends BaseService<IndecItem, String> {

    public Page<IndecItem> queryPage(Map<String, Object> params);

    public List<IndecItem> queryByIndecOrderId(String indecOrderId);

    public List<OrderItemStatistics> queryStatistics(String orgId, String productId);
}