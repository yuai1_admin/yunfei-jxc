package com.yunfeisoft.business.dao.impl.postgres;

import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.WhereBuilder;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.QuoteItemDao;
import com.yunfeisoft.business.model.QuoteItem;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * ClassName: QuoteItemDaoImpl
 * Description: 报价单商品信息Dao实现
 * Author: Jackie liu
 * Date: 2020-09-01
 */
@Repository
public class QuoteItemDaoImpl extends ServiceDaoImpl<QuoteItem, String> implements QuoteItemDao {

    @Override
    public Page<QuoteItem> queryPage(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        if (params != null) {
            initPageParam(wb, params);
        }
        return queryPage(wb);
    }
}