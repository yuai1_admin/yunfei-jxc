/**
 * DictionaryDao.java
 * Created at 2017-07-06
 * Created by Jackie liu
 * Copyright (C) 2014, All rights reserved.
 */
package com.yunfeisoft.dao.inter;

import com.applet.base.BaseDao;
import com.applet.utils.Page;
import com.yunfeisoft.model.Dictionary;

import java.util.List;
import java.util.Map;

/**
 * <p>ClassName: DictionaryDao</p>
 * <p>Description: 字典管理Dao</p>
 * <p>Author: Jackie liu</p>
 * <p>Date: 2017-07-06</p>
 */
public interface DictionaryDao extends BaseDao<Dictionary, String> {

	public Page<Dictionary> queryPage(Map<String, Object> params);

	public int modifyState(String[] idArr ,Integer state);

	public boolean isDuplicateName(String id, String name);

	public boolean isDuplicateValue(String id, String value);

	public List<Dictionary> queryList(Map<String, Object> params);
}