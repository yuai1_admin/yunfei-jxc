/**
 * AreaDao.java
 * Created at 2017-07-06
 * Created by Jackie liu
 * Copyright (C) 2014, All rights reserved.
 */
package com.yunfeisoft.dao.inter;

import com.applet.base.BaseDao;
import com.applet.utils.Page;
import com.yunfeisoft.model.Area;

import java.util.List;
import java.util.Map;

/**
 * <p>ClassName: AreaDao</p>
 * <p>Description: 区域管理Dao</p>
 * <p>Author: Jackie liu</p>
 * <p>Date: 2017-07-06</p>
 */
public interface AreaDao extends BaseDao<Area, String> {

    public Page<Area> queryPage(Map<String, Object> params);

    public int batchRemove(String[] ids);

    public int modifyState(int state, String[] ids);

    public List<Area> query(Map<String, Object> params);

    public boolean isDuplicateCode(String id, String code);

    public boolean isDuplicateName(String id, String name, String parentId);

    public List<Area> queryRootList(Map<String, Object> params);

    public List<Area> querySecondList(String parentId, int state);
}