/**
 * DictionaryServiceImpl.java
 * Created at 2017-07-06
 * Created by Jackie liu
 * Copyright (C) 2014 , All rights reserved.
 */
package com.yunfeisoft.service.impl;

import com.applet.base.BaseServiceImpl;
import com.applet.sql.separation.DataSourceChange;
import com.applet.utils.Page;
import com.yunfeisoft.dao.inter.DictionaryDao;
import com.yunfeisoft.model.Dictionary;
import com.yunfeisoft.service.inter.DictionaryService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>ClassName: DictionaryServiceImpl</p>
 * <p>Description: 字典管理service实现</p>
 * <p>Author: Jackie liu</p>
 * <p>Date: 2017-07-06</p>
 */
@Service("dictionaryService")
public class DictionaryServiceImpl extends BaseServiceImpl<Dictionary, String, DictionaryDao> implements DictionaryService {

    @DataSourceChange(slave = true)
    @Override
    public Page<Dictionary> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }

    @DataSourceChange(slave = false)
    @Override
    public int modifyState(String[] idArr, Integer state) {
        return getDao().modifyState(idArr, state);
    }

    @Override
    public boolean isDuplicateName(String id, String name) {
        return getDao().isDuplicateName(id, name);
    }

    @Override
    public boolean isDuplicateValue(String id, String value) {
        return getDao().isDuplicateValue(id, value);
    }

    @Override
    public List<Dictionary> queryList(Map<String, Object> params) {
        return getDao().queryList(params);
    }
}