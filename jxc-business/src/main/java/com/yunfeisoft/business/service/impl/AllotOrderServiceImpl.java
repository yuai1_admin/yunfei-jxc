package com.yunfeisoft.business.service.impl;

import com.applet.base.BaseServiceImpl;
import com.applet.sql.separation.DataSourceChange;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.AllotItemDao;
import com.yunfeisoft.business.dao.inter.AllotOrderDao;
import com.yunfeisoft.business.dao.inter.WarehouseProductDao;
import com.yunfeisoft.business.model.AllotItem;
import com.yunfeisoft.business.model.AllotOrder;
import com.yunfeisoft.business.model.WarehouseProduct;
import com.yunfeisoft.business.service.inter.AllotOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * ClassName: AllotOrderServiceImpl
 * Description: 调拨单信息service实现
 * Author: Jackie liu
 * Date: 2020-08-06
 */
@Service("allotOrderService")
public class AllotOrderServiceImpl extends BaseServiceImpl<AllotOrder, String, AllotOrderDao> implements AllotOrderService {

    @Autowired
    private AllotItemDao allotItemDao;
    @Autowired
    private WarehouseProductDao warehouseProductDao;

    @Override
    @DataSourceChange(slave = true)
    public Page<AllotOrder> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }

    @Override
    public int modifyWithUnLock(String id, AllotOrder allotOrder) {
        //1 回退商品库存
        allotOrder = allotOrder == null ? getDao().load(id) : allotOrder;

        List<AllotItem> allotItemList = allotItemDao.queryByAllotOrderId(id);
        for (AllotItem item : allotItemList) {
            //调出仓库商品
            WarehouseProduct outProduct = warehouseProductDao.loadSingle(item.getProductId(), allotOrder.getOutWarehouseId());
            //调入仓库商品
            WarehouseProduct inProduct = warehouseProductDao.loadSingle(item.getProductId(), allotOrder.getInWarehouseId());

            //调出仓库商品添加库存
            WarehouseProduct product = new WarehouseProduct();
            product.setId(outProduct.getId());
            product.setStock(outProduct.getStock().add(item.getQuantity()));
            warehouseProductDao.update(product);

            //调入仓库商品减少库存
            product.setId(inProduct.getId());
            product.setStock(inProduct.getStock().subtract(item.getQuantity()));
            warehouseProductDao.update(product);
        }

        AllotOrder order = new AllotOrder();
        order.setId(id);
        order.setStatus(AllotOrder.AllotOrderStatusEnum.TO_BE_CHANGED.getValue());
        return getDao().update(order);
    }

    @Override
    public int save(AllotOrder allotOrder) {
        int result = super.save(allotOrder);
        //1 添加商品库存、添加商品记录
        for (AllotItem item : allotOrder.getAllotItemList()) {
            //调出仓库商品
            WarehouseProduct outProduct = warehouseProductDao.loadSingle(item.getProductId(), allotOrder.getOutWarehouseId());
            item.setBeforeAllotQuantity(outProduct.getStock());
            //调入仓库商品
            WarehouseProduct inProduct = warehouseProductDao.loadSingle(item.getProductId(), allotOrder.getInWarehouseId());

            //调出仓库商品减少库存
            WarehouseProduct product = new WarehouseProduct();
            product.setId(outProduct.getId());
            product.setStock(outProduct.getStock().subtract(item.getQuantity()));
            warehouseProductDao.update(product);

            //调入仓库商品增加库存
            if (inProduct == null) {
                inProduct = new WarehouseProduct();
                inProduct.setWarehouseId(allotOrder.getInWarehouseId());
                inProduct.setProductId(item.getProductId());
                inProduct.setStock(item.getQuantity());
                inProduct.setInitStock(inProduct.getStock());
                inProduct.setShortageLimit(BigDecimal.ZERO);
                inProduct.setBacklogLimit(BigDecimal.ONE);
                warehouseProductDao.insert(inProduct);

                item.setAfterAllotQuantity(inProduct.getStock());
            } else {
                product.setId(inProduct.getId());
                product.setStock(inProduct.getStock().add(item.getQuantity()));
                warehouseProductDao.update(product);

                item.setAfterAllotQuantity(product.getStock());
            }
        }
        allotItemDao.batchInsert(allotOrder.getAllotItemList());
        return result;
    }

    @Override
    public int modify(AllotOrder allotOrder) {
        AllotOrder order = getDao().load(allotOrder.getId());
        if (order.getStatus() == AllotOrder.AllotOrderStatusEnum.ALLOTED.getValue()) {
            modifyWithUnLock(order.getId(), order);
        }

        //1 扣减商品库存、删除老商品、添加新商品
        allotItemDao.removeByAllotOrderId(allotOrder.getId());
        //1 添加商品库存、添加商品记录
        for (AllotItem item : allotOrder.getAllotItemList()) {
            //调出仓库商品
            WarehouseProduct outProduct = warehouseProductDao.loadSingle(item.getProductId(), allotOrder.getOutWarehouseId());
            item.setBeforeAllotQuantity(outProduct.getStock());
            //调入仓库商品
            WarehouseProduct inProduct = warehouseProductDao.loadSingle(item.getProductId(), allotOrder.getInWarehouseId());

            //调出仓库商品减少库存
            WarehouseProduct product = new WarehouseProduct();
            product.setId(outProduct.getId());
            product.setStock(outProduct.getStock().subtract(item.getQuantity()));
            warehouseProductDao.update(product);

            //调入仓库商品增加库存
            if (inProduct == null) {
                inProduct = new WarehouseProduct();
                inProduct.setWarehouseId(allotOrder.getInWarehouseId());
                inProduct.setProductId(item.getProductId());
                inProduct.setStock(item.getQuantity());
                inProduct.setInitStock(inProduct.getStock());
                inProduct.setShortageLimit(BigDecimal.ZERO);
                inProduct.setBacklogLimit(BigDecimal.ONE);
                warehouseProductDao.insert(inProduct);

                item.setAfterAllotQuantity(inProduct.getStock());
            } else {
                product.setId(inProduct.getId());
                product.setStock(inProduct.getStock().add(item.getQuantity()));
                warehouseProductDao.update(product);

                item.setAfterAllotQuantity(product.getStock());
            }
        }
        allotItemDao.batchInsert(allotOrder.getAllotItemList());
        return super.modify(allotOrder);
    }

    @Override
    public int remove(String id) {
        //1 回退商品库存
        AllotOrder allotOrder = getDao().load(id);

        if (allotOrder.getStatus() == AllotOrder.AllotOrderStatusEnum.ALLOTED.getValue()) {
            List<AllotItem> allotItemList = allotItemDao.queryByAllotOrderId(id);
            for (AllotItem item : allotItemList) {
                //调出仓库商品
                WarehouseProduct outProduct = warehouseProductDao.loadSingle(item.getProductId(), allotOrder.getOutWarehouseId());
                //调入仓库商品
                WarehouseProduct inProduct = warehouseProductDao.loadSingle(item.getProductId(), allotOrder.getInWarehouseId());

                //调出仓库商品添加库存
                WarehouseProduct product = new WarehouseProduct();
                product.setId(outProduct.getId());
                product.setStock(outProduct.getStock().add(item.getQuantity()));
                warehouseProductDao.update(product);

                //调入仓库商品减少库存
                product.setId(inProduct.getId());
                product.setStock(inProduct.getStock().subtract(item.getQuantity()));
                warehouseProductDao.update(product);
            }
        }

        //1 删除商品
        allotItemDao.removeByAllotOrderId(id);
        return super.remove(id);
    }

    @Override
    public int remove(String[] ids) {
        for (String id : ids) {
            remove(id);
        }
        return super.remove(ids);
    }
}