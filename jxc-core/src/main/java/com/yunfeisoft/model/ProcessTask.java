package com.yunfeisoft.model;

import com.applet.base.BaseModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
* ClassName: ProcessTask
* Description: 流程任务(userTask)信息
*
* @Author: Jackie liu
* Date: 2019-07-08
*/
@Entity
@Table(name="TT_PROCESS_TASK")
public class ProcessTask extends BaseModel implements Serializable {
	
	/**
     * Field serialVersionUID: 序列号
     */
    private static final long serialVersionUID = 1L;

	/**
     * 流程id
     */
	@Column
	private String processKey;

	/**
     * 任务id
     */
	@Column
	private String key;

	/**
     * 任务名称
     */
	@Column
	private String name;


	public String getProcessKey() {
		return processKey;
	}

	public void setProcessKey(String processKey) {
		this.processKey = processKey;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}