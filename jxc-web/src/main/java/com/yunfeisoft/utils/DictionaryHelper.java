package com.yunfeisoft.utils;

import com.applet.utils.SpringContextHelper;
import com.yunfeisoft.model.DictionaryItem;
import com.yunfeisoft.service.inter.DictionaryItemService;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * DictionaryHelper class<br/>
 * 数据字典帮助类，在程序中可以通过“字典项值”获取“字典项名称”和“字典项对象”<br/>
 * 注意：<br/>
 * “字典值”指的是Dictionary.value，“字典名称”指的是Dictionary.name<br/>
 * “字典项值”指的是DictionaryItem.value，“字典项名称”指的是DictionaryItem.name
 *
 * @author Jackie Liu
 * @date 2018/2/1
 */
public class DictionaryHelper {

    private Map<String, DictionaryItem> dictionaryItemMap = new HashMap<>();
    ;
    private String dictionaryValue;

    public DictionaryHelper() {
    }

    /**
     * 构造函数
     *
     * @param dictionaryValue 字典值
     */
    public DictionaryHelper(String dictionaryValue) {
        switchDictionary(dictionaryValue);
    }

    private Map<String, DictionaryItem> getDictionaryItemMap() {
        if (dictionaryItemMap.isEmpty()) {
            DictionaryItemService dictionaryItemService = SpringContextHelper.getBean(DictionaryItemService.class);
            List<DictionaryItem> list = dictionaryItemService.queryAcceptList(dictionaryValue);

            for (DictionaryItem item : list) {
                dictionaryItemMap.put(item.getValue(), item);
            }
        }
        return dictionaryItemMap;
    }

    /**
     * 获取字典项名称
     *
     * @param dictionaryItemValue 字典项值
     * @return
     */
    public String getDictionaryItemName(String dictionaryItemValue) {
        DictionaryItem dictionaryItem = getDictionaryItem(dictionaryItemValue);
        if (dictionaryItem != null) {
            return dictionaryItem.getName();
        }
        return null;
    }

    /**
     * 获取字典项名称列表
     * @param dictionaryItemValues 字典项值
     * @param split 分隔字符串
     * @return
     */
    public List<String> getDictionaryItemNames(String dictionaryItemValues, String split) {
        if (StringUtils.isBlank(dictionaryItemValues)) {
            return new ArrayList<>();
        }
        List<String> nameList = new ArrayList<>();
        String[] array = dictionaryItemValues.split(split);
        for (String dictionaryItemValue : array) {
            DictionaryItem dictionaryItem = getDictionaryItem(dictionaryItemValue);
            if (dictionaryItem == null) {
                continue;
            }
            nameList.add(dictionaryItem.getName());
        }
        return nameList;
    }

    /**
     * 获取字典项值
     *
     * @param dictionaryItemName 字典项名称
     * @return
     */
    public String getDictionaryItemValue(String dictionaryItemName) {
        Map<String, DictionaryItem> itemMap = getDictionaryItemMap();
        for (Map.Entry<String, DictionaryItem> entry : itemMap.entrySet()) {
            DictionaryItem item = entry.getValue();
            if (item.getName().equals(dictionaryItemName)) {
                return item.getValue();
            }
        }
        return null;
    }

    /**
     * 获取字典项对象
     *
     * @param dictionaryItemValue 字典项值
     * @return
     */
    public DictionaryItem getDictionaryItem(String dictionaryItemValue) {
        return getDictionaryItemMap().get(dictionaryItemValue);
    }

    /**
     * 切换字典
     *
     * @param dictionaryValue 字典值
     */
    public void switchDictionary(String dictionaryValue) {
        if (StringUtils.isBlank(dictionaryValue)) {
            throw new IllegalArgumentException("字典值为空");
        }
        this.dictionaryValue = dictionaryValue;
        dictionaryItemMap.clear();
    }
}
