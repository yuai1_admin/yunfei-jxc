package com.yunfeisoft.business.dao.inter;

import com.applet.base.BaseDao;
import com.yunfeisoft.business.model.ProductParams;
import com.applet.utils.Page;

import java.util.Map;

/**
 * ClassName: ProductParamsDao
 * Description: 商品参数信息Dao
 * Author: Jackie liu
 * Date: 2020-07-23
 */
public interface ProductParamsDao extends BaseDao<ProductParams, String> {

    public Page<ProductParams> queryPage(Map<String, Object> params);
}