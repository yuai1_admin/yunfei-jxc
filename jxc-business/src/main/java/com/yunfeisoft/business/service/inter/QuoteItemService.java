package com.yunfeisoft.business.service.inter;

import com.applet.base.BaseService;
import com.yunfeisoft.business.model.QuoteItem;
import com.applet.utils.Page;

import java.util.Map;

/**
 * ClassName: QuoteItemService
 * Description: 报价单商品信息service接口
 * Author: Jackie liu
 * Date: 2020-09-01
 */
public interface QuoteItemService extends BaseService<QuoteItem, String> {

    public Page<QuoteItem> queryPage(Map<String, Object> params);
}