<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>编辑区域信息</title>
    <link rel="stylesheet" type="text/css" href="${params.contextPath}/common/ztree/css/zTreeStyle/zTreeStyle.css"/>
    <#include "/common/resource.ftl">
    <script type="text/javascript" src="${params.contextPath}/common/ztree/js/jquery.ztree.all.min.js"></script>
    <script type="text/javascript">
        $(function () {
        <#if (params.id)??>
            $.ajaxRequest({
                url: '${params.contextPath}/web/area/query.json',
                data: {id: "${params.id}"},
                success: function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    var record = data.data;
                    for (var key in record) {
                        $("[name='" + key + "']").val(record[key]);
                    }
                }
            });
        </#if>
        });
    </script>
</head>
<body>
<div class="ui-form">
    <form class="layui-form ajax-form" action="${params.contextPath}/web/area/<#if (params.id)??>modify<#else>save</#if>.json" method="post">
        <input type="hidden" name="id" value="${params.id}"/>
        <div class="layui-form-item">
            <label class="layui-form-label">区域名称<span class="ui-request">*</span></label>
            <div class="layui-input-block">
                <input type="text" class="layui-input" placeholder="请输入区域名称" name="name">
            </div>
        </div>
        <#if !(params.id??)>
            <div class="layui-form-item">
                <label class="layui-form-label">上级区域</label>
                <div class="layui-input-block ui-more-parent">
                    <input type="hidden" name="parentId" id="parentId" value="ROOT"/>
                    <input type="text" class="layui-input select-tree" readonly="readonly" id="parentName" p="check:false,id:'#parentId',name:'#parentName',url:'${params.contextPath}/web/area/tree.json'"/>
                </div>
            </div>
        </#if>
        <div class="layui-form-item">
            <label class="layui-form-label">区域类型<span class="ui-request">*</span></label>
            <div class="layui-input-block">
                <select class="layui-selectnew" name="category">
                    <option value="">请选择区域</option>
                    <option value="1">省</option>
                    <option value="2">市</option>
                    <option value="3">区/县</option>
                    <option value="4">镇</option>
                    <option value="5">村</option>
                </select>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">区域编码<span class="ui-request">*</span></label>
            <div class="layui-input-block">
                <input type="text" class="layui-input" placeholder="请输入区域编码" name="code">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">排序序号<span class="ui-request">*</span></label>
            <div class="layui-input-block">
                <input type="text" class="layui-input" placeholder="请输入排序序号" name="orderBy">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">描述</label>
            <div class="layui-input-block">
                <textarea class="layui-textarea" name="remark" placeholder="区域说明，可输入200个字符" maxlength="200"></textarea>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <input type="submit" value="保存" class="layui-btn"/>
            </div>
        </div>
    </form>
</div>
</body>

</html>
