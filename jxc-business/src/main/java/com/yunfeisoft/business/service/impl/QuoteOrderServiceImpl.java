package com.yunfeisoft.business.service.impl;

import com.applet.base.BaseServiceImpl;
import com.yunfeisoft.business.dao.inter.QuoteOrderDao;
import com.yunfeisoft.business.model.QuoteOrder;
import com.yunfeisoft.business.service.inter.QuoteOrderService;
import com.applet.sql.separation.DataSourceChange;
import com.applet.utils.Page;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * ClassName: QuoteOrderServiceImpl
 * Description: 报价单信息service实现
 * Author: Jackie liu
 * Date: 2020-09-01
 */
@Service("quoteOrderService")
public class QuoteOrderServiceImpl extends BaseServiceImpl<QuoteOrder, String, QuoteOrderDao> implements QuoteOrderService {

    @Override
    @DataSourceChange(slave = true)
    public Page<QuoteOrder> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }
}