package com.yunfeisoft.controller.base;

import com.applet.base.BaseController;
import com.applet.utils.Page;
import com.applet.utils.Response;
import com.applet.utils.ResponseUtils;
import com.applet.utils.Validator;
import com.yunfeisoft.model.ProcessTask;
import com.yunfeisoft.service.inter.ProcessTaskService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * ClassName: ProcessTaskController
 * Description: 流程任务(userTask)信息Controller
 * Author: Jackie liu
 * Date: 2019-07-08
 */
@Controller
public class ProcessTaskController extends BaseController {

    @Autowired
    private ProcessTaskService processTaskService;

    /**
     * 添加流程任务(userTask)信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/processTask/save", method = RequestMethod.POST)
    @ResponseBody
    public Response save(ProcessTask record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "processKey", "流程KEY为空");
        validator.required(request, "key", "任务KEY为空");
        validator.required(request, "name", "任务名称为空");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }
        processTaskService.save(record);
        return ResponseUtils.success("保存成功");
    }

    /**
     * 修改流程任务(userTask)信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/processTask/modify", method = RequestMethod.POST)
    @ResponseBody
    public Response modify(ProcessTask record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "id", "参数错误");
        validator.required(request, "processKey", "流程KEY为空");
        validator.required(request, "key", "任务KEY为空");
        validator.required(request, "name", "任务名称为空");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }
        processTaskService.modify(record);
        return ResponseUtils.success("保存成功");
    }

    /**
     * 查询流程任务(userTask)信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/processTask/query", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response query(HttpServletRequest request, HttpServletResponse response) {
        String id = ServletRequestUtils.getStringParameter(request, "id", null);
        if (StringUtils.isBlank(id)) {
            return ResponseUtils.warn("参数错误");
        }
        ProcessTask record = processTaskService.load(id);
        return ResponseUtils.success(record);
    }

    /**
     * 分页查询流程任务(userTask)信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/processTask/list", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response list(HttpServletRequest request, HttpServletResponse response) {
        String processKey = ServletRequestUtils.getStringParameter(request, "processKey", null);

        Map<String, Object> params = new HashMap<String, Object>();
        initParams(params, request);
        params.put("processKey", processKey);

        Page<ProcessTask> page = processTaskService.queryPage(params);
        return ResponseUtils.success(page);
    }

    /**
     * 批量删除流程任务(userTask)信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/processTask/delete", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response delete(HttpServletRequest request, HttpServletResponse response) {
        String ids = ServletRequestUtils.getStringParameter(request, "ids", null);
        if (StringUtils.isBlank(ids)) {
            return ResponseUtils.warn("参数错误");
        }
        processTaskService.remove(ids.split(","));
        return ResponseUtils.success("删除成功");
    }
}
