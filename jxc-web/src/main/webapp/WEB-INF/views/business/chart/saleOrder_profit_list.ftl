<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>利润明细</title>
	<#include "/common/vue_resource.ftl">
</head>
<body>
<div id="app" v-cloak>
    <div class="app-container" @click="hideMenu">
        <div class="layui-row app-header">
            <div class="layui-col-md12 text-right">
                <span style="margin-top:5px;display:inline-block;">销售日期：</span>
                <input type="text" v-model="params.beginDate" id="beginDate" class="layui-input" style="width:90px;cursor:pointer;" readonly>-
                <input type="text" v-model="params.endDate" id="endDate" class="layui-input" style="width:90px;margin-right:5px;cursor:pointer;" readonly>
                <span style="margin-top:5px;display:inline-block;">客户名称：</span>
                <input type="text" v-model="params.customerName" placeholder="客户名称" class="layui-input" style="width:80px;margin-right:5px;" @keyup.13="loadData">
                <span style="margin-top:5px;display:inline-block;">业务员：</span>
                <input type="text" v-model="params.createName" placeholder="业务员" class="layui-input" style="width:80px;margin-right:5px;" @keyup.13="loadData">
                <input type="text" v-model="params.productName" placeholder="商品名称" class="layui-input" @keyup.13="loadData">
                <button type="button" class="layui-btn layui-btn-sm layui-btn-primary search-button" @click="seachData">查询</button>
            </div>
        </div>
        <div class="app-list">
            <div class="layui-row">
                <div class="layui-col-md9">
                    <div class="app-table-num"><span class="num">销售单列表(共 {{total}} 条)</span></div>
                </div>
                <div class="layui-col-md3 text-right">
                    <span class="prev" @click="loadPrev">上一页</span>
                    <span class="next" @click="loadNext">下一页</span>
                </div>
            </div>
            <table class="layui-table" lay-even lay-skin="nob" lay-size1="sm">
                <thead>
                <tr>
                    <th style="width:20px;">#</th>
                    <th>客户名称</th>
                    <th>业务员</th>
                    <#--<th>订单编号</th>-->
                    <th>销售日期</th>
                    <th>商品编号</th>
                    <th>商品名称</th>
                    <th>商品规格</th>
                    <th>销售数量</th>
                    <th>折扣</th>
                    <th>成本单价</th>
                    <th>成本金额</th>
                    <th>销售单价</th>
                    <th>销售金额</th>
                    <th>毛利</th>
                    <#--<th style="width:350px;">操作</th>-->
                </tr>
                </thead>
                <tbody>
                <tr v-for="(item, index) in rows">
                    <td>{{20 * (params.page - 1) + 1 + index}}</td>
                    <td>{{item.customerName}}</td>
                    <td>{{item.createName}}</td>
                    <#--<td>{{item.orderCode}}</td>-->
                    <td>{{item.saleDate}}</td>
                    <td>{{item.productCode}}</td>
                    <td>{{item.productName}}</td>
                    <td>{{item.productStandard}}</td>
                    <td>{{item.quantity}} {{item.productUnit}}</td>
                    <td>{{item.discount}}</td>
                    <td>￥{{item.price}}</td>
                    <td>￥{{item.amount}}</td>
                    <td>￥{{item.salePrice}}</td>
                    <td>￥{{item.saleAmount}}</td>
                    <td>￥{{item.costAmount}}</td>
                    <#--<td class="more-parent">
                        <div class="ui-operating" v-if="item.status == 2" @click="changeOrder(index)">改单</div>
                        <div class="ui-split" v-if="item.status == 2"></div>
                        <div class="ui-operating" v-if="item.payStatus != 2 && item.status == 2" @click="income(index)">收款</div>
                        <div class="ui-split" v-if="item.payStatus != 2 && item.status == 2"></div>
                        <div class="ui-operating" v-if="item.status != 2" @click="modify(index)">编辑</div>
                        <div class="ui-split" v-if="item.status != 2"></div>
                        <div class="ui-operating" @click="showDetail(index)">查看</div>
                        <div class="ui-split" v-if="item.status != 2"></div>
                        <div class="ui-operating" v-if="item.status != 2" @click="remove(index)">删除</div>
                    </td>-->
                </tr>
                <tr v-if="rows.length <= 0">
                    <td colspan="15" class="text-center">没有更多数据了...</td>
                </tr>
                </tbody>
            </table>
            <div class="layui-row">
                <div class="layui-col-md6">
                    <div class="app-table-num"><span class="num">共 {{total}} 条</span></div>
                </div>
                <div class="layui-col-md6 text-right">
                    <span class="prev" @click="loadPrev">上一页</span>
                    <span class="next" @click="loadNext">下一页</span>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            params: {
                beginDate: '',
                endDate: '',
                customerName: '',
                createName: '',
                productName: '',
                page: 1,
            },
            rows: [],
            total: 0,
        },
        mounted: function () {
            this.loadData();

            var that = this;
            laydate.render({
                elem: '#beginDate', type: 'date', done: function (value) {
                    that.params.beginDate = value;
                }
            });

            laydate.render({
                elem: '#endDate', type: 'date', done: function (value) {
                    that.params.endDate = value;
                }
            });
        },
        methods: {
            seachData: function () {
                this.params.page = 1;
                this.$nextTick(function () {
                    this.loadData();
                });
            },
            loadData: function () {
                var that = this;
                $.http.post("${params.contextPath}/web/saleItem/list.json", this.params).then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    that.rows = data.rows;
                    that.total = data.total;
                });
            },
            loadNext: function () {
                if (this.rows.length < 20 && this.rows.length > 0) {
                    return;
                }
                this.params.page = this.params.page + 1;
                this.loadData();
            },
            loadPrev: function () {
                if (this.params.page <= 1) {
                    return;
                }
                this.params.page = this.params.page - 1;
                this.loadData();
            },
            showMenu: function (index) {
                this.hideMenu();
                this.$set(this.rows[index], "showMenu", true);
            },
            hideMenu: function () {
                var that = this;
                this.rows.forEach(function (item) {
                    that.$set(item, 'showMenu', false);
                });
            },
        }
    });
</script>
</body>

</html>
