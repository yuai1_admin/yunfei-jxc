package com.yunfeisoft.business.model;

import com.applet.base.ServiceModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;
import java.util.Date;

/**
* ClassName: QuoteOrder
* Description: 报价单信息
*
* @Author: Jackie liu
* Date: 2020-09-01
*/
@Entity
@Table(name="TT_QUOTE_ORDER")
public class QuoteOrder extends ServiceModel implements Serializable {
	
	/**
     * Field serialVersionUID: 序列号
     */
    private static final long serialVersionUID = 1L;

	/**
     * 组织id
     */
	@Column
	private String orgId;

	/**
     * 名称
     */
	@Column
	private String name;

	/**
     * 备注
     */
	@Column
	private String remark;







	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}






}