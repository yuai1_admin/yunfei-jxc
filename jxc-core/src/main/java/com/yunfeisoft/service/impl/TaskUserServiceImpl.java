package com.yunfeisoft.service.impl;

import com.applet.base.BaseServiceImpl;
import com.applet.sql.separation.DataSourceChange;
import com.applet.utils.Page;
import com.yunfeisoft.dao.inter.TaskUserDao;
import com.yunfeisoft.model.TaskUser;
import com.yunfeisoft.service.inter.TaskUserService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * ClassName: TaskUserServiceImpl
 * Description: 流程任务用户信息service实现
 * Author: Jackie liu
 * Date: 2019-07-08
 */
@Service("taskUserService")
public class TaskUserServiceImpl extends BaseServiceImpl<TaskUser, String, TaskUserDao> implements TaskUserService {

    @Override
    @DataSourceChange(slave = true)
    public Page<TaskUser> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }

    @Override
    public List<TaskUser> queryList(Map<String, Object> params) {
        return getDao().queryList(params);
    }

    @Override
    public int batchSave(List<TaskUser> list) {
        if (CollectionUtils.isEmpty(list)) {
            return 0;
        }
        String taskKey = list.get(0).getTaskKey();
        getDao().deleteByTaskKey(taskKey);

        return super.batchSave(list);
    }
}