package com.yunfeisoft.business.model;

import com.applet.base.ServiceModel;
import com.applet.sql.record.TransientField;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * ClassName: AllotItem
 * Description: 调拨单商品信息
 *
 * @Author: Jackie liu
 * Date: 2020-08-06
 */
@Entity
@Table(name = "TT_ALLOT_ITEM")
public class AllotItem extends ServiceModel implements Serializable {

    /**
     * Field serialVersionUID: 序列号
     */
    private static final long serialVersionUID = 1L;

    /**
     * 组织id
     */
    @Column
    private String orgId;

    /**
     * 调拨单id
     */
    @Column
    private String allotOrderId;

    /**
     * 商品id
     */
    @Column
    private String productId;

    /**
     * 仓库id
     */
    @Column
    private String warehouseId;

    /**
     * 调拨数量
     */
    @Column
    private BigDecimal quantity;

    /**
     * 调出仓库调拨前数量
     */
    @Column
    private BigDecimal beforeAllotQuantity;

    /**
     * 调入仓库调拨后数量
     */
    @Column
    private BigDecimal afterAllotQuantity;

    /**
     * 备注
     */
    @Column
    private String remark;

    /**
     * 进货单价
     */
    @Column
    private BigDecimal price;

    /**
     * 总价
     */
    @Column
    private BigDecimal amount;

    @TransientField
    private String productCode;
    @TransientField
    private String productName;
    @TransientField
    private String productStandard;
    @TransientField
    private String productUnit;
    @TransientField
    private String outWarehouseName;
    @TransientField
    private String inWarehouseName;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @TransientField
    private Date allotDate;
    @TransientField
    private String orderCode;
    @TransientField
    private String createName;

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getAllotOrderId() {
        return allotOrderId;
    }

    public void setAllotOrderId(String allotOrderId) {
        this.allotOrderId = allotOrderId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getBeforeAllotQuantity() {
        return beforeAllotQuantity;
    }

    public void setBeforeAllotQuantity(BigDecimal beforeAllotQuantity) {
        this.beforeAllotQuantity = beforeAllotQuantity;
    }

    public BigDecimal getAfterAllotQuantity() {
        return afterAllotQuantity;
    }

    public void setAfterAllotQuantity(BigDecimal afterAllotQuantity) {
        this.afterAllotQuantity = afterAllotQuantity;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductStandard() {
        return productStandard;
    }

    public void setProductStandard(String productStandard) {
        this.productStandard = productStandard;
    }

    public String getProductUnit() {
        return productUnit;
    }

    public void setProductUnit(String productUnit) {
        this.productUnit = productUnit;
    }

    public String getOutWarehouseName() {
        return outWarehouseName;
    }

    public void setOutWarehouseName(String outWarehouseName) {
        this.outWarehouseName = outWarehouseName;
    }

    public String getInWarehouseName() {
        return inWarehouseName;
    }

    public void setInWarehouseName(String inWarehouseName) {
        this.inWarehouseName = inWarehouseName;
    }

    public Date getAllotDate() {
        return allotDate;
    }

    public void setAllotDate(Date allotDate) {
        this.allotDate = allotDate;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }
}