package com.yunfeisoft.business.model;

import com.applet.base.ServiceModel;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * ClassName: IndecOrder
 * Description: 损益单信息
 *
 * @Author: Jackie liu
 * Date: 2020-08-06
 */
@Entity
@Table(name = "TT_INDEC_ORDER")
public class IndecOrder extends ServiceModel implements Serializable {

    /**
     * Field serialVersionUID: 序列号
     */
    private static final long serialVersionUID = 1L;

    /**
     * 组织id
     */
    @Column
    private String orgId;

    /**
     * 订单编号
     */
    @Column
    private String code;

    /**
     * 损益日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @Column
    private Date indecDate;

    /**
     * 备注
     */
    @Column
    private String remark;

    /**
     * 状态(1待出库，2已出库，3待更改)
     */
    @Column
    private Integer status;

    private List<IndecItem> indecItemList;

    public String getStatusStr() {
        return IndecOrderStatusEnum.valueOf(status);
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getIndecDate() {
        return indecDate;
    }

    public void setIndecDate(Date indecDate) {
        this.indecDate = indecDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<IndecItem> getIndecItemList() {
        return indecItemList;
    }

    public void setIndecItemList(List<IndecItem> indecItemList) {
        this.indecItemList = indecItemList;
    }

    /**
     * 状态(1待出库，2已出库，3待更改)
     */
    public enum IndecOrderStatusEnum {

        TO_BE_DELIVERED(1, "待入库"),
        DELIVERED(2, "已入库"),
        TO_BE_CHANGED(3, "待变更");

        private int value;
        private String label;

        private IndecOrderStatusEnum(int value, String label) {
            this.value = value;
            this.label = label;
        }

        public static String valueOf(Integer value) {
            if (value == null) {
                return null;
            }
            for (IndecOrderStatusEnum loop : IndecOrderStatusEnum.values()) {
                if (value == loop.getValue()) {
                    return loop.getLabel();
                }
            }
            return null;
        }

        public int getValue() {
            return value;
        }

        public String getLabel() {
            return label;
        }
    }
}