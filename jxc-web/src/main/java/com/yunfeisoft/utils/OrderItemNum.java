package com.yunfeisoft.utils;

import java.math.BigDecimal;

public class OrderItemNum {

    //日期
    private String date;

    //销售数量
    public BigDecimal saleNum;

    //进货数量
    public BigDecimal purchaseNum;

    //调拨数量
    public BigDecimal allotNum;

    //损益数量
    public BigDecimal indecNum;

    //初始库存
    public BigDecimal initStock;

    //当前库存
    public BigDecimal stock;

    public BigDecimal getCaclStock() {
        return initStock.subtract(saleNum == null ? BigDecimal.ZERO : saleNum).add(purchaseNum == null ? BigDecimal.ZERO : purchaseNum);
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public BigDecimal getSaleNum() {
        return saleNum;
    }

    public void setSaleNum(BigDecimal saleNum) {
        this.saleNum = saleNum;
    }

    public BigDecimal getPurchaseNum() {
        return purchaseNum;
    }

    public void setPurchaseNum(BigDecimal purchaseNum) {
        this.purchaseNum = purchaseNum;
    }

    public BigDecimal getAllotNum() {
        return allotNum;
    }

    public void setAllotNum(BigDecimal allotNum) {
        this.allotNum = allotNum;
    }

    public BigDecimal getIndecNum() {
        return indecNum;
    }

    public void setIndecNum(BigDecimal indecNum) {
        this.indecNum = indecNum;
    }

    public BigDecimal getInitStock() {
        return initStock;
    }

    public void setInitStock(BigDecimal initStock) {
        this.initStock = initStock;
    }

    public BigDecimal getStock() {
        return stock;
    }

    public void setStock(BigDecimal stock) {
        this.stock = stock;
    }
}
