package com.yunfeisoft.business.service.inter;

import com.applet.base.BaseService;
import com.yunfeisoft.business.model.Supplier;
import com.applet.utils.Page;

import java.util.List;
import java.util.Map;

/**
 * ClassName: SupplierService
 * Description: 供应商信息service接口
 * Author: Jackie liu
 * Date: 2020-07-23
 */
public interface SupplierService extends BaseService<Supplier, String> {

    public Page<Supplier> queryPage(Map<String, Object> params);

    public List<Supplier> queryList(Map<String, Object> params);

    public List<Supplier> queryByName(String orgId, String name);

    public boolean isDupName(String orgId, String id, String name);
}