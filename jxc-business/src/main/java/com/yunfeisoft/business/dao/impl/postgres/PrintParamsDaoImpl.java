package com.yunfeisoft.business.dao.impl.postgres;

import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.WhereBuilder;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.PrintParamsDao;
import com.yunfeisoft.business.model.PrintParams;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * ClassName: PrintParamsDaoImpl
 * Description: 打印设置信息Dao实现
 * Author: Jackie liu
 * Date: 2020-08-18
 */
@Repository
public class PrintParamsDaoImpl extends ServiceDaoImpl<PrintParams, String> implements PrintParamsDao {

    @Override
    public Page<PrintParams> queryPage(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        if (params != null) {
            initPageParam(wb, params);
        }
        return queryPage(wb);
    }

    @Override
    public PrintParams loadByType(String orgId, String type) {
        WhereBuilder wb = new WhereBuilder();
        wb.andEquals("orgId", orgId);
        wb.andEquals("type", type);
        List<PrintParams> printParams = query(wb);

        return CollectionUtils.isEmpty(printParams) ? null : printParams.get(0);
    }
}