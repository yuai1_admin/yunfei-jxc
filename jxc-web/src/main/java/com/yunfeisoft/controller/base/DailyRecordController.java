package com.yunfeisoft.controller.base;

import com.applet.base.BaseController;
import com.applet.utils.Page;
import com.applet.utils.Response;
import com.applet.utils.ResponseUtils;
import com.yunfeisoft.model.DailyRecord;
import com.yunfeisoft.service.inter.DailyRecordService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * ClassName: DailyRecordController
 * Description: 操作日志Controller
 * Author: Jackie liu
 * Date: 2018-03-08
 */
@Controller
public class DailyRecordController extends BaseController {

    @Autowired
    private DailyRecordService dailyRecordService;

    /**
     * 查询操作日志
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/dailyRecord/query", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response query(HttpServletRequest request, HttpServletResponse response) {
        String id = ServletRequestUtils.getStringParameter(request, "id", null);
        if (StringUtils.isBlank(id)) {
            return ResponseUtils.warn("参数错误");
        }
        DailyRecord record = dailyRecordService.load(id);
        return ResponseUtils.success(record);
    }

    /**
     * 分页查询操作日志
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/dailyRecord/list", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response list(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> params = new HashMap<String, Object>();
        initParams(params, request);
        Page<DailyRecord> page = dailyRecordService.queryPage(params);
        return ResponseUtils.success(page);
    }

}
