package com.yunfeisoft.business.service.impl;

import com.applet.base.BaseServiceImpl;
import com.applet.sql.separation.DataSourceChange;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.CustomerDao;
import com.yunfeisoft.business.dao.inter.IncomeRecordDao;
import com.yunfeisoft.business.dao.inter.SaleOrderDao;
import com.yunfeisoft.business.model.Customer;
import com.yunfeisoft.business.model.IncomeRecord;
import com.yunfeisoft.business.model.SaleOrder;
import com.yunfeisoft.business.service.inter.IncomeRecordService;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.math.BigDecimal;
import java.util.*;

/**
 * ClassName: IncomeRecordServiceImpl
 * Description: 收款信息service实现
 * Author: Jackie liu
 * Date: 2020-07-23
 */
@Service("incomeRecordService")
public class IncomeRecordServiceImpl extends BaseServiceImpl<IncomeRecord, String, IncomeRecordDao> implements IncomeRecordService {

    @Autowired
    private SaleOrderDao saleOrderDao;
    @Autowired
    private CustomerDao customerDao;

    @Override
    @DataSourceChange(slave = true)
    public Page<IncomeRecord> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }

    @Override
    public List<IncomeRecord> queryBySaleOrderId(String saleOrderId) {
        return getDao().queryBySaleOrderId(saleOrderId);
    }

    @Override
    public String batchSave2(String orgId, String[] saleOrderIds, BigDecimal actualAmount) {
        if (ArrayUtils.isEmpty(saleOrderIds)) {
            return "订单号为空";
        }

        List<SaleOrder> orderList = saleOrderDao.queryByIds(saleOrderIds);
        Collections.sort(orderList, new Comparator<SaleOrder>() {
            @Override
            public int compare(SaleOrder o1, SaleOrder o2) {
                return o1.getSurplusAmount().compareTo(o2.getSurplusAmount());
            }
        });


        BigDecimal amount = actualAmount;
        List<SaleOrder> list = new ArrayList<>();
        for (SaleOrder order : orderList) {
            if (order.getStatus() != SaleOrder.SaleOrderStatusEnum.DELIVERED.getValue()
                    && order.getPayStatus() != SaleOrder.SaleOrderPayStatusEnum.TO_BE_CLEARED.getValue()) {
                continue;
            }

            BigDecimal surplusAmount = order.getSurplusAmount();
            BigDecimal surplus = amount.subtract(surplusAmount);
            if (surplus.compareTo(BigDecimal.ZERO) < 0) {
                IncomeRecord incomeRecord = new IncomeRecord();
                incomeRecord.setOrgId(orgId);
                incomeRecord.setSaleOrderId(order.getId());
                incomeRecord.setCustomerId(order.getCustomerId());
                incomeRecord.setReceiveAmount(surplusAmount.subtract(amount));
                incomeRecord.setIncomeAmount(amount);
                incomeRecord.setOutAmount(BigDecimal.ZERO);
                getDao().insert(incomeRecord);

                SaleOrder saleOrder = new SaleOrder();
                saleOrder.setId(order.getId());
                saleOrder.setPayStatus(SaleOrder.SaleOrderPayStatusEnum.TO_BE_CLEARED.getValue());
                saleOrder.setPayAmount(order.getPayAmount().add(amount));
                saleOrderDao.update(saleOrder);

                Customer customer = customerDao.load(order.getCustomerId());
                customer.setId(order.getCustomerId());
                customer.setBalance(customer.getBalance().add(incomeRecord.getIncomeAmount()));
                customerDao.update(customer);
                break;
            }

            amount = surplus;

            IncomeRecord incomeRecord = new IncomeRecord();
            incomeRecord.setOrgId(orgId);
            incomeRecord.setSaleOrderId(order.getId());
            incomeRecord.setCustomerId(order.getCustomerId());
            incomeRecord.setReceiveAmount(order.getSurplusAmount());
            incomeRecord.setIncomeAmount(incomeRecord.getReceiveAmount());
            incomeRecord.setOutAmount(BigDecimal.ZERO);
            getDao().insert(incomeRecord);

            SaleOrder saleOrder = new SaleOrder();
            saleOrder.setId(order.getId());
            saleOrder.setPayStatus(SaleOrder.SaleOrderPayStatusEnum.RECEIVED.getValue());
            saleOrder.setPayAmount(order.getTotalAmount());
            saleOrderDao.update(saleOrder);

            Customer customer = customerDao.load(order.getCustomerId());
            customer.setId(order.getCustomerId());
            customer.setBalance(customer.getBalance().add(incomeRecord.getIncomeAmount()));
            customerDao.update(customer);
        }

        /*for (String saleOrderId : saleOrderIds) {
            SaleOrder saleOrder = saleOrderDao.load(saleOrderId);
            if (saleOrder == null) {
                continue;
            }
            if (saleOrder.getStatus() != SaleOrder.SaleOrderStatusEnum.DELIVERED.getValue()
                    && saleOrder.getPayStatus() != SaleOrder.SaleOrderPayStatusEnum.TO_BE_CLEARED.getValue()) {
                continue;
            }

            IncomeRecord incomeRecord = new IncomeRecord();
            incomeRecord.setOrgId(orgId);
            incomeRecord.setSaleOrderId(saleOrderId);
            incomeRecord.setCustomerId(saleOrder.getCustomerId());
            incomeRecord.setReceiveAmount(saleOrder.getSurplusAmount());
            incomeRecord.setIncomeAmount(incomeRecord.getReceiveAmount());
            incomeRecord.setOutAmount(BigDecimal.ZERO);

            SaleOrder order = new SaleOrder();
            order.setId(saleOrderId);
            order.setPayStatus(SaleOrder.SaleOrderPayStatusEnum.RECEIVED.getValue());
            order.setPayAmount(saleOrder.getTotalAmount());
            saleOrderDao.update(order);

            Customer customer = customerDao.load(saleOrder.getCustomerId());
            customer.setId(saleOrder.getCustomerId());
            customer.setBalance(customer.getBalance().add(incomeRecord.getIncomeAmount()));
            customerDao.update(customer);

            getDao().insert(incomeRecord);
        }*/

        return "";
    }

    @Override
    public int save(IncomeRecord incomeRecord) {
        SaleOrder saleOrder = saleOrderDao.load(incomeRecord.getSaleOrderId());
        if (saleOrder == null) {
            return 0;
        }
        if (saleOrder.getStatus() != SaleOrder.SaleOrderStatusEnum.DELIVERED.getValue()
                && saleOrder.getPayStatus() != SaleOrder.SaleOrderPayStatusEnum.TO_BE_CLEARED.getValue()) {
            return 0;
        }

        incomeRecord.setCustomerId(saleOrder.getCustomerId());
        incomeRecord.setReceiveAmount(saleOrder.getSurplusAmount());
        incomeRecord.setOutAmount(incomeRecord.getReceiveAmount().subtract(incomeRecord.getIncomeAmount()));

        BigDecimal payAmount = saleOrder.getPayAmount().add(incomeRecord.getIncomeAmount());

        if (incomeRecord.getOutAmount().compareTo(BigDecimal.ZERO) == 0) {
            SaleOrder order = new SaleOrder();
            order.setId(incomeRecord.getSaleOrderId());
            order.setPayStatus(SaleOrder.SaleOrderPayStatusEnum.RECEIVED.getValue());
            order.setPayAmount(payAmount);
            saleOrderDao.update(order);
        } else {
            SaleOrder order = new SaleOrder();
            order.setId(incomeRecord.getSaleOrderId());
            order.setPayStatus(SaleOrder.SaleOrderPayStatusEnum.TO_BE_CLEARED.getValue());
            order.setPayAmount(payAmount);
            saleOrderDao.update(order);
        }

        Customer customer = customerDao.load(saleOrder.getCustomerId());
        customer.setId(saleOrder.getCustomerId());
        customer.setBalance(customer.getBalance().add(incomeRecord.getIncomeAmount()));
        customerDao.update(customer);

        return super.save(incomeRecord);
    }
}