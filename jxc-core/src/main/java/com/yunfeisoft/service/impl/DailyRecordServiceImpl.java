package com.yunfeisoft.service.impl;

import com.applet.base.BaseServiceImpl;
import com.applet.sql.separation.DataSourceChange;
import com.applet.utils.Page;
import com.yunfeisoft.dao.inter.DailyRecordDao;
import com.yunfeisoft.model.DailyRecord;
import com.yunfeisoft.service.inter.DailyRecordService;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * ClassName: DailyRecordServiceImpl
 * Description: 操作日志service实现
 * Author: Jackie liu
 * Date: 2018-03-08
 */
@Service("dailyRecordService")
public class DailyRecordServiceImpl extends BaseServiceImpl<DailyRecord, String, DailyRecordDao> implements DailyRecordService {

    @Override
    @DataSourceChange(slave = true)
    public Page<DailyRecord> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }
}