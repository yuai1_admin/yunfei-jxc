package com.yunfeisoft.controller.business;

import com.applet.base.BaseController;
import com.applet.utils.Page;
import com.applet.utils.Response;
import com.applet.utils.ResponseUtils;
import com.applet.utils.Validator;
import com.yunfeisoft.business.model.QuoteOrder;
import com.yunfeisoft.business.service.inter.QuoteOrderService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * ClassName: QuoteOrderController
 * Description: 报价单信息Controller
 * Author: Jackie liu
 * Date: 2020-09-01
 */
@Controller
public class QuoteOrderController extends BaseController {

    @Autowired
    private QuoteOrderService quoteOrderService;

    /**
     * 添加报价单信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/quoteOrder/save", method = RequestMethod.POST)
    @ResponseBody
    public Response save(QuoteOrder record, HttpServletRequest request, HttpServletResponse response) {
        quoteOrderService.save(record);
        return ResponseUtils.success("保存成功");
    }

    /**
     * 修改报价单信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/quoteOrder/modify", method = RequestMethod.POST)
    @ResponseBody
    public Response modify(QuoteOrder record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "id", "参数错误");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }
        quoteOrderService.modify(record);
        return ResponseUtils.success("保存成功");
    }

    /**
     * 查询报价单信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/quoteOrder/query", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response query(HttpServletRequest request, HttpServletResponse response) {
        String id = ServletRequestUtils.getStringParameter(request, "id", null);
        if (StringUtils.isBlank(id)) {
            return ResponseUtils.warn("参数错误");
        }
        QuoteOrder record = quoteOrderService.load(id);
        return ResponseUtils.success(record);
    }

    /**
     * 分页查询报价单信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/quoteOrder/list", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response list(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> params = new HashMap<String, Object>();
        initParams(params, request);
        Page<QuoteOrder> page = quoteOrderService.queryPage(params);
        return ResponseUtils.success(page);
    }

    /**
     * 批量删除报价单信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/quoteOrder/delete", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response delete(HttpServletRequest request, HttpServletResponse response) {
        String ids = ServletRequestUtils.getStringParameter(request, "ids", null);
        if (StringUtils.isBlank(ids)) {
            return ResponseUtils.warn("参数错误");
        }
        quoteOrderService.remove(ids.split(","));
        return ResponseUtils.success("删除成功");
    }
}
