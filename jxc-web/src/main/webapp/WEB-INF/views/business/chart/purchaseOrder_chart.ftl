<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>采购汇总</title>
    <#include "/common/vue_resource.ftl">
    <style>
        .layui-card-header {font-size: 18px;}
        .search {float: right;padding-left: 5px;}
        .search input, .search select{height:28px;width:100px;border:1px solid #C9C9C9;border-radius:2px;padding:0px 5px;}
        .chart{height:600px;}
    </style>
</head>
<body>
<div id="app" v-cloak>
    <div class="app-container" style="background:none;">
        <div class="layui-card">
            <div class="layui-card-header">图表统计
                <button type="button" class="layui-btn layui-btn-sm layui-btn-danger" @click="top.showNotice('purchase_order_chart')">使用技巧</button>
                <div class="search">
                    汇总条件：<select v-model="params.type">
                        <option value="supplier">供应商</option>
                        <option value="category">类别</option>
                        <option value="product">商品</option>
                        <option value="create">制单人</option>
                    </select>
                    <input type="text" id="beginDate" v-model="params.beginDate" readonly style="cursor:pointer;background:#F2F2F2;"/> 至
                    <input type="text" id="endDate" v-model="params.endDate" readonly style="cursor:pointer;background:#F2F2F2;"/>
                    <button type="button" class="layui-btn layui-btn-normal layui-btn-xs" style="margin-top:-5px;" @click="loadData">查询</button>
                </div>
            </div>
            <div class="layui-card-body chart" id="chart"></div>
        </div>
        <div class="layui-card">
            <div class="layui-card-header">数据列表</div>
            <div class="layui-card-body">
                <div class="app-list">
                    <table class="layui-table" lay-even lay-skin="nob" lay-size1="sm">
                        <thead>
                        <tr>
                            <th style="width:20px;">#</th>
                            <th>名称</th>
                            <th>采购总额</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="(item, index) in rows">
                            <td>{{1 + index}}</td>
                            <td>{{item.name}}</td>
                            <td>￥{{item.totalAmount}}</td>
                        </tr>
                        <tr v-if="rows.length <= 0">
                            <td colspan="5" class="text-center">没有更多数据了...</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <#--end-->
            </div>
        </div>
    </div>

</div>
<script crossorigin="anonymous" integrity="sha384-et+fqdf7kslHL5Ip8rXSJPUPODLa7eMfpFTBaCfnlMzrcAz/wxI5Xm/mNTZwd+7H" src="https://lib.baomitu.com/echarts/4.7.0/echarts.min.js"></script>
<script>
    //活动统计
    var Chart = {
        chartObj: null,
        init: function () {
            if (this.chartObj) {
                return this;
            }
            this.chartObj = echarts.init(document.getElementById('chart'));
            return this;
        },
        setOption: function (data) {
            /*if (!data || data.length <= 0) {
                this.chartObj = null;
                $("#chart").html("暂无数据");
                return;
            }*/
            //名称, 采购总额
            var names = [], totalAmount = [];
            for (var i = 0; i < data.length; i ++) {
                var item = data[i];
                names.push(item.name);
                totalAmount.push(item.totalAmount || 0);
            }
            this.init();
            var option = {
                color: ['#3398DB'],
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {            // 坐标轴指示器，坐标轴触发有效
                        type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                    }
                },
                grid: {left: '3%',right: '4%',bottom: '3%',containLabel: true},
                xAxis: [{type: 'category',data: names,axisTick: {alignWithLabel: true}}],
                yAxis: [{type: 'value'}],
                series: [{name: '采购金额', type: 'bar', barWidth: '60%', data: totalAmount}]
            };
            console.log(this.chartObj);
            this.chartObj.setOption(option);
            return this;
        }
    };

    var app = new Vue({
        el: '#app',
        data: {
            params: {
                type: 'supplier',
                beginDate: '${.now?string("yyyy-MM-dd")}',
                endDate: '${.now?string("yyyy-MM-dd")}',
            },
            rows: [],
            total: 0,
        },
        mounted: function () {
            this.loadData();

            var that = this;
            laydate.render({elem: '#beginDate', type:'date', done:function (value) {
                    that.params.beginDate = value;
                }});

            laydate.render({elem: '#endDate', type:'date', done:function (value) {
                    that.params.endDate = value;
                }});
        },
        methods: {
            seachData:function(){
                this.params.page = 1;
                this.$nextTick(function () {
                    this.loadData();
                });
            },
            loadData: function () {
                var that = this;
                $.http.post("${params.contextPath}/web/purchaseOrder/statistics.json", this.params).then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    that.rows = data.data;
                    Chart.setOption(data.data);
                });
            },
        }
    });
</script>
</body>

</html>
