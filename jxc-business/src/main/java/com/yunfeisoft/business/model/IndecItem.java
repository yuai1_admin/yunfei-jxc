package com.yunfeisoft.business.model;

import com.applet.base.ServiceModel;
import com.applet.sql.record.TransientField;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * ClassName: IndecItem
 * Description: 损益单商品信息
 *
 * @Author: Jackie liu
 * Date: 2020-08-06
 */
@Entity
@Table(name = "TT_INDEC_ITEM")
public class IndecItem extends ServiceModel implements Serializable {

    /**
     * Field serialVersionUID: 序列号
     */
    private static final long serialVersionUID = 1L;

    /**
     * 组织id
     */
    @Column
    private String orgId;

    /**
     * 损益单id
     */
    @Column
    private String indecOrderId;

    /**
     * 商品id
     */
    @Column
    private String productId;

    /**
     * 仓库id
     */
    @Column
    private String warehouseId;

    /**
     * 实际库存
     */
    @Column
    private BigDecimal stock;

    /**
     * 损益数量
     */
    @Column
    private BigDecimal indecQuantity;

    /**
     * 损益单价
     */
    @Column
    private BigDecimal price;

    /**
     * 损益金额
     */
    @Column
    private BigDecimal amount;

    /**
     * 备注
     */
    @Column
    private String remark;

    @TransientField
    private String productCode;
    @TransientField
    private String productName;
    @TransientField
    private String productStandard;
    @TransientField
    private String productUnit;
    @TransientField
    private BigDecimal productStock;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @TransientField
    private Date indecDate;
    @TransientField
    private String orderCode;
    @TransientField
    private String createName;

    public BigDecimal getActualStock() {
        return stock.subtract(indecQuantity);
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getIndecOrderId() {
        return indecOrderId;
    }

    public void setIndecOrderId(String indecOrderId) {
        this.indecOrderId = indecOrderId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public BigDecimal getStock() {
        return stock;
    }

    public void setStock(BigDecimal stock) {
        this.stock = stock;
    }

    public BigDecimal getIndecQuantity() {
        return indecQuantity;
    }

    public void setIndecQuantity(BigDecimal indecQuantity) {
        this.indecQuantity = indecQuantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductStandard() {
        return productStandard;
    }

    public void setProductStandard(String productStandard) {
        this.productStandard = productStandard;
    }

    public String getProductUnit() {
        return productUnit;
    }

    public void setProductUnit(String productUnit) {
        this.productUnit = productUnit;
    }

    public BigDecimal getProductStock() {
        return productStock;
    }

    public void setProductStock(BigDecimal productStock) {
        this.productStock = productStock;
    }

    public Date getIndecDate() {
        return indecDate;
    }

    public void setIndecDate(Date indecDate) {
        this.indecDate = indecDate;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }
}