package com.yunfeisoft.dao.inter;

import com.applet.base.BaseDao;
import com.applet.utils.Page;
import com.yunfeisoft.model.DailyRecord;

import java.util.Map;

/**
 * ClassName: DailyRecordDao
 * Description: 操作日志Dao
 * Author: Jackie liu
 * Date: 2018-03-08
 */
public interface DailyRecordDao extends BaseDao<DailyRecord, String> {

    public Page<DailyRecord> queryPage(Map<String, Object> params);
}