/**
 * DictionaryItemDaoImpl.java
 * Created at 2017-07-06
 * Created by Jackie liu
 * Copyright (C) 2014, All rights reserved.
 */
package com.yunfeisoft.dao.impl.postgres;

import com.applet.base.BaseDaoImpl;
import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.SelectBuilder;
import com.applet.sql.builder.WhereBuilder;
import com.applet.utils.Page;
import com.yunfeisoft.dao.inter.DictionaryItemDao;
import com.yunfeisoft.model.Dictionary;
import com.yunfeisoft.model.DictionaryItem;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * <p>ClassName: DictionaryItemDaoImpl</p>
 * <p>Description: 字典项管理Dao实现</p>
 * <p>Author: Jackie liu</p>
 * <p>Date: 2017-07-06</p>
 */
@Repository
public class DictionaryItemDaoImpl extends BaseDaoImpl<DictionaryItem, String> implements DictionaryItemDao {

    @Override
    public Page<DictionaryItem> queryPage(Map<String, Object> params) {
        WhereBuilder qc = new WhereBuilder();
        qc.setOrderByWithDesc("value");
        if (params != null) {
            initPageParam(qc, params);
            String searchInput = (String) params.get("searchInput");
            qc.andEquals("dictionaryId", params.get("dicId"));
            if (StringUtils.isNotEmpty(searchInput)) {
                qc.andFullLike("name", searchInput);
            }
        }
        return queryPage(qc);
    }

    @Override
    public int removeByDicId(final String[] dicIds) {
        String sql = String.format("DELETE FROM %s WHERE DICTIONARY_ID_ = ?", domainModelAnalysis.getTableName());
        int[] batchUpdate = jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {

            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setObject(1, dicIds[i]);
            }

            @Override
            public int getBatchSize() {
                return dicIds.length;
            }
        });
        return batchUpdate.length;
    }

    @Override
    public List<DictionaryItem> queryAcceptList(String dictionaryValue) {
        WhereBuilder whereBuilder = new WhereBuilder();
        whereBuilder.andEquals("d.state", Dictionary.StateEnum.START_USING.getValue());
        whereBuilder.andEquals("d.value", dictionaryValue);

        SelectBuilder selectBuilder = selectBuilder("di");
        selectBuilder.column("d.name as dictionaryName", "dictionary.name")
                .column("d.value as dictionaryValue", "dictionary.value")
                .from(domainModelAnalysis.getTableName()).alias("di").build()
                .join(Dictionary.class).alias("d").on("di.dictionaryId = d.id").build();

        return query(selectBuilder.getSql(), whereBuilder);
    }
}