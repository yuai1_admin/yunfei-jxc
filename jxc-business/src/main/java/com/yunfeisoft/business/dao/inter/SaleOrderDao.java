package com.yunfeisoft.business.dao.inter;

import com.applet.base.BaseDao;
import com.yunfeisoft.business.model.SaleOrder;
import com.applet.utils.Page;
import com.yunfeisoft.business.model.SaleOrderStatistics;

import java.util.List;
import java.util.Map;

/**
 * ClassName: SaleOrderDao
 * Description: 销售单信息Dao
 * Author: Jackie liu
 * Date: 2020-07-23
 */
public interface SaleOrderDao extends BaseDao<SaleOrder, String> {

    public Page<SaleOrder> queryPage(Map<String, Object> params);

    public List<SaleOrder> queryByIds(String[] ids);

    /**
     * 根据客户汇总销售数据
     * @param orgId
     * @param beginDate
     * @param endDate
     * @return
     */
    public List<SaleOrderStatistics> queryStatisticsByCustomer(String orgId, String beginDate, String endDate);

    /**
     * 根据商品类别汇总销售数据
     * @param orgId
     * @param beginDate
     * @param endDate
     * @return
     */
    public List<SaleOrderStatistics> queryStatisticsByProductCategory(String orgId, String beginDate, String endDate);

    /**
     * 根据商品汇总销售数据
     * @param orgId
     * @param beginDate
     * @param endDate
     * @return
     */
    public List<SaleOrderStatistics> queryStatisticsByProduct(String orgId, String beginDate, String endDate);

    /**
     * 根据供应商汇总销售数据
     * @param orgId
     * @param beginDate
     * @param endDate
     * @return
     */
    public List<SaleOrderStatistics> queryStatisticsBySupplier(String orgId, String beginDate, String endDate);

    /**
     * 根据制单人汇总销售数据
     * @param orgId
     * @param beginDate
     * @param endDate
     * @return
     */
    public List<SaleOrderStatistics> queryStatisticsByCreateId(String orgId, String beginDate, String endDate);

    /**
     * 根据日期统计销售单数、销售总额
     * @param orgId
     * @param beginDate
     * @param endDate
     * @return
     */
    public List<SaleOrderStatistics> queryStatisticsByDate(String orgId, String beginDate, String endDate);

    public SaleOrder queryTotalAmount(String orgId, int status, int payStatus);
}