package com.yunfeisoft.business.model;

import com.applet.sql.record.TransientField;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * 订单出库数量统计
 */
@Entity
@Table(name = "TT_ORDER_ITEM")
public class OrderItemStatistics {

    /**
     * 日期
     */
    @TransientField
    private String day;

    /**
     * 数量
     */
    @TransientField
    private BigDecimal totalQuantity;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public BigDecimal getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(BigDecimal totalQuantity) {
        this.totalQuantity = totalQuantity;
    }
}
