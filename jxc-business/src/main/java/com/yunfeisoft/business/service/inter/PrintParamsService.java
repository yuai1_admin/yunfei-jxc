package com.yunfeisoft.business.service.inter;

import com.applet.base.BaseService;
import com.yunfeisoft.business.model.PrintParams;
import com.applet.utils.Page;

import java.util.Map;

/**
 * ClassName: PrintParamsService
 * Description: 打印设置信息service接口
 * Author: Jackie liu
 * Date: 2020-08-18
 */
public interface PrintParamsService extends BaseService<PrintParams, String> {

    public Page<PrintParams> queryPage(Map<String, Object> params);

    public PrintParams loadByType(String orgId, String type);
}