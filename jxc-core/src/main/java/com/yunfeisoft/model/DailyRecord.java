package com.yunfeisoft.model;

import com.applet.base.BaseModel;
import org.joda.time.DateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * ClassName: DailyRecord
 * Description: 操作日志
 *
 * @Author: Jackie liu
 * Date: 2018-03-08
 */
@Entity
@Table(name = "TC_DAILY_RECORD")
public class DailyRecord extends BaseModel implements Serializable {

    /**
     * Field serialVersionUID: 序列号
     */
    private static final long serialVersionUID = 1L;

    /**
     * 企业名称
     */
    @Column
    private String orgName;

    /**
     * 部门名称
     */
    @Column
    private String deptName;

    /**
     * 用户名称
     */
    @Column
    private String userName;

    /**
     * 用户账号
     */
    @Column
    private String userAccount;

    /**
     * 用户角色
     */
    @Column
    private String userRoles;

    /**
     * 操作类型
     */
    @Column
    private String operateType;

    /**
     * 操作说明
     */
    @Column
    private String remark;

    /**
     * 企业id
     */
    @Column
    private String orgId;

    /**
     * 部门id
     */
    @Column
    private String deptId;

    /**
     * 用户账号id
     */
    @Column
    private String userId;

    /**
     * 用户IP
     */
    @Column
    private String ip;

    /**
     * 操作日期
     */
    @Column
    protected Date createTime;


    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }

    public String getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(String userRoles) {
        this.userRoles = userRoles;
    }

    public String getOperateType() {
        return operateType;
    }

    public void setOperateType(String operateType) {
        this.operateType = operateType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateTimeStr() {
        if (createTime != null) {
            return new DateTime(createTime).toString("yyyy-MM-dd HH:mm:ss");
        }
        return null;
    }
}