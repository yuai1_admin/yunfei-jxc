package com.yunfeisoft.controller.business;

import com.applet.base.BaseController;
import com.yunfeisoft.business.model.ProductParams;
import com.yunfeisoft.business.service.inter.ProductParamsService;
import com.applet.utils.*;
import com.yunfeisoft.model.User;
import com.yunfeisoft.utils.ApiUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * ClassName: ProductParamsController
 * Description: 商品参数信息Controller
 * Author: Jackie liu
 * Date: 2020-07-23
 */
@Controller
public class ProductParamsController extends BaseController {

    @Autowired
    private ProductParamsService productParamsService;

    /**
     * 添加商品参数信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/productParams/save", method = RequestMethod.POST)
    @ResponseBody
    public Response save(ProductParams record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "type", "类别为空");
        validator.required(request, "name", "名称为空");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }

        User user = ApiUtils.getLoginUser();
        record.setOrgId(user.getOrgId());

        productParamsService.save(record);
        return ResponseUtils.success("保存成功");
    }

    /**
     * 修改商品参数信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/productParams/modify", method = RequestMethod.POST)
    @ResponseBody
    public Response modify(ProductParams record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "id", "参数错误");
        validator.required(request, "type", "类别为空");
        validator.required(request, "name", "名称为空");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }
        productParamsService.modify(record);
        return ResponseUtils.success("保存成功");
    }

    /**
     * 查询商品参数信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/productParams/query", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response query(HttpServletRequest request, HttpServletResponse response) {
        String id = ServletRequestUtils.getStringParameter(request, "id", null);
        if (StringUtils.isBlank(id)) {
            return ResponseUtils.warn("参数错误");
        }
        ProductParams record = productParamsService.load(id);
        return ResponseUtils.success(record);
    }

    /**
     * 分页查询商品参数信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/productParams/list", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response list(HttpServletRequest request, HttpServletResponse response) {
        String name = ServletRequestUtils.getStringParameter(request, "name", null);
        int type = ServletRequestUtils.getIntParameter(request, "type", -1);

        User user = ApiUtils.getLoginUser();

        Map<String, Object> params = new HashMap<String, Object>();
        initParams(params, request);
        params.put("name", name);
        params.put("orgId", user.getOrgId());
        if (type > 0) {
            params.put("type", type);
        }

        Page<ProductParams> page = productParamsService.queryPage(params);
        return ResponseUtils.success(page);
    }

    /**
     * 批量删除商品参数信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/productParams/delete", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response delete(HttpServletRequest request, HttpServletResponse response) {
        String ids = ServletRequestUtils.getStringParameter(request, "ids", null);
        if (StringUtils.isBlank(ids)) {
            return ResponseUtils.warn("参数错误");
        }
        productParamsService.remove(ids.split(","));
        return ResponseUtils.success("删除成功");
    }
}
