<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>流程任务信息列表</title>
	<#include "/common/resource.ftl">
</head>
<body>

<div class="ui-operation">
    <#--<div class="ui-search-items layui-form">
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">多规则验证:</label>
                <div class="layui-input-inline">
                    <input type="text" name="number" class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">验证日期</label>
                <div class="layui-input-inline">
                    <input type="text" name="date" class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">验证链接</label>
                <div class="layui-input-inline">
                    <input type="tel" name="url" class="layui-input">
                </div>
            </div>
        </div>
    </div>-->
    <div class="ui-buttons">
        <div class="layui-btn-group tools">
            <button class="layui-btn layui-btn-normal open-dialog" p="url:'${params.contextPath}/view/processTask/processTask_edit.htm?processKey=${params.processKey!}',title:'添加流程任务信息',width:'600px',height:'300px'"><i class="fa fa-plus"></i>添加</button>
            <button class="layui-btn layui-btn-normal singleSelected" p="url:'${params.contextPath}/view/processTask/processTask_edit.htm',title:'编辑流程任务信息',width:'600px',height:'300px'"><i class="fa fa-pencil"></i>修改</button>
            <button class="layui-btn layui-btn-normal remove-button" reurl="${params.contextPath}/web/processTask/delete.json"><i class="fa fa-remove"></i>删除</button>
            <button class="layui-btn layui-btn-normal process-task-button" p="url:'${params.contextPath}/view/processTask/processTask_user.htm',title:'配置用户信息',width:'800px',height:'600px'"><i class="fa fa-user"></i>配置用户</button>
        </div>
        <#--<div class="ui-searchs">
            <div class="value"><input type="text" name="name" value="" placeholder="名称"/></div>
            <input type="button" value="搜索" class="layui-btn layui-btn-danger search-button"/>
        </div>-->
    </div>
</div>
<div class="ui-content" style="padding-left: 10px;padding-right:10px;">
    <table id="datagrid" options="url:'${params.contextPath}/web/processTask/list.json?processKey=${params.processKey!}',params:'getSearchParams',dblClickRow:'showDetail'">
        <thead>
			<tr>
				<th data-options="field:'id',checkbox:true"></th>
				<th data-options="field:'processKey',width:100,align:'left'">流程KEY</th>
				<th data-options="field:'key',width:100,align:'left'">任务KEY</th>
				<th data-options="field:'name',width:100,align:'left'">任务名称</th>
			</tr>
        </thead>
    </table>
</div>
<script type="text/javascript">
    function getSearchParams() {
        return {name: $("input[name='name']").val()};
    }
    var showDetail = function (index, row) {
        var url = "${params.contextPath}/view/processTask/processTask_detail.htm?id=" + row.id;
        DialogManager.openDialog("url:'" + url + "',title:'流程任务信息详情',width:'600px',height:'200px'");
    };
    //formatter:formatState
    function formatState(val, row) {
    }

    $(function () {
        $(".remove-button").click(function () {
            var ids = DataGrid.getCheckedIds();
            if (ids == "") {
                layer.msg("请选择删除记录")
                return false;
            }
            var url = $(this).attr("reurl");
            layer.confirm('确定删除记录', function () {
                $.ajaxRequest({
                    type: 'post',
                    url: url,
                    data: {ids: ids.join(",")},
                    success: function (data) {
                        $.message(data.message);
                        if (data.success) {
                            DataGrid.reload();
                        }
                    }
                });
            });
        });
        $(".process-task-button").click(function () {
            var row = DataGrid.getSelectRow();
            if (!row) {
                $.message("请选择一行!");
                return false;
            }
            var p = $(this).attr("p");
            var params = eval("({" + (p || "") + "})");
            params.url = $.getUrl(params.url, "taskKey", row.key);
            DialogManager.openDialog(JSON.stringify(params).replace("{", "").replace("}", ""));
        });
    });
</script>
</body>

</html>
