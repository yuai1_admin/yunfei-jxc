package com.yunfeisoft.business.service.impl;

import com.applet.base.BaseServiceImpl;
import com.applet.sql.separation.DataSourceChange;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.*;
import com.yunfeisoft.business.model.*;
import com.yunfeisoft.business.service.inter.SaleOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * ClassName: SaleOrderServiceImpl
 * Description: 销售单信息service实现
 * Author: Jackie liu
 * Date: 2020-07-23
 */
@Service("saleOrderService")
public class SaleOrderServiceImpl extends BaseServiceImpl<SaleOrder, String, SaleOrderDao> implements SaleOrderService {

    @Autowired
    private SaleItemDao saleItemDao;
    @Autowired
    private IncomeRecordDao incomeRecordDao;
    //@Autowired
    //private ProductDao productDao;
    @Autowired
    private CustomerDao customerDao;
    @Autowired
    private WarehouseProductDao warehouseProductDao;

    @Override
    @DataSourceChange(slave = true)
    public Page<SaleOrder> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }

    @Override
    public int modifyWithUnLock(String id, SaleOrder saleOrder) {
        //1 回退商品库存
        List<SaleItem> saleItemList = saleItemDao.queryBySaleOrderId(id);
        for (SaleItem item : saleItemList) {
            WarehouseProduct warehouseProduct = warehouseProductDao.loadSingle(item.getProductId(), item.getWarehouseId());

            WarehouseProduct product = new WarehouseProduct();
            product.setId(warehouseProduct.getId());
            product.setStock(warehouseProduct.getStock().add(item.getQuantity()));
            warehouseProductDao.update(product);
            /*Product product = productDao.load(item.getProductId());
            Product pro = new Product();
            pro.setId(product.getId());
            pro.setStock(product.getStock().add(item.getQuantity()));
            productDao.update(pro);*/
        }

        //2 减少客户销售款
        saleOrder = saleOrder == null ? load(id) : saleOrder;
        Customer customer = customerDao.load(saleOrder.getCustomerId());
        Customer cus = new Customer();
        cus.setId(customer.getId());
        cus.setBalance(customer.getBalance().subtract(saleOrder.getPayAmount()));
        customerDao.update(cus);

        //3 删除付款记录
        incomeRecordDao.removeBySaleOrderId(id);

        SaleOrder order = new SaleOrder();
        order.setId(id);
        order.setStatus(SaleOrder.SaleOrderStatusEnum.TO_BE_CHANGED.getValue());
        order.setPayStatus(SaleOrder.SaleOrderPayStatusEnum.TO_BE_CLEARED.getValue());
        order.setPayAmount(BigDecimal.ZERO);
        return getDao().update(order);
    }

    @Override
    public List<SaleOrderStatistics> queryStatisticsByCustomer(String orgId, String beginDate, String endDate) {
        return getDao().queryStatisticsByCustomer(orgId, beginDate, endDate);
    }

    @Override
    public List<SaleOrderStatistics> queryStatisticsByProductCategory(String orgId, String beginDate, String endDate) {
        return getDao().queryStatisticsByProductCategory(orgId, beginDate, endDate);
    }

    @Override
    public List<SaleOrderStatistics> queryStatisticsByProduct(String orgId, String beginDate, String endDate) {
        return getDao().queryStatisticsByProduct(orgId, beginDate, endDate);
    }

    @Override
    public List<SaleOrderStatistics> queryStatisticsBySupplier(String orgId, String beginDate, String endDate) {
        return getDao().queryStatisticsBySupplier(orgId, beginDate, endDate);
    }

    @Override
    public List<SaleOrderStatistics> queryStatisticsByCreateId(String orgId, String beginDate, String endDate) {
        return getDao().queryStatisticsByCreateId(orgId, beginDate, endDate);
    }

    @Override
    public List<SaleOrderStatistics> queryStatisticsByDate(String orgId, String beginDate, String endDate) {
        return getDao().queryStatisticsByDate(orgId, beginDate, endDate);
    }

    @Override
    public SaleOrder queryTotalAmount(String orgId, int status, int payStatus) {
        return getDao().queryTotalAmount(orgId, status, payStatus);
    }

    /*@Override
    public int modifyWithCheck(String id) {
        SaleOrder order = getDao().load(id);
        if (order == null || order.getStatus() == SaleOrder.SaleOrderStatusEnum.DELIVERED.getValue()) {
            return 0;
        }

        //1 扣减商品库存
        List<SaleItem> saleItemList = saleItemDao.queryBySaleOrderId(id);
        for (SaleItem item : saleItemList) {
            WarehouseProduct warehouseProduct = warehouseProductDao.loadSingle(item.getProductId(), item.getWarehouseId());

            WarehouseProduct product = new WarehouseProduct();
            product.setId(warehouseProduct.getId());
            product.setStock(warehouseProduct.getStock().subtract(item.getQuantity()));
            warehouseProductDao.update(product);
        }

        //2 增加客户销售款
        Customer customer = customerDao.load(order.getCustomerId());
        Customer cus = new Customer();
        cus.setId(customer.getId());
        cus.setBalance(order.getPayAmount().add(customer.getBalance()));
        customerDao.update(cus);

        SaleOrder saleOrder = new SaleOrder();
        saleOrder.setId(id);
        saleOrder.setStatus(SaleOrder.SaleOrderStatusEnum.DELIVERED.getValue());
        return super.modify(saleOrder);
    }*/

    @Override
    public int save(SaleOrder saleOrder) {
        //1 扣减商品库存、添加商品记录
        for (SaleItem item : saleOrder.getSaleItemList()) {
            WarehouseProduct warehouseProduct = warehouseProductDao.loadSingle(item.getProductId(), item.getWarehouseId());

            WarehouseProduct product = new WarehouseProduct();
            product.setId(warehouseProduct.getId());
            product.setStock(warehouseProduct.getStock().subtract(item.getQuantity()));
            warehouseProductDao.update(product);
        }
        saleItemDao.batchInsert(saleOrder.getSaleItemList());

        //2 增加客户销售款
        Customer customer = customerDao.load(saleOrder.getCustomerId());
        Customer cus = new Customer();
        cus.setId(customer.getId());
        cus.setBalance(saleOrder.getPayAmount().add(customer.getBalance()));
        customerDao.update(cus);

        //3 添加付款记录
        if (saleOrder.getIncomeRecord() != null) {
            incomeRecordDao.insert(saleOrder.getIncomeRecord());
        }
        return super.save(saleOrder);
    }

    @Override
    public int modify(SaleOrder saleOrder) {
        SaleOrder order = getDao().load(saleOrder.getId());
        if (order.getStatus() == SaleOrder.SaleOrderStatusEnum.DELIVERED.getValue()) {
            modifyWithUnLock(order.getId(), order);
        }

        //1 扣减商品库存、删除老商品、添加新商品
        saleItemDao.removeBySaleOrderId(saleOrder.getId());
        for (SaleItem item : saleOrder.getSaleItemList()) {
            WarehouseProduct warehouseProduct = warehouseProductDao.loadSingle(item.getProductId(), item.getWarehouseId());

            WarehouseProduct product = new WarehouseProduct();
            product.setId(warehouseProduct.getId());
            product.setStock(warehouseProduct.getStock().subtract(item.getQuantity()));
            warehouseProductDao.update(product);
        }
        saleItemDao.batchInsert(saleOrder.getSaleItemList());

        //2 增加客户销售款
        Customer customer = customerDao.load(saleOrder.getCustomerId());
        Customer cus = new Customer();
        cus.setId(customer.getId());
        cus.setBalance(saleOrder.getPayAmount().add(customer.getBalance()));
        customerDao.update(cus);

        //3 添加付款记录
        if (saleOrder.getIncomeRecord() != null) {
            incomeRecordDao.insert(saleOrder.getIncomeRecord());
        }
        return super.modify(saleOrder);
    }

    @Override
    public int remove(String id) {
        SaleOrder order = getDao().load(id);

        if (order.getStatus() == SaleOrder.SaleOrderStatusEnum.DELIVERED.getValue()) {
            List<SaleItem> saleItemList = saleItemDao.queryBySaleOrderId(id);
            for (SaleItem item : saleItemList) {
                WarehouseProduct warehouseProduct = warehouseProductDao.loadSingle(item.getProductId(), item.getWarehouseId());

                WarehouseProduct product = new WarehouseProduct();
                product.setId(warehouseProduct.getId());
                product.setStock(warehouseProduct.getStock().add(item.getQuantity()));
                warehouseProductDao.update(product);
            }

            //2 减少客户销售款
            Customer customer = customerDao.load(order.getCustomerId());
            Customer cus = new Customer();
            cus.setId(customer.getId());
            cus.setBalance(customer.getBalance().subtract(order.getPayAmount()));
            customerDao.update(cus);
        }

        //1 删除商品
        saleItemDao.removeBySaleOrderId(id);
        //2 删除付款记录
        incomeRecordDao.removeBySaleOrderId(id);

        return super.remove(id);
    }

    @Override
    public int remove(String[] ids) {
        for (String id : ids) {
            remove(id);
        }
        return ids.length;
    }
}