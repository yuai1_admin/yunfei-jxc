<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>编辑流程任务(userTask)信息</title>
	<#include "/common/resource.ftl">
	<script type="text/javascript">
		$(function () {
			<#if (params.id)??>
				$.ajaxRequest({
					url: '${params.contextPath}/web/processTask/query.json',
					data: {id: "${params.id}"},
					success: function (data) {
						if (!data.success) {
							$.message(data.message);
							return;
						}
						var record = data.data;
						for (var key in record) {
							$("[name='" + key + "']").val(record[key]);
						}
					}
				});
			</#if>
		});
	</script>
</head>
<body>
	<div class="ui-form">
		<form class="layui-form ajax-form" action="${params.contextPath}/web/processTask/<#if (params.id)??>modify<#else>save</#if>.json" method="post">
			<input type="hidden" name="id" value="${params.id!}" />
			<input type="hidden" name="processKey" value="${params.processKey!}" />
			<div class="layui-form-item">
                <label class="layui-form-label">任务KEY<span class="ui-request">*</span></label>
                <div class="layui-input-block">
                    <input type="text" name="key" placeholder="请输入任务KEY" class="layui-input {required:true}"/>
                </div>
            </div>
			<div class="layui-form-item">
                <label class="layui-form-label">任务名称<span class="ui-request">*</span></label>
                <div class="layui-input-block">
                    <input type="text" name="name" placeholder="请输入任务名称" class="layui-input {required:true}"/>
                </div>
            </div>

			<div class="layui-form-item">
				<div class="layui-input-block">
					<input type="submit" value="保存" class="layui-btn" />
				</div>
			</div>
		</form>
	</div>
</body>

</html>
