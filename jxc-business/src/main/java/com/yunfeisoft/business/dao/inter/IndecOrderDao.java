package com.yunfeisoft.business.dao.inter;

import com.applet.base.BaseDao;
import com.yunfeisoft.business.model.IndecOrder;
import com.applet.utils.Page;

import java.util.Map;

/**
 * ClassName: IndecOrderDao
 * Description: 损益单信息Dao
 * Author: Jackie liu
 * Date: 2020-08-06
 */
public interface IndecOrderDao extends BaseDao<IndecOrder, String> {

    public Page<IndecOrder> queryPage(Map<String, Object> params);
}