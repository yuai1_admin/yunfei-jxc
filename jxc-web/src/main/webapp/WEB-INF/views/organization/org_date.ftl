<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>编辑组织机构信息</title>
    <link rel="stylesheet" type="text/css" href="${params.contextPath}/common/ztree/css/zTreeStyle/zTreeStyle.css"/>
    <#include "/common/resource.ftl">
    <script type="text/javascript" src="${params.contextPath}/common/ztree/js/jquery.ztree.all.min.js"></script>
    <script type="text/javascript">
        $(function () {
            <#if (params.id)??>
                $.ajaxRequest({
                    url:'${params.contextPath}/web/organization/query.json',
                    data:{id:"${params.id}"},
                    success:function (data) {
                        if (!data.success) {
                            layer.msg(data.message);
                            return;
                        }
                        var record = data.data;
                        for (var key in record) {
                            $("[name='"+key+"']").val(record[key]);
                        }

                        if (record.parentOrg) {
                            $("[name='parentId']").val(record.parentOrg.id);
                            $("[name='parentName']").val(record.parentOrg.name);
                        }
                    }
                });
            </#if>
        });
    </script>
</head>
<body>
    <div class="ui-form">
        <form class="layui-form ajax-form" action="${params.contextPath}/web/organization/modifyDate.json" method="post">
            <input type="hidden" name="id" value=""/>
            <div class="layui-form-item">
                <label class="layui-form-label">开始日期<span class="ui-required">*</span></label>
                <div class="layui-input-block">
                    <input type="text" name="beginDate" placeholder="请输入名称" class="layui-input ui-datetime" readonly>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">结束日期<span class="ui-required">*</span></label>
                <div class="layui-input-block">
                    <input type="text" name="endDate" placeholder="请输入名称" class="layui-input ui-datetime" readonly>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <input type="submit" value="保存" class="layui-btn"/>
                </div>
            </div>
        </form>
    </div>
</body>

</html>
