package com.yunfeisoft.business.model;

import com.applet.sql.record.TransientField;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * ClassName: SaleOrderStatistics
 * Description: 采购单统计
 *
 * @Author: Jackie liu
 * Date: 2020-07-23
 */
@Entity
@Table(name = "TT_PURCHASE_ORDER2")
public class PurchaseOrderStatistics implements Serializable {

    /**
     * Field serialVersionUID: 序列号
     */
    private static final long serialVersionUID = 1L;

    /**
     * 类别id、商品id、供应商id、制单人id
     */
    @TransientField
    private String id;

    /**
     * 类别名称、商品名称、供应商名称、制单人名称
     */
    @TransientField
    private String name;

    /**
     * 采购总额
     */
    @TransientField
    private BigDecimal totalAmount;

    /**
     * 采购单数
     */
    @TransientField
    private Integer purchaseNum;

    /**
     * 日期（天）
     */
    @TransientField
    private String day;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getTotalAmount() {
        if (totalAmount == null) {
            return BigDecimal.ZERO;
        }
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getPurchaseNum() {
        return purchaseNum;
    }

    public void setPurchaseNum(Integer purchaseNum) {
        this.purchaseNum = purchaseNum;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }
}