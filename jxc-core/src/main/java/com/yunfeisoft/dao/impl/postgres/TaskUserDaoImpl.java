package com.yunfeisoft.dao.impl.postgres;

import com.applet.base.BaseDaoImpl;
import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.SelectBuilder;
import com.applet.sql.builder.WhereBuilder;
import com.applet.utils.Page;
import com.yunfeisoft.dao.inter.TaskUserDao;
import com.yunfeisoft.model.ProcessTask;
import com.yunfeisoft.model.TaskUser;
import com.yunfeisoft.model.User;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * ClassName: TaskUserDaoImpl
 * Description: 流程任务用户信息Dao实现
 * Author: Jackie liu
 * Date: 2019-07-08
 */
@Repository
public class TaskUserDaoImpl extends BaseDaoImpl<TaskUser, String> implements TaskUserDao {

    @Override
    public Page<TaskUser> queryPage(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        if (params != null) {
            initPageParam(wb, params);
        }
        return queryPage(wb);
    }

    @Override
    public List<TaskUser> queryList(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        if (params != null) {
            wb.andEquals("taskKey", params.get("taskKey"));
        }

        SelectBuilder builder = selectBuilder("tu");
        builder.column("u.name as userName")
                .column("pt.name as taskName")
                .from(domainModelAnalysis.getTableName()).alias("tu").build()
                .join(User.class).alias("u").on("tu.userId = u.id").build()
                .join(ProcessTask.class).alias("pt").on("tu.taskKey = pt.key");

        return query(builder.getSql(), wb);
    }

    @Override
    public int deleteByTaskKey(String taskKey) {
        if (StringUtils.isBlank(taskKey)) {
            return 0;
        }

        WhereBuilder wb = new WhereBuilder();
        wb.andEquals("taskKey", taskKey);

        return deleteByCondition(wb);
    }
}