package com.yunfeisoft.business.dao.inter;

import com.applet.base.BaseDao;
import com.applet.utils.Page;
import com.yunfeisoft.business.model.OrderItemStatistics;
import com.yunfeisoft.business.model.SaleItem;

import java.util.List;
import java.util.Map;

/**
 * ClassName: SaleItemDao
 * Description: 销售单商品信息Dao
 * Author: Jackie liu
 * Date: 2020-07-23
 */
public interface SaleItemDao extends BaseDao<SaleItem, String> {

    public Page<SaleItem> queryPage(Map<String, Object> params);

    public List<SaleItem> queryBySaleOrderId(String saleOrderId);

    public int removeBySaleOrderId(String saleOrderId);

    public List<OrderItemStatistics> queryStatistics(String orgId, String productId);
}