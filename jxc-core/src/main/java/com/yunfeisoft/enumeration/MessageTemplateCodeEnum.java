package com.yunfeisoft.enumeration;

/**
 * 消息模板code
 * Created by Jackie Liu on 2017/4/4.
 */
public enum MessageTemplateCodeEnum {

    MSGTPL_0001("MSGTPL0001", "举报信息提交成功通知"),
    MSGTPL_0002("MSGTPL0002", "举报受理进度通知"),
    MSGTPL_0003("MSGTPL0003", "义警报名申请提交通知"),
    MSGTPL_0004("MSGTPL0004", "义警报名申请审核进度通知"),
    ;

    private String code;
    private String label;

    private MessageTemplateCodeEnum(String code, String label) {
        this.code = code;
        this.label = label;
    }

    public static String valueAt(String code) {
        for (MessageTemplateCodeEnum loop : MessageTemplateCodeEnum.values()) {
            if (loop.getCode().equals(code)) {
                return loop.getLabel();
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public String getLabel() {
        return label;
    }
}
