/**
 * MessageTemplateServiceImpl.java
 * Created at 2017-09-05
 * Created by Jackie liu
 * Copyright (C) 2014 , All rights reserved.
 */
package com.yunfeisoft.service.impl;

import com.applet.base.BaseServiceImpl;
import com.yunfeisoft.dao.inter.MessageTemplateDao;
import com.yunfeisoft.model.MessageTemplate;
import com.yunfeisoft.service.inter.MessageTemplateService;
import org.springframework.stereotype.Service;

/**
 * <p>ClassName: MessageTemplateServiceImpl</p>
 * <p>Description: 消息模板service实现</p>
 * <p>Author: Jackie liu</p>
 * <p>Date: 2017-09-05</p>
 */
@Service("messageTemplateService")
public class MessageTemplateServiceImpl extends BaseServiceImpl<MessageTemplate, String, MessageTemplateDao> implements MessageTemplateService {

    @Override
    public MessageTemplate queryByCode(String code) {
        return getDao().queryByCode(code);
    }
}