<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>字典列表</title>
    <link rel="stylesheet" type="text/css" href="${params.contextPath}/common/ztree/css/zTreeStyle/zTreeStyle.css"/>
    <#include "/common/resource.ftl">
    <script type="text/javascript" src="${params.contextPath}/common/ztree/js/jquery.ztree.all.min.js"></script>
    <script type="text/javascript">
        var getSearchParams = function(){
            return {
                searchInput:$("input[name='searchInput']").val() || "",
            };
        }
    </script>
</head>
<body>

   <div class="ui-operation">
       <div class="ui-buttons">
   		 <div class="layui-btn-group tools">
            <button class="layui-btn layui-btn-normal open-dialog" p="url:'${params.contextPath}/view/dictionary/dictionary_edit.htm',title:'添加字典',width:'600px',height:'400px'"><i class="fa fa-plus"></i>添加</button>
         	<button class="layui-btn layui-btn-normal singleSelected" p="url:'${params.contextPath}/view/dictionary/dictionary_edit.htm',title:'编辑字典',width:'600px',height:'400px'"><i class="fa fa-pencil"></i>修改</button>
            <button class="layui-btn layui-btn-normal remove-button" reurl="${params.contextPath}/web/dictionary/remove.json"><i class="fa fa-remove"></i>删除</button>
            <button class="layui-btn layui-btn-normal accept-button" reurl="${params.contextPath}/web/dictionary/modifyState.json"><i class="fa fa-play"></i>启用</button>
            <button class="layui-btn layui-btn-normal stop-button" reurl="${params.contextPath}/web/dictionary/modifyState.json"><i class="fa fa-stop"></i>停用</button>
            <button class="layui-btn layui-btn-normal dicitem-button"><i class="fa fa-tasks"></i>查看字典项</button>
         </div>   
         <div class="ui-searchs">
             <div class="value" style="">
                 <input type="text" name="searchInput" placeholder="搜索字典名称">
             </div>
             <input type="button" value="搜索" class="layui-btn layui-btn-danger search-button"/>
         </div>
       </div>
   </div>
   <div class="ui-content" style="padding-left: 10px;padding-right:10px;">
       <table id="datagrid" options="url:'${params.contextPath}/web/dictionary/list.json',params:'getSearchParams'">
           <thead>
               <tr>
                   <th data-options="field:'id',checkbox:true"></th>
                   <th data-options="field:'name',width:80">字典名称</th>
                   <th data-options="field:'value',width:80">字典key</th>
                   <th data-options="field:'stateStr',width:80,formatter:formatState">状态</th>
                   <th data-options="field:'remark',width:80">备注</th>
<!--                    <th data-options="field:'operation',width:80,formatter:formatOperation">操作</th>
 -->               </tr>
           </thead>
       </table>
   </div>
   
</body>
<script type="text/javascript">
//审核状态格式化
function formatState(val, row) {
    if (row.state == 1) {
        return '<span class="ui-accept">' + val + '</span>';
    } else if(row.state == 2){
        return '<span class="ui-stop">' + val + '</span>';
    }
}

/* function formatOperation(val, row) {
	var btn = '';
	btn+='<span style="cursor:pointer;" class="ui-accept" onclick="openReplyDialog(\''+row.id+'\')">[查看字典项]</span>'; 
	return btn;
}

function openReplyDialog(id){
	var p = "url:'${params.contextPath}/view/dictionary/dictionaryItem_list.htm?dicId="+id+"',title:'字典项列表',width:'70%',height:'70%'";
	DialogManager.openDialog(p);
} */

$(function(){
	$(".remove-button").click(function () {
        var ids = DataGrid.getCheckedIds();
        if (ids == "") {
            $.message("请选择删除记录")
            return false;
        }
        var url = $(this).attr("reurl");
        layer.confirm('确定删除记录', function () {
            $.ajaxRequest({
                url:url,
                data:{ids:ids.join(",")},
                success:function(data){
                    $.message(data.message);
                     if (data.success) {
                        DataGrid.reload();
                    }
                }
            });
        });
    });
	
	 $(".accept-button").click(function () {
         var ids = DataGrid.getCheckedIds();
         if (ids == "") {
             $.message("请选择启用的字典")
             return false;
         }
         var url = $(this).attr("reurl");
         $.ajaxRequest({
             url:url,
             data:{ids:ids.join(","),state:1},
             success:function(data){
                 $.message(data.message);
                 if (data.success) {
                     DataGrid.reload();
                 }
             }
         });
     });
     $(".stop-button").click(function () {
         var ids = DataGrid.getCheckedIds();
         if (ids == "") {
             $.message("请选择停用的字典")
             return false;
         }
         var url = $(this).attr("reurl");
         $.ajaxRequest({
             url:url,
             data:{ids:ids.join(","),state:2},
             success:function(data){
                 $.message(data.message);
                 if (data.success) {
                     DataGrid.reload();
                 }
             }
         });
     });
     
     $(".dicitem-button").click(function () {
    	 var id = DataGrid.getSelectId();
         if (id == "" || id == undefined) {
             layer.msg("请选择一行!");
             return false;
         }
         var p = "url:'${params.contextPath}/view/dictionary/dictionaryItem_list.htm?dicId={id}',title:'字典项列表',width:'80%',height:'100%'";
         p = p.format({id:id});
         DialogManager.openDialog(p);
     });
});
</script>
</html>
