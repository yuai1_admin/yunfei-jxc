<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>商品参数列表</title>
	<#include "/common/vue_resource.ftl">
</head>
<body>
<div id="app" v-cloak>
    <div class="app-container" @click="hideMenu">
        <div class="layui-row app-header">
            <div class="layui-col-md3">
                <@auth code='productParams_add'>
                    <button type="button" class="layui-btn layui-btn-sm" @click="add">创建商品参数</button>
                </@auth>
            </div>
            <div class="layui-col-md9 text-right">
                <select v-model="params.type" class="layui-input">
                    <option value="">选择类别</option>
                    <option value="1">商品规格</option>
                    <option value="2">商品单位</option>
                    <option value="3">收入项目</option>
                    <option value="4">支出项目</option>
                    <option value="5">交易对象</option>
                </select>
                <input type="text" v-model="params.name" placeholder="商品参数名称" class="layui-input" @keyup.13="loadData"/>
                <button type="button" class="layui-btn layui-btn-sm layui-btn-primary search-button" @click="seachData">查询</button>
            </div>
        </div>
        <div class="app-list">
            <div class="layui-row">
                <div class="layui-col-md9">
                    <div class="app-table-num"><span class="num">商品参数列表(共 {{total}} 条)</span></div>
                </div>
                <div class="layui-col-md3 text-right">
                    <span class="prev" @click="loadPrev">上一页</span>
                    <span class="next" @click="loadNext">下一页</span>
                </div>
            </div>
            <table class="layui-table" lay-even lay-skin="nob" lay-size1="sm">
                <thead>
                <tr>
                    <th style="width:20px;">#</th>
                    <th>类别</th>
                    <th>名称</th>
                    <th>备注</th>
                    <th style="width:85px;">操作</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(item, index) in rows">
                    <td>{{20 * (params.page - 1) + 1 + index}}</td>
                    <td>{{item.typeStr}}</td>
                    <td>{{item.name}}</td>
                    <td>{{item.remark}}</td>
                    <td class="more-parent">
                        <@auth code='productParams_update'>
                            <div class="ui-operating" @click="modify(index)">编辑</div>
                            <div class="ui-split"></div>
                        </@auth>
                        <@auth code='productParams_delete'>
                            <div class="ui-operating" @click.stop="showMenu(index)">更多</div>
                            <div class="more-container" v-if="item.showMenu">
                                <div class="more-item" @click="remove(index)">删除</div>
                            </div>
                        </@auth>
                    </td>
                </tr>
                <tr v-if="rows.length <= 0">
                    <td colspan="5" class="text-center">没有更多数据了...</td>
                </tr>
                </tbody>
            </table>
            <div class="layui-row">
                <div class="layui-col-md6">
                    <div class="app-table-num"><span class="num">共 {{total}} 条</span></div>
                </div>
                <div class="layui-col-md6 text-right">
                    <span class="prev" @click="loadPrev">上一页</span>
                    <span class="next" @click="loadNext">下一页</span>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            params: {
                type:'',
                name:'',
                page: 1,
            },
            rows: [],
            total: 0,
        },
        mounted: function () {
            this.loadData();
        },
        methods: {
            seachData:function(){
                this.params.page = 1;
                this.$nextTick(function () {
                    this.loadData();
                });
            },
            loadData: function () {
                var that = this;
                $.http.post("${params.contextPath}/web/productParams/list.json", this.params).then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    that.rows = data.rows;
                    that.total = data.total;
                });
            },
            loadNext: function () {
                if (this.rows.length < 20 && this.rows.length > 0) {
                    return;
                }
                this.params.page = this.params.page + 1;
                this.loadData();
            },
            loadPrev: function () {
                if (this.params.page <= 1) {
                    return;
                }
                this.params.page = this.params.page - 1;
                this.loadData();
            },
            add:function () {
                this.showTypes = false;
                var url = "${params.contextPath!}/view/business/productParams/productParams_edit.htm";
                DialogManager.open({url:url, width:'650px', height:'100%', title:'添加商品参数'});
            },
            modify: function (index) {
                var row = this.rows[index];
                var url = "${params.contextPath!}/view/business/productParams/productParams_edit.htm?id=" + row.id;
                DialogManager.open({url: url, width: '650px', height: '100%', title: '编辑商品参数'});
            },
            showMenu:function (index) {
                this.hideMenu();
                this.$set(this.rows[index], "showMenu", true);
            },
            hideMenu: function () {
                var that = this;
                this.rows.forEach(function (item) {
                    that.$set(item, 'showMenu', false);
                });
            },
            remove:function (index) {//删除
                if (!confirm("确定删除吗？")) {
                    return;
                }
                var that = this;
                $.http.post("${params.contextPath}/web/productParams/delete.json", {ids: this.rows[index].id}).then(function (data) {
                    $.message(data.message);
                    if (!data.success) {
                        return;
                    }
                    that.loadData();
                });
            },
        }
    });
</script>
</body>

</html>
