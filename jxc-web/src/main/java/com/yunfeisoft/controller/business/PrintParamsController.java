package com.yunfeisoft.controller.business;

import com.applet.base.BaseController;
import com.applet.utils.Response;
import com.applet.utils.ResponseUtils;
import com.applet.utils.Validator;
import com.yunfeisoft.business.model.PrintParams;
import com.yunfeisoft.business.service.inter.PrintParamsService;
import com.yunfeisoft.model.User;
import com.yunfeisoft.utils.ApiUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * ClassName: PrintParamsController
 * Description: 打印设置信息Controller
 * Author: Jackie liu
 * Date: 2020-08-18
 */
@Controller
public class PrintParamsController extends BaseController {

    @Autowired
    private PrintParamsService printParamsService;

    /**
     * 添加打印设置信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/printParams/save", method = RequestMethod.POST)
    @ResponseBody
    public Response save(PrintParams record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "type", "单据类型为空");
        validator.required(request, "params", "参数为空");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }

        User user = ApiUtils.getLoginUser();
        record.setOrgId(user.getOrgId());

        printParamsService.save(record);
        return ResponseUtils.success("保存成功");
    }

    /**
     * 修改打印设置信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    /*@RequestMapping(value = "/web/printParams/modify", method = RequestMethod.POST)
    @ResponseBody
    public Response modify(PrintParams record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "id", "参数错误");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }
        printParamsService.modify(record);
        return ResponseUtils.success("保存成功");
    }*/

    /**
     * 查询打印设置信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/printParams/query", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response query(HttpServletRequest request, HttpServletResponse response) {
        String type = ServletRequestUtils.getStringParameter(request, "type", null);
        if (StringUtils.isBlank(type)) {
            return ResponseUtils.warn("参数错误");
        }

        User user = ApiUtils.getLoginUser();
        PrintParams record = printParamsService.loadByType(user.getOrgId(), type);
        return ResponseUtils.success(record);
    }

    /**
     * 分页查询打印设置信息
     *
     * @param request
     * @param response
     * @return
     */
    /*@RequestMapping(value = "/web/printParams/list", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response list(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> params = new HashMap<String, Object>();
        initParams(params, request);
        Page<PrintParams> page = printParamsService.queryPage(params);
        return ResponseUtils.success(page);
    }*/

    /**
     * 批量删除打印设置信息
     *
     * @param request
     * @param response
     * @return
     */
    /*@RequestMapping(value = "/web/printParams/delete", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response delete(HttpServletRequest request, HttpServletResponse response) {
        String ids = ServletRequestUtils.getStringParameter(request, "ids", null);
        if (StringUtils.isBlank(ids)) {
            return ResponseUtils.warn("参数错误");
        }
        printParamsService.remove(ids.split(","));
        return ResponseUtils.success("删除成功");
    }*/
}
