/**
 * MessageTemplateDaoImpl.java
 * Created at 2017-09-05
 * Created by Jackie liu
 * Copyright (C) 2014, All rights reserved.
 */
package com.yunfeisoft.dao.impl.postgres;

import com.applet.base.BaseDaoImpl;
import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.WhereBuilder;
import com.yunfeisoft.dao.inter.MessageTemplateDao;
import com.yunfeisoft.model.MessageTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>ClassName: MessageTemplateDaoImpl</p>
 * <p>Description: 消息模板Dao实现</p>
 * <p>Author: Jackie liu</p>
 * <p>Date: 2017-09-05</p>
 */
@Repository
public class MessageTemplateDaoImpl extends BaseDaoImpl<MessageTemplate, String> implements MessageTemplateDao {

    @Override
    public MessageTemplate queryByCode(String code) {
        WhereBuilder qc = new WhereBuilder();
        qc.andEquals("code", code);
        List<MessageTemplate> list = query(qc);
        return list.isEmpty() ? null : list.get(0);
    }
}