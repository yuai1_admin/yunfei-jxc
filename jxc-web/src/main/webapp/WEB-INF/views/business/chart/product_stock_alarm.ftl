<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>商品库存预警列表</title>
	<#include "/common/vue_resource.ftl">
</head>
<body>
<div id="app" v-cloak>
    <div class="app-container" @click="hideMenu">
        <div class="layui-row app-header">
            <div class="layui-col-md6">
                <button type="button" class="layui-btn layui-btn-sm" @click="exportExcel">导出Excel</button>
                <button type="button" class="layui-btn layui-btn-sm layui-btn-danger" @click="top.showNotice('product_stock_alarm')">使用技巧</button>
            </div>
            <div class="layui-col-md6 text-right">
                <span style="margin-top:5px;display:inline-block;">缺货/积压：</span>
                <select v-model="params.warnType" class="layui-input" @change="seachData">
                    <option value="">请选择</option>
                    <option value="outStock">缺货商品</option>
                    <option value="backlog">积压商品</option>
                    <option value="nq0">不等于0</option>
                    <option value="eq0">等于0</option>
                    <option value="gt0">大于0</option>
                    <option value="lt0">小于0</option>
                </select>
                <span style="margin-top:5px;display:inline-block;margin-left:5px;">仓库：</span>
                <select v-model="params.warehouseId" class="layui-input" @change="seachData">
                    <option value="">请选择</option>
                    <option :value="item.id" v-for="(item, index) in warehouses" :key="item.id">{{item.name}}</option>
                </select>
                <input type="text" v-model="params.namePinyin" placeholder="商品名称" class="layui-input" @keyup.13="seachData"/>
                <button type="button" class="layui-btn layui-btn-sm layui-btn-primary search-button" @click="seachData">查询</button>
            </div>
        </div>
        <div class="app-list">
            <div class="layui-row">
                <div class="layui-col-md9">
                    <div class="app-table-num"><span class="num">商品库存列表(共 {{total}} 条)</span></div>
                </div>
                <div class="layui-col-md3 text-right">
                    <span class="prev" @click="loadPrev">上一页</span>
                    <span class="next" @click="loadNext">下一页</span>
                </div>
            </div>
            <table class="layui-table" lay-even lay-skin="nob" lay-size1="sm">
                <thead>
                <tr>
                    <th style="width:20px;">#</th>
                    <th>编码</th>
                    <th>名称</th>
                    <th>规格</th>
                    <#--<th>类别</th>-->
                    <th>仓库</th>
                    <th>初始库存</th>
                    <th>当前库存</th>
                    <th>缺货下限</th>
                    <th>积压上限</th>
                    <th style1="width:185px;">操作</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(item, index) in rows" @dblclick="showDetail(index)">
                    <td>{{20 * (params.page - 1) + 1 + index}}</td>
                    <td>{{item.productCode}}</td>
                    <td>{{item.productName}}</td>
                    <td>{{item.productStandard || ""}}</td>
                    <#--<td>{{item.categoryName}}</td>-->
                    <td>{{item.warehouseName}}</td>
                    <td>{{item.initStock}}</td>
                    <td>{{item.stock}}</td>
                    <td>
                        {{item.shortageLimit}}
                        <#--{{item.shortageLimit}}/
                        <span class="ui-error" v-if="item.shortage > 0">{{item.shortage}}</span>
                        <span v-if="item.shortage <= 0">{{item.shortage}}</span>-->
                    </td>
                    <td>
                        {{item.backlogLimit}}
                        <#--{{item.backlogLimit}}/
                        <span class="ui-error" v-if="item.backlog > 0">{{item.backlog}}</span>
                        <span v-if="item.backlog <= 0">{{item.backlog}}</span>-->
                    </td>
                    <td class="more-parent">
                        <#--<div class="ui-operating" @click="modify(index)">编辑</div>
                        <div class="ui-split"></div>-->
                        <div class="ui-operating" @click="showDetail(index)">查看</div>
                        <#--<div class="ui-split"></div>
                        <div class="ui-operating" @click.stop="showMenu(index)">更多</div>
                        <div class="more-container" v-if="item.showMenu">
                            <div class="more-item" @click="remove(index)">删除</div>
                        </div>-->
                    </td>
                </tr>
                <tr v-if="rows.length <= 0">
                    <td colspan="11" class="text-center">没有更多数据了...</td>
                </tr>
                </tbody>
            </table>
            <div class="layui-row">
                <div class="layui-col-md6">
                    <div class="app-table-num"><span class="num">共 {{total}} 条</span></div>
                </div>
                <div class="layui-col-md6 text-right">
                    <span class="prev" @click="loadPrev">上一页</span>
                    <span class="next" @click="loadNext">下一页</span>
                </div>
            </div>
        </div>
    </div>

    <div style="display:none">
        <form @submit.prevent="uploadFileForm()" method="post">
            <input type="file" name="file" id="uploadFile" ref="fileUpload" single accept="application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"/>
        </form>
    </div>
</div>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            params: {
                warnType: '',
                warehouseId: '',
                productName: '',
                page: 1,
            },
            warehouses: [],
            rows: [],
            total: 0,
        },
        mounted: function () {
            this.loadWarehouses();
        },
        methods: {
            seachData:function(){
                this.params.page = 1;
                this.$nextTick(function () {
                    this.loadData();
                });
            },
            loadWarehouses: function () {
                var that = this;
                $.http.post("${params.contextPath}/web/warehouse/queryList.json", this.params).then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    that.warehouses = data.data;
                    //that.params.warehouseId = that.warehouses[0].id;
                    that.loadData();
                });
            },
            loadData: function () {
                var that = this;
                $.http.post("${params.contextPath}/web/warehouseProduct/list.json", this.params).then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    that.rows = data.rows;
                    that.total = data.total;
                });
            },
            loadNext: function () {
                if (this.rows.length < 20 && this.rows.length > 0) {
                    return;
                }
                this.params.page = this.params.page + 1;
                this.loadData();
            },
            loadPrev: function () {
                if (this.params.page <= 1) {
                    return;
                }
                this.params.page = this.params.page - 1;
                this.loadData();
            },
            showDetail: function (index) {
                var row = this.rows[index];
                var url = "${params.contextPath!}/view/business/product/product_detail.htm?id=" + row.productId;
                DialogManager.open({url: url, width: '650px', height: '100%', title: '查看商品'});
            },
            showMenu:function (index) {
                this.hideMenu();
                this.$set(this.rows[index], "showMenu", true);
            },
            hideMenu: function () {
                var that = this;
                this.rows.forEach(function (item) {
                    that.$set(item, 'showMenu', false);
                });
            },
            exportExcel: function () {
                var url = "${params.contextPath}/web/warehouseProduct/exportExcel.htm?productName=" + (this.params.productName || "");
                url += "&warnType=" + this.params.warnType;
                url += "&warehouseId=" + this.params.warehouseId;
                location.href = url;
            },
        }
    });
</script>
</body>

</html>
