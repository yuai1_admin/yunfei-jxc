<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>编辑字典项信息</title>
    <link rel="stylesheet" type="text/css" href="${params.contextPath}/common/ztree/css/zTreeStyle/zTreeStyle.css"/>
    <#include "/common/resource.ftl">
    <script type="text/javascript" src="${params.contextPath}/common/ztree/js/jquery.ztree.all.min.js"></script>
    <script type="text/javascript">
        $(function () {
            <#if (params.id)??>
                $.ajaxRequest({
                    url:'${params.contextPath}/web/dictionaryItem/query.json',
                    data:{id:"${params.id}"},
                    success:function (data) {
                        if (!data.success) {
                            layer.msg(data.message);
                            return;
                        }
                        var record = data.data;
                        for (var key in record) {
                            $("[name='"+key+"']").val(record[key]);
                        }
                    }
                });
            </#if>
        });
    </script>
</head>
<body>
    <div class="ui-form">
        <form class="layui-form ajax-form" action="${params.contextPath}/web/dictionaryItem/<#if (params.id)??>modify<#else>save</#if>.json" method="post">
       		<input name="id" type="hidden" value="<#if (param.id)?exists>${param.id}</#if>"/>
       		<input type="hidden" name="dictionaryId" value="${params.dicId}"/>
           	<div class="layui-form-item">
               <label class="layui-form-label">字典项名称<span class="ui-required">*</span></label>
               <div class="layui-input-block">
                   <input type="text" name="name" placeholder="请输入字典名称" class="layui-input" maxlength="25"/>
               </div>
           	</div>
           	<div class="layui-form-item">
                <label class="layui-form-label">字典项值<span class="ui-required">*</span></label>
                <div class="layui-input-block">
                    <input type="text" name="value" placeholder="请输入字典项值" class="layui-input" maxlength="25"/>
                </div>
           	</div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <input type="submit" value="保存" class="layui-btn"/>
                </div>
            </div>
        </form>
    </div>
</body>

</html>
