package com.yunfeisoft.business.service.impl;

import com.applet.base.BaseServiceImpl;
import com.applet.sql.separation.DataSourceChange;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.ProductDao;
import com.yunfeisoft.business.dao.inter.PurchaseItemDao;
import com.yunfeisoft.business.dao.inter.WarehouseProductDao;
import com.yunfeisoft.business.model.Product;
import com.yunfeisoft.business.model.PurchaseItem;
import com.yunfeisoft.business.model.WarehouseProduct;
import com.yunfeisoft.business.service.inter.ProductService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * ClassName: ProductServiceImpl
 * Description: 商品信息service实现
 * Author: Jackie liu
 * Date: 2020-07-23
 */
@Service("productService")
public class ProductServiceImpl extends BaseServiceImpl<Product, String, ProductDao> implements ProductService {

    @Autowired
    private WarehouseProductDao warehouseProductDao;
    @Autowired
    private PurchaseItemDao purchaseItemDao;

    @Override
    @DataSourceChange(slave = true)
    public Page<Product> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }

    @Override
    public List<Product> queryList(Map<String, Object> params) {
        return getDao().queryList(params);
    }

    @Override
    public boolean isDupName(String orgId, String id, String name) {
        return getDao().isDupName(orgId, id, name);
    }

    @Override
    public boolean isDupCode(String orgId, String id, String code) {
        return getDao().isDupCode(orgId, id, code);
    }

    @Override
    public int save(Product product) {
        if (CollectionUtils.isNotEmpty(product.getWarehouseProductList())) {
            warehouseProductDao.batchInsert(product.getWarehouseProductList());
        }
        return super.save(product);
    }

    @Override
    public int modify(Product product) {
        if (CollectionUtils.isNotEmpty(product.getWarehouseProductList())) {
            //所有仓库的动态库存总和
            BigDecimal initStock = BigDecimal.ZERO;
            for (WarehouseProduct warehouseProduct : product.getWarehouseProductList()) {
                WarehouseProduct wpStorage = warehouseProductDao.loadSingle(product.getId(), warehouseProduct.getWarehouseId());
                if (wpStorage == null) {
                    initStock = initStock.add(warehouseProduct.getInitStock());
                    warehouseProduct.setStock(warehouseProduct.getInitStock());
                    warehouseProductDao.insert(warehouseProduct);
                    continue;
                }

                //1 计算动态库存 = (新修改的初始库存 - 库中存在的初始库存) + 库中动态库存  动态库存指stock
                BigDecimal s = warehouseProduct.getInitStock().subtract(wpStorage.getInitStock()).add(wpStorage.getStock());
                warehouseProduct.setStock(s);

                initStock = initStock.add(warehouseProduct.getInitStock());
                warehouseProduct.setId(wpStorage.getId());
                warehouseProductDao.update(warehouseProduct);
            }

            //获取该商品所有进货单的金额和数量
            BigDecimal totalAmount = BigDecimal.ZERO;
            BigDecimal totalNum = BigDecimal.ZERO;
            PurchaseItem purchaseItem = purchaseItemDao.queryAmountAndQuantity(product.getId());
            totalAmount = purchaseItem.getAmount() == null ? BigDecimal.ZERO : purchaseItem.getAmount();
            totalNum = purchaseItem.getQuantity() == null ? BigDecimal.ZERO : purchaseItem.getQuantity();

            //2 计算平均价格 = （新的进价*第一步中的动态库存 + 所有采购单中的该商品总额）/ (第一步中的动态库存 + 所有进货单库存)
            totalNum = totalNum.add(initStock);
            if (totalNum.compareTo(BigDecimal.ZERO) <= 0) {
                product.setAvgPrice(product.getBuyPrice());
            } else {
                totalAmount = totalAmount.add(product.getBuyPrice().multiply(initStock));
                BigDecimal avgPrice = totalAmount.divide(totalNum, 2, 4);
                product.setAvgPrice(avgPrice);
            }
        }
        return super.modify(product);
    }
}