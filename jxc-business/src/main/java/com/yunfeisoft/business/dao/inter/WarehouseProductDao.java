package com.yunfeisoft.business.dao.inter;

import com.applet.base.BaseDao;
import com.yunfeisoft.business.model.WarehouseProduct;
import com.applet.utils.Page;

import java.util.List;
import java.util.Map;

/**
 * ClassName: WarehouseProductDao
 * Description: 仓库商品信息Dao
 * Author: Jackie liu
 * Date: 2020-08-04
 */
public interface WarehouseProductDao extends BaseDao<WarehouseProduct, String> {

    public Page<WarehouseProduct> queryPage(Map<String, Object> params);

    public List<WarehouseProduct> queryList(Map<String, Object> params);

    public WarehouseProduct loadSingle(String productId, String warehouseId);

    public WarehouseProduct queryStockSum(String productId);
}