<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>流程任务信息列表</title>
	<#include "/common/resource.ftl">
</head>
<style>
    body{background:#eee;}
    .ui-container{padding:10px;}
    .select-icon{width:50px;text-align:center;font-size:18px !important;cursor:pointer;color:#FF5722;}
    .layui-card-body{overflow:auto;}
    .unselect{color:#c2c2c2;}
    .fa-minus-circle{color:#1E9FFF}
</style>
<body>
<div class="ui-container">
    <form class="layui-form ajax-form" action="${params.contextPath}/web/taskUser/save.json" method="post">
        <input type="hidden" name="userIds" value="">
        <input type="hidden" name="taskKey" value="${params.taskKey!}">
        <div class="layui-row layui-col-space10">
            <div class="layui-col-xs6 layui-col-md6">
                <div class="layui-card">
                    <div class="layui-card-header">可选用户</div>
                    <div class="layui-card-body">
                        <table class="layui-table user-table" lay-even lay-skin="line" lay-size="sm">
                            <tbody>
                            <#--<tr>
                                <td>贤心</td>
                                <td class="select-icon unselect"><i class="fa fa-plus-circle"></i></td>
                            </tr>
                            <tr>
                                <td>许闲心</td>
                                <td class="select-icon"><i class="fa fa-plus-circle"></i></td>
                            </tr>-->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="layui-col-xs6 layui-col-md6">
                <div class="layui-card">
                    <div class="layui-card-header">已选用户
                        <button type="submit" class="layui-btn layui-btn-danger layui-btn-sm" style="float:right;margin-top:5px;">保存</button>
                    </div>
                    <div class="layui-card-body">
                        <table class="layui-table selected-table" lay-even lay-skin="line" lay-size="sm">
                            <tbody>
                            <#--<tr>
                                <td>贤心</td>
                                <td class="select-icon unselect"><i class="fa fa-minus-circle"></i></td>
                            </tr>
                            <tr>
                                <td>许闲心</td>
                                <td class="select-icon"><i class="fa fa-minus-circle"></i></td>
                            </tr>-->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script id="user-template" type="text/html" style="display: none">
        {{ each list item index }}
        <tr data-id="{{item.id}}">
            <td>{{item.orgName}}/{{item.name}}</td>
            <td class="select-icon"><i class="fa fa-plus-circle"></i></td>
        </tr>
        {{ /each }}
    </script>
</div>
<script type="text/javascript">
    var userMap = {}, selectedMap = [];
    //获取用户列表
    var getUserData = function() {
        $.ajaxRequest({
            url: '${params.contextPath}/web/user/queryList.json',
            //data: {id: ""},
            success: function (data) {
                if (!data.success) {
                    $.message(data.message);
                    return;
                }
                var list = data.data;
                for (var i = 0; i < list.length; i ++) {
                    var item = list[i];
                    item.orgName = item.orgName ? item.orgName : "--";
                    userMap[item.id] = item;
                }
                var html = $.template("user-template", {list : list});
                $(".user-table tbody").html(html);

                getSelectedUserData();
            }
        });
    };
    //获取已配置的用户
    var getSelectedUserData = function(){
        $.ajaxRequest({
            url: '${params.contextPath}/web/taskUser/list.json',
            data: {taskKey: "${params.taskKey!}"},
            success: function (data) {
                if (!data.success) {
                    $.message(data.message);
                    return;
                }
                var list = data.data, userIds = [], trs = "";
                for (var i = 0; i < list.length; i++) {
                    var item = list[i];
                    var user = userMap[item.userId] || "";
                    if (!user) {
                        continue;
                    }
                    userIds.push(item.userId);
                    $("[data-id='" + item.userId + "'] .select-icon").addClass("unselect");
                    var str = '<tr data-id="{id}">' +
                        '<td>{orgName}/{name}</td>' +
                        ' <td class="select-icon"><i class="fa fa-minus-circle"></i></td>' +
                        '</tr>';
                    trs += str.format(user);
                }
                $('[name="userIds"]').val(userIds.join(","));
                $('.selected-table tbody').html(trs);
            }
        });
    };
    $(function () {
        $(".layui-card-body").height($(window).height() - 85);
        getUserData();

        //选择用户
        $(".user-table").on("click", ".select-icon", function () {
            if ($(this).hasClass("unselect")) {
                return false;
            }
            var userId = $(this).parents("tr").attr("data-id");
            $(this).addClass("unselect");
            var userIds = $('[name="userIds"]').val() || "";
            userIds = userIds ? userIds.split(",") : [];
            userIds.push(userId);
            $('[name="userIds"]').val(userIds.join(","));

            var user = userMap[userId] || "";
            $("[data-id='" + userId + "'] .select-icon").addClass("unselect");
            var str = '<tr data-id="{id}">' +
                '<td>{orgName}/{name}</td>' +
                ' <td class="select-icon"><i class="fa fa-minus-circle"></i></td>' +
                '</tr>';
            $('.selected-table tbody').append(str.format(user));
        });

        //删除用户
        $(".selected-table").on("click", ".select-icon", function () {
            var userId = $(this).parents("tr").attr("data-id");
            var userIds = $('[name="userIds"]').val().split(","), newIds = [];
            for (var i = 0; i < userIds.length; i ++) {
                var id = userIds[i];
                if (id == userId) {
                    continue;
                }
                newIds.push(id);
            }
            $('[name="userIds"]').val(newIds.join(","));
            $("[data-id='" + userId + "'] .select-icon").removeClass("unselect");
            $(this).parents("tr").remove();
        });
    });
</script>
</body>

</html>
