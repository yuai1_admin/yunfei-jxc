package com.yunfeisoft.business.service.inter;

import com.applet.base.BaseService;
import com.yunfeisoft.business.model.ProductParams;
import com.applet.utils.Page;

import java.util.Map;

/**
 * ClassName: ProductParamsService
 * Description: 商品参数信息service接口
 * Author: Jackie liu
 * Date: 2020-07-23
 */
public interface ProductParamsService extends BaseService<ProductParams, String> {

    public Page<ProductParams> queryPage(Map<String, Object> params);
}