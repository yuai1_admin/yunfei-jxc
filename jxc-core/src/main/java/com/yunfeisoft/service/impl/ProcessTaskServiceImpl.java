package com.yunfeisoft.service.impl;

import com.applet.base.BaseServiceImpl;
import com.applet.sql.separation.DataSourceChange;
import com.applet.utils.Page;
import com.yunfeisoft.dao.inter.ProcessTaskDao;
import com.yunfeisoft.model.ProcessTask;
import com.yunfeisoft.service.inter.ProcessTaskService;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * ClassName: ProcessTaskServiceImpl
 * Description: 流程任务(userTask)信息service实现
 * Author: Jackie liu
 * Date: 2019-07-08
 */
@Service("processTaskService")
public class ProcessTaskServiceImpl extends BaseServiceImpl<ProcessTask, String, ProcessTaskDao> implements ProcessTaskService {

    @Override
    @DataSourceChange(slave = true)
    public Page<ProcessTask> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }

    @Override
    public boolean isDuplicateKey(String id, String processKey, String key) {
        return getDao().isDuplicateKey(id, processKey, key);
    }
}