package com.yunfeisoft.business.dao.impl.postgres;

import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.SelectBuilder;
import com.applet.sql.builder.WhereBuilder;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.AllotOrderDao;
import com.yunfeisoft.business.model.AllotOrder;
import com.yunfeisoft.business.model.Warehouse;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * ClassName: AllotOrderDaoImpl
 * Description: 调拨单信息Dao实现
 * Author: Jackie liu
 * Date: 2020-08-06
 */
@Repository
public class AllotOrderDaoImpl extends ServiceDaoImpl<AllotOrder, String> implements AllotOrderDao {

    @Override
    public Page<AllotOrder> queryPage(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        wb.setOrderByWithDesc("ao.createTime");
        if (params != null) {
            initPageParam(wb, params);
            wb.andEquals("ao.orgId", params.get("orgId"));
            wb.andEquals("ao.outWarehouseId", params.get("outWarehouseId"));
            wb.andEquals("ao.inWarehouseId", params.get("inWarehouseId"));
        }

        SelectBuilder builder = getSelectBuilder("ao");
        builder.column("wo.name as outWarehouseName")
                .column("wi.name as inWarehouseName")
                .join(Warehouse.class).alias("wo").on("ao.outWarehouseId = wo.id").build()
                .join(Warehouse.class).alias("wi").on("ao.inWarehouseId = wi.id").build();

        return queryPage(builder.getSql(), wb);
    }

}