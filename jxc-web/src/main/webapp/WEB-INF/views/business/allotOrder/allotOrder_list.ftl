<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>调拨单信息列表</title>
	<#include "/common/vue_resource.ftl">
</head>
<body>
<div id="app" v-cloak>
    <div class="app-container" @click="hideMenu">
        <div class="layui-row app-header">
            <div class="layui-col-md3">
                <@auth code='allotOrder_add'>
                    <button type="button" class="layui-btn layui-btn-sm" @click="add">创建调拨单</button>
                </@auth>
                <button type="button" class="layui-btn layui-btn-sm layui-btn-danger" @click="top.showNotice('allot_order_list')">使用技巧</button>
            </div>
            <div class="layui-col-md9 text-right">
                <span style="margin-top:5px;display:inline-block;">调出仓库：</span>
                <select v-model="params.outWarehouseId" class="layui-input" @change="loadData" style="margin-right:10px;">
                    <option value="">请选择</option>
                    <option :value="item.id" v-for="(item, index) in warehouses" :key="item.id">{{item.name}}</option>
                </select>
                <span style="margin-top:5px;display:inline-block;">调入仓库：</span>
                <select v-model="params.inWarehouseId" class="layui-input" @change="loadData">
                    <option value="">请选择</option>
                    <option :value="item.id" v-for="(item, index) in warehouses" :key="item.id">{{item.name}}</option>
                </select>
                <#--<input type="text" v-model="params.outWarehouseName" placeholder="调出仓库名称" class="layui-input">-->
                <button type="button" class="layui-btn layui-btn-sm layui-btn-primary search-button" @click="seachData">查询</button>
            </div>
        </div>
        <div class="app-list">
            <div class="layui-row">
                <div class="layui-col-md9">
                    <div class="app-table-num"><span class="num">调拨单列表(共 {{total}} 条)</span></div>
                </div>
                <div class="layui-col-md3 text-right">
                    <span class="prev" @click="loadPrev">上一页</span>
                    <span class="next" @click="loadNext">下一页</span>
                </div>
            </div>
            <table class="layui-table" lay-even lay-skin="nob" lay-size1="sm">
                <thead>
                <tr>
                    <th style="width:20px;">#</th>
                    <th>订单编号</th>
                    <th>调出仓库</th>
                    <th>调入仓库</th>
                    <th>调拨日期</th>
                    <th>调拨状态</th>
                    <th>备注</th>
                    <th style="width:120px;">操作</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(item, index) in rows" <@auth code='allotOrder_detail'> @dblclick="showDetail(index)" </@auth>>
                    <td>{{20 * (params.page - 1) + 1 + index}}</td>
                    <td>{{item.code}}</td>
                    <td>{{item.outWarehouseName}}</td>
                    <td>{{item.inWarehouseName}}</td>
                    <td>{{item.allotDate}}</td>
                    <td>
                        <span class="ui-warn" v-if="item.status == 1">{{item.statusStr}}</span>
                        <span class="ui-accept" v-if="item.status == 2">{{item.statusStr}}</span>
                        <span class="ui-error" v-if="item.status == 3">{{item.statusStr}}</span>
                    </td>
                    <td>{{item.remark}}</td>
                    <td class="more-parent">
                        <@auth code='change_allot_order'>
                            <div class="ui-operating" v-if="item.status == 2" @click="modify(index)">改单</div>
                            <div class="ui-split" v-if="item.status == 2"></div>
                        </@auth>
                        <@auth code='allotOrder_update'>
                            <div class="ui-operating" v-if="item.status != 2" @click="modify(index)">编辑</div>
                            <div class="ui-split" v-if="item.status != 2"></div>
                        </@auth>
                        <@auth code='allotOrder_detail'>
                            <div class="ui-operating" @click="showDetail(index)">查看</div>
                            <div class="ui-split"></div>
                        </@auth>
                        <@auth code='allotOrder_delete'>
                            <div class="ui-operating" @click="remove(index)">删除</div>
                        </@auth>
                    </td>
                </tr>
                <tr v-if="rows.length <= 0">
                    <td colspan="8" class="text-center">没有更多数据了...</td>
                </tr>
                </tbody>
            </table>
            <div class="layui-row">
                <div class="layui-col-md6">
                    <div class="app-table-num"><span class="num">共 {{total}} 条</span></div>
                </div>
                <div class="layui-col-md6 text-right">
                    <span class="prev" @click="loadPrev">上一页</span>
                    <span class="next" @click="loadNext">下一页</span>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            params: {
                outWarehouseId:'',
                inWarehouseId:'',
                page: 1,
            },
            warehouses:[],
            rows: [],
            total: 0,
        },
        mounted: function () {
            this.loadWarehouses();
        },
        methods: {
            seachData:function(){
                this.params.page = 1;
                this.$nextTick(function () {
                    this.loadData();
                });
            },
            loadWarehouses: function () {
                var that = this;
                $.http.post("${params.contextPath}/web/warehouse/queryList.json", this.params).then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    that.warehouses = data.data;
                    //that.params.warehouseId = that.warehouses[0].id;
                    that.loadData();
                });
            },
            loadData: function () {
                var that = this;
                $.http.post("${params.contextPath}/web/allotOrder/list.json", this.params).then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    that.rows = data.rows;
                    that.total = data.total;
                });
            },
            loadNext: function () {
                if (this.rows.length < 20 && this.rows.length > 0) {
                    return;
                }
                this.params.page = this.params.page + 1;
                this.loadData();
            },
            loadPrev: function () {
                if (this.params.page <= 1) {
                    return;
                }
                this.params.page = this.params.page - 1;
                this.loadData();
            },
            add:function () {
                this.showTypes = false;
                var url = "${params.contextPath!}/view/business/allotOrder/allotOrder_edit.htm";
                DialogManager.open({url:url, width:'90%', height:'100%', title:'添加调拨单', cancel:function (index, layero) {
                        return confirm("单据未保存\n点【确定】表示不保存直接退出\n点【取消】回到单据界面保存");
                    }});
            },
            modify: function (index) {
                var row = this.rows[index];
                var url = "${params.contextPath!}/view/business/allotOrder/allotOrder_edit.htm?id=" + row.id;
                DialogManager.open({url: url, width: '90%', height: '100%', title: '编辑调拨单', cancel:function (index, layero) {
                        return confirm("单据未保存\n点【确定】表示不保存直接退出\n点【取消】回到单据界面保存");
                    }});
            },
            showDetail: function (index) {
                var row = this.rows[index];
                var url = "${params.contextPath!}/view/business/allotOrder/allotOrder_detail.htm?id=" + row.id;
                DialogManager.open({url: url, width: '90%', height: '100%', title: '查看销售单'});
            },
            /*changeOrder: function (index) {
                var that = this;
                $.http.post("${params.contextPath}/web/allotOrder/changeOrder.json", {id: this.rows[index].id}).then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    //$.message("改单成功，现在可以修改或者删除订单");
                    that.loadData();
                    that.modify(index);
                });
            },*/
            showMenu:function (index) {
                this.hideMenu();
                this.$set(this.rows[index], "showMenu", true);
            },
            hideMenu: function () {
                var that = this;
                this.rows.forEach(function (item) {
                    that.$set(item, 'showMenu', false);
                });
            },
            remove:function (index) {//删除
                if (!confirm("确定删除订单吗?")) {
                    return;
                }
                var that = this;
                $.http.post("${params.contextPath}/web/allotOrder/delete.json", {ids: this.rows[index].id}).then(function (data) {
                    $.message(data.message);
                    if (!data.success) {
                        return;
                    }
                    that.loadData();
                });
            },
        }
    });
</script>
</body>

</html>
