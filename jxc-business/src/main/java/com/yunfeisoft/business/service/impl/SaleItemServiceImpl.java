package com.yunfeisoft.business.service.impl;

import com.applet.base.BaseServiceImpl;
import com.applet.sql.separation.DataSourceChange;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.SaleItemDao;
import com.yunfeisoft.business.model.OrderItemStatistics;
import com.yunfeisoft.business.model.SaleItem;
import com.yunfeisoft.business.service.inter.SaleItemService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * ClassName: SaleItemServiceImpl
 * Description: 销售单商品信息service实现
 * Author: Jackie liu
 * Date: 2020-07-23
 */
@Service("saleItemService")
public class SaleItemServiceImpl extends BaseServiceImpl<SaleItem, String, SaleItemDao> implements SaleItemService {

    @Override
    @DataSourceChange(slave = true)
    public Page<SaleItem> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }

    @Override
    public List<SaleItem> queryBySaleOrderId(String saleOrderId) {
        return getDao().queryBySaleOrderId(saleOrderId);
    }

    @Override
    public List<OrderItemStatistics> queryStatistics(String orgId, String productId) {
        return getDao().queryStatistics(orgId, productId);
    }
}