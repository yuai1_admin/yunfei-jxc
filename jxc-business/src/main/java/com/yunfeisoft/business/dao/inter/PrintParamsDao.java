package com.yunfeisoft.business.dao.inter;

import com.applet.base.BaseDao;
import com.applet.utils.Page;
import com.yunfeisoft.business.model.PrintParams;

import java.util.Map;

/**
 * ClassName: PrintParamsDao
 * Description: 打印设置信息Dao
 * Author: Jackie liu
 * Date: 2020-08-18
 */
public interface PrintParamsDao extends BaseDao<PrintParams, String> {

    public Page<PrintParams> queryPage(Map<String, Object> params);

    public PrintParams loadByType(String orgId, String type);
}