/**
 * DictionaryController.java
 * Created at 2017-07-06
 * Created by Jackie liu
 * Copyright (C) 2014, All rights reserved.
 */
package com.yunfeisoft.controller.base;

import com.applet.base.BaseController;
import com.applet.utils.*;
import com.yunfeisoft.model.Dictionary;
import com.yunfeisoft.model.DictionaryItem;
import com.yunfeisoft.service.inter.DictionaryItemService;
import com.yunfeisoft.service.inter.DictionaryService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>ClassName: DictionaryController</p>
 * <p>Description: 字典管理Controller</p>
 * <p>Author: Jackie liu</p>
 * <p>Date: 2017-07-06</p>
 */

/**
 * Created by Jackie Liu on 2017/3/18.
 */
@Controller
public class DictionaryController extends BaseController {

    @Autowired
    private DictionaryService dictionaryService;
    @Autowired
    private DictionaryItemService dictionaryItemService;

    /**
     * 添加字典管理
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/dictionary/save", method = RequestMethod.POST)
    @ResponseBody
    public Response save(Dictionary record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "name", "字典名称为空");
        validator.required(request, "value", "字典值为空");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }

        boolean isDuplicateName = dictionaryService.isDuplicateName(record.getId(), record.getName());
        if (isDuplicateName) {
            return ResponseUtils.warn("该字典名称已经存在");
        }

        boolean isDuplicateValue = dictionaryService.isDuplicateValue(record.getId(), record.getValue());
        if (isDuplicateValue) {
            return ResponseUtils.warn("该字典值已经存在");
        }

        record.setId(KeyUtils.getKey());
        record.setState(Dictionary.StateEnum.START_USING.getValue());
        dictionaryService.save(record);
        return ResponseUtils.success("保存成功");
    }

    /**
     * 修改字典管理
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/dictionary/modify", method = RequestMethod.POST)
    @ResponseBody
    public Response modify(Dictionary record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "name", "字典名称为空");
        validator.required(request, "value", "字典值为空");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }

        boolean isDuplicateName = dictionaryService.isDuplicateName(record.getId(), record.getName());
        if (isDuplicateName) {
            return ResponseUtils.warn("该字典名称已经存在");
        }

        boolean isDuplicateValue = dictionaryService.isDuplicateValue(record.getId(), record.getValue());
        if (isDuplicateValue) {
            return ResponseUtils.warn("该字典值已经存在");
        }

        dictionaryService.modify(record);
        return ResponseUtils.success("保存成功");
    }

    /**
     * 查询字典管理
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/dictionary/query", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response query(HttpServletRequest request, HttpServletResponse response) {
        String id = ServletRequestUtils.getStringParameter(request, "id", null);
        if (StringUtils.isBlank(id)) {
            return ResponseUtils.warn("参数错误");
        }
        Dictionary record = dictionaryService.load(id);
        return ResponseUtils.success("sucesss", record);
    }

    /**
     * 查询字典管理
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/dictionary/queryByValue", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response queryByValue(HttpServletRequest request, HttpServletResponse response) {
        String dictionaryValue = ServletRequestUtils.getStringParameter(request, "dictionaryValue", null);
        if (StringUtils.isBlank(dictionaryValue)) {
            return ResponseUtils.warn("参数错误");
        }

        Map<String, Object> params = new HashMap<>();
        params.put("value", dictionaryValue);

        List<Dictionary> list = dictionaryService.queryList(params);

        return ResponseUtils.success("sucesss", list);
    }

    /**
     * 分页查询字典管理
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/dictionary/list", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response list(HttpServletRequest request, HttpServletResponse response) {
        String searchInput = ServletRequestUtils.getStringParameter(request, "searchInput", null);
        Map<String, Object> params = new HashMap<String, Object>();
        initParams(params, request);
        params.put("searchInput", searchInput);
        initParams(params, request);
        Page<Dictionary> page = dictionaryService.queryPage(params);
        return ResponseUtils.success(page);
    }

    /**
     * 批量发布字典项
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/dictionary/modifyState", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response modifyState(HttpServletRequest request, HttpServletResponse response) {
        String ids = ServletRequestUtils.getStringParameter(request, "ids", null);
        Integer state = ServletRequestUtils.getIntParameter(request, "state", 0);
        if (state == 0) {
            return ResponseUtils.warn("操作状态值错误");
        }
        String[] idArr = ids.split(",");
        if (state == Dictionary.StateEnum.START_USING.getValue()) {
            if (StringUtils.isEmpty(ids)) {
                return ResponseUtils.warn("请勾选需要启用的字典");
            }
            dictionaryService.modifyState(idArr, Dictionary.StateEnum.START_USING.getValue());
            return ResponseUtils.success("启用成功");
        } else {
            if (StringUtils.isEmpty(ids)) {
                return ResponseUtils.warn("请勾选需要停用的字典");
            }
            dictionaryService.modifyState(idArr, Dictionary.StateEnum.END_USING.getValue());
            return ResponseUtils.success("停用成功");
        }
    }

    /**
     * 批量删除字典
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/dictionary/remove", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response remove(HttpServletRequest request, HttpServletResponse response) {
        String ids = ServletRequestUtils.getStringParameter(request, "ids", null);
        if (StringUtils.isEmpty(ids)) {
            return ResponseUtils.warn("参数错误");
        }
        String[] idArr = ids.split(",");
        dictionaryItemService.removeByDicId(idArr);
        dictionaryService.remove(idArr);
        return ResponseUtils.success("删除成功");
    }

    /**
     * 获取有效的字典项列表
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/dictionary/queryAcceptList", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response queryAcceptList(HttpServletRequest request, HttpServletResponse response) {
        String dictionaryName = ServletRequestUtils.getStringParameter(request, "dictionaryName", null);
        List<DictionaryItem> list = dictionaryItemService.queryAcceptList(dictionaryName);
        return ResponseUtils.success(list);
    }

    /**
     * 获取有效的字典项列表
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/wx/dictionary/queryAcceptList", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response wxQueryAcceptList(HttpServletRequest request, HttpServletResponse response) {
        return queryAcceptList(request, response);
    }

    /**
     * 获取有效的字典项树列表
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/dictionary/queryAcceptTree", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response queryAcceptTree(HttpServletRequest request, HttpServletResponse response) {
        String dictionaryValue = ServletRequestUtils.getStringParameter(request, "dictionaryValue", null);
        List<DictionaryItem> list = dictionaryItemService.queryAcceptList(dictionaryValue);

        List<Tree> treeList = new ArrayList<>();
        for (DictionaryItem item : list) {
            Tree tree = new Tree(item.getId(), item.getDictionaryId(), item.getName(), item);
            treeList.add(tree);
        }

        return ResponseUtils.success(treeList);
    }
}
