/**
 * AreaService.java
 * Created at 2017-07-06
 * Created by Jackie liu
 * Copyright (C) 2014 , All rights reserved.
 */
package com.yunfeisoft.service.inter;

import com.applet.base.BaseService;
import com.applet.utils.Page;
import com.yunfeisoft.model.Area;

import java.util.List;
import java.util.Map;

/**
 * <p>ClassName: AreaService</p>
 * <p>Description: 区域管理service接口</p>
 * <p>Author: Jackie liu</p>
 * <p>Date: 2017-07-06</p>
 */
public interface AreaService extends BaseService<Area, String> {

    public Page<Area> queryPage(Map<String, Object> params);

    public int batchRemove(String[] ids);

    public int modifyState(int state, String[] ids);

    public List<Area> query(Map<String, Object> params);

    public boolean isDuplicateCode(String id, String code);

    public boolean isDuplicateName(String id, String name, String parentId);

    /**
     * 拖拽变换组织节点从属
     *
     * @param id       要变换的节点id
     * @param targetId 变换到目标节点id
     * @return
     */
    public int modifyWithDrag(String id, String targetId);

    public int insert(Area area);

    public List<Area> queryRootList(Map<String, Object> params);

    public List<Area> querySecondList(String parentId, int state);
}