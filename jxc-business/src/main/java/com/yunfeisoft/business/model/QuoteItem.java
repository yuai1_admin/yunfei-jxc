package com.yunfeisoft.business.model;

import com.applet.base.ServiceModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * ClassName: QuoteItem
 * Description: 报价单商品信息
 *
 * @Author: Jackie liu
 * Date: 2020-09-01
 */
@Entity
@Table(name = "TT_QUOTE_ITEM")
public class QuoteItem extends ServiceModel implements Serializable {

    /**
     * Field serialVersionUID: 序列号
     */
    private static final long serialVersionUID = 1L;

    /**
     * 组织id
     */
    @Column
    private String orgId;

    /**
     * 报价单id
     */
    @Column
    private String quoteOrderId;

    /**
     * 商品id
     */
    @Column
    private String productId;

    /**
     * 商品名称
     */
    @Column
    private String productName;

    /**
     * 单价
     */
    @Column
    private BigDecimal price;

    /**
     * 备注
     */
    @Column
    private String remark;


    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getQuoteOrderId() {
        return quoteOrderId;
    }

    public void setQuoteOrderId(String quoteOrderId) {
        this.quoteOrderId = quoteOrderId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }


}