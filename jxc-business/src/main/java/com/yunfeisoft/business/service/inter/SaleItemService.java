package com.yunfeisoft.business.service.inter;

import com.applet.base.BaseService;
import com.yunfeisoft.business.model.OrderItemStatistics;
import com.yunfeisoft.business.model.SaleItem;
import com.applet.utils.Page;

import java.util.List;
import java.util.Map;

/**
 * ClassName: SaleItemService
 * Description: 销售单商品信息service接口
 * Author: Jackie liu
 * Date: 2020-07-23
 */
public interface SaleItemService extends BaseService<SaleItem, String> {

    public Page<SaleItem> queryPage(Map<String, Object> params);

    public List<SaleItem> queryBySaleOrderId(String saleOrderId);

    public List<OrderItemStatistics> queryStatistics(String orgId, String productId);
}