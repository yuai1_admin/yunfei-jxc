package com.yunfeisoft.controller.base;

import com.applet.base.BaseController;
import com.applet.utils.Page;
import com.applet.utils.Response;
import com.applet.utils.ResponseUtils;
import com.applet.utils.Validator;
import com.yunfeisoft.model.Process;
import com.yunfeisoft.service.inter.ProcessService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * ClassName: ProcessController
 * Description: 流程信息Controller
 * Author: Jackie liu
 * Date: 2019-07-08
 */
@Controller
public class ProcessController extends BaseController {

    @Autowired
    private ProcessService processService;

    /**
     * 添加流程信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/process/save", method = RequestMethod.POST)
    @ResponseBody
    public Response save(Process record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "key", "流程KEY为空");
        validator.required(request, "name", "流程名称为空");
        validator.required(request, "bpmnPath", "BPMN路径为空");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }

        boolean duplicateKey = processService.isDuplicateKey(record.getId(), record.getKey());
        if (duplicateKey) {
            return ResponseUtils.warn("该流程KEY已存在");
        }
        processService.save(record);
        return ResponseUtils.success("保存成功");
    }

    /**
     * 修改流程信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/process/modify", method = RequestMethod.POST)
    @ResponseBody
    public Response modify(Process record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "id", "参数错误");
        validator.required(request, "key", "流程KEY为空");
        validator.required(request, "name", "流程名称为空");
        validator.required(request, "bpmnPath", "BPMN路径为空");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }

        boolean duplicateKey = processService.isDuplicateKey(record.getId(), record.getKey());
        if (duplicateKey) {
            return ResponseUtils.warn("该流程KEY已存在");
        }
        processService.modify(record);
        return ResponseUtils.success("保存成功");
    }

    /**
     * 查询流程信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/process/query", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response query(HttpServletRequest request, HttpServletResponse response) {
        String id = ServletRequestUtils.getStringParameter(request, "id", null);
        if (StringUtils.isBlank(id)) {
            return ResponseUtils.warn("参数错误");
        }
        Process record = processService.load(id);
        return ResponseUtils.success(record);
    }

    /**
     * 流程部署
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/process/deploy", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response deploy(HttpServletRequest request, HttpServletResponse response) {
        String id = ServletRequestUtils.getStringParameter(request, "id", null);
        if (StringUtils.isBlank(id)) {
            return ResponseUtils.warn("参数错误");
        }
        processService.saveDeploy(id);
        return ResponseUtils.success("部署成功");
    }

    /**
     * 强制删除流程部署
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/process/removeDeploy", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response removeDeploy(HttpServletRequest request, HttpServletResponse response) {
        String id = ServletRequestUtils.getStringParameter(request, "id", null);
        if (StringUtils.isBlank(id)) {
            return ResponseUtils.warn("参数错误");
        }
        processService.removeDeploy(id);
        return ResponseUtils.success("删除部署成功");
    }

    /**
     * 分页查询流程信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/process/list", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response list(HttpServletRequest request, HttpServletResponse response) {
        String key = ServletRequestUtils.getStringParameter(request, "key", null);
        String name = ServletRequestUtils.getStringParameter(request, "name", null);

        Map<String, Object> params = new HashMap<String, Object>();
        initParams(params, request);
        params.put("key", key);
        params.put("name", name);

        Page<Process> page = processService.queryPage(params);
        return ResponseUtils.success(page);
    }

    /**
     * 批量删除流程信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/process/delete", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response delete(HttpServletRequest request, HttpServletResponse response) {
        String ids = ServletRequestUtils.getStringParameter(request, "ids", null);
        if (StringUtils.isBlank(ids)) {
            return ResponseUtils.warn("参数错误");
        }
        processService.remove(ids.split(","));
        return ResponseUtils.success("删除成功");
    }
}
