package com.yunfeisoft.business.dao.impl.postgres;

import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.WhereBuilder;
import com.applet.utils.DateUtils;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.IndecOrderDao;
import com.yunfeisoft.business.model.IndecOrder;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * ClassName: IndecOrderDaoImpl
 * Description: 损益单信息Dao实现
 * Author: Jackie liu
 * Date: 2020-08-06
 */
@Repository
public class IndecOrderDaoImpl extends ServiceDaoImpl<IndecOrder, String> implements IndecOrderDao {

    @Override
    public Page<IndecOrder> queryPage(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        wb.setOrderByWithDesc("createTime");
        if (params != null) {
            initPageParam(wb, params);
            wb.andEquals("orgId", params.get("orgId"));
            wb.andEquals("indecDate", DateUtils.getTimestamp(params.get("indecDate")));
        }
        return queryPage(wb);
    }
}