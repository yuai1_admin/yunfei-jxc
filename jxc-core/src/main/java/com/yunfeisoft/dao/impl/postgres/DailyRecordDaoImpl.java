package com.yunfeisoft.dao.impl.postgres;

import com.applet.base.BaseDaoImpl;
import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.WhereBuilder;
import com.applet.utils.Page;
import com.yunfeisoft.dao.inter.DailyRecordDao;
import com.yunfeisoft.model.DailyRecord;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * ClassName: DailyRecordDaoImpl
 * Description: 操作日志Dao实现
 * Author: Jackie liu
 * Date: 2018-03-08
 */
@Repository
public class DailyRecordDaoImpl extends BaseDaoImpl<DailyRecord, String> implements DailyRecordDao {

    @Override
    public Page<DailyRecord> queryPage(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        if (params != null) {
            initPageParam(wb, params);
        }
        return queryPage(wb);
    }
}