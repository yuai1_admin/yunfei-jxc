package com.yunfeisoft.business.dao.inter;

import com.applet.base.BaseDao;
import com.yunfeisoft.business.model.AllotOrder;
import com.applet.utils.Page;

import java.util.List;
import java.util.Map;

/**
 * ClassName: AllotOrderDao
 * Description: 调拨单信息Dao
 * Author: Jackie liu
 * Date: 2020-08-06
 */
public interface AllotOrderDao extends BaseDao<AllotOrder, String> {

    public Page<AllotOrder> queryPage(Map<String, Object> params);

}